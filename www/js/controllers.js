angular.module('starter.controllers', ['ngCordova', 'kinvey', 'ionic-datepicker','ngMaterial','ngResource','cordovaDeviceModule', 'cordovaDeviceMotionModule', 'cordovaNetworkInformationModule', 'cordovaVibrationModule', 'cordovaGeolocationModule'])
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:prescriptionsCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $ionicActionSheet, userDataService, $cordovaCamera, $state, $kinvey, $ionicPopup, $ionicLoading, prescriptionService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("prescriptionsCtrl", function($scope, $ionicActionSheet, userDataService, $cordovaCamera, $state, $kinvey, $ionicPopup, $ionicLoading, prescriptionService){

  try {

  //  localization using localStorage --onMenuToggle

  if(typeof analytics !== 'undefined') { analytics.trackView("prescriptionsCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage

    var activeUser = $kinvey.getActiveUser();
    if(activeUser.username=="TarantulaAdmin"){
      $scope.showPres = false;

    }
    else{
      $scope.user = userDataService.getData();
      $scope.showPres = true;
      $ionicLoading.show({});
      var query = new $kinvey.Query();
      query.equalTo('pres_patient_id',$scope.user.Kidoro_Code);
      var promise = Kinvey.DataStore.find('KidoroPrescriptions',query);
      promise.then(function(entities) {
        // console.log(entities);
        $scope.presArray = [];

        for (var i = 0; i < entities.length; i++) {
          if (entities[i].pres_id == null) {
          } else {
            $scope.presArray.push(entities[i]);
          }
        }
        for (var i = 0; i < $scope.presArray.length; i++) {
          var c = new Date(Number($scope.presArray[i].pres_id));
          $scope.presArray[i]["time"] = c.toDateString();
        }
        // console.log($scope.presArray);

        $ionicLoading.hide();

      }, function(error) {
        $ionicLoading.hide();
        // console.log(error);
      });

      $scope.appendMore = function() {

        $ionicActionSheet.show({
             // titleText: 'ActionSheet Example',
             buttons: [
             { text: '<i class="icon ion-images"></i> Gallery' },
             { text: '<i class="icon ion-ios-camera"></i> Camera' },
            ],
             cancel: function() {
               // console.log('CANCELLED by user');

             },
             buttonClicked: function(index) {

               var currentTime = new Date();
               var uniqueDateTime = currentTime.getTime();

               $scope.newPresArray = {
                 pres_id: uniqueDateTime,
                 pres_image_array: [],
                 pres_uploaded_by: $scope.user.Kidoro_Code,
                 pres_uploader_name: $scope.user.User_Name
               };

               if (index == 0) {
                 // console.log("Gallery Code");

                //  Gallery Code upload and download
                var options = {
                 quality: 20,
                 destinationType: Camera.DestinationType.DATA_URL,
                 sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                  //  allowEdit: true,
                  encodingType: Camera.EncodingType.JPEG,
                  //  targetWidth: 300,
                  //  targetHeight: 300,
                  popoverOptions: CameraPopoverOptions,
                  allowEdit: true,
                  saveToPhotoAlbum: false,
                  correctOrientation: true

                };

                $cordovaCamera.getPicture(options).then(function (imageData) {

                 $scope.lastimgURI = "data:image/jpeg;base64," + imageData;

                 $ionicLoading.show({});

                 if($scope.lastimgURI!==undefined){
                   var contentType = 'image/png';
                   var blob = b64toBlob(imageData, contentType);
                   // console.log(blob);
                   var fileContent = blob;
                   var data = {
                    _filename : uniqueDateTime,
                    mimeType  : 'image/png',
                    size      : fileContent.length
                  }

                  var promise = $kinvey.File.upload(fileContent, data, {
                    public:true
                  });
                  promise.then(function(response) {
                    var promise = $kinvey.File.stream(response._id);
                    promise.then(function(file) {
                      // console.log(file);
                      var url = file._downloadURL;
                      $scope.newPresArray.pres_image_array.push({'pres_link': url,'pres_name': response._filename});
                      prescriptionService.setData($scope.newPresArray);
                      localStorage.setItem("binary", "true");
                      $state.go('menu.presDetails');
                      $ionicLoading.hide();
                    });
                  }, function(err) {
                    $ionicLoading.hide();
                  });
                }

              }, function (err) {
                // console.log(err);
                $ionicLoading.hide();

              });

                   //  Gallery Code upload and download

                 } else if (index == 1) {
                   // console.log("Camera Code");
                 //  Camera Image upload and download

                 var options = {
                   quality: 20,
                   destinationType: Camera.DestinationType.DATA_URL,
                   sourceType: Camera.PictureSourceType.CAMERA,
                   encodingType: Camera.EncodingType.JPEG,
                  //  targetWidth: 512,
                  //  targetHeight: 1024,
                  saveToPhotoAlbum: false,
                  correctOrientation: true
                };

                $cordovaCamera.getPicture(options).then(function (imageData) {

                 $scope.lastimgURI = "data:image/jpeg;base64," + imageData;

                 $ionicLoading.show({});

                 if($scope.lastimgURI!==undefined){
                   var contentType = 'image/png';
                   var blob = b64toBlob(imageData, contentType);
                   // console.log(blob);
                   var fileContent = blob;
                   var data = {
                    _filename : uniqueDateTime,
                    mimeType  : 'image/png',
                    size      : fileContent.length
                  }

                  var promise = $kinvey.File.upload(fileContent, data, {
                    public:true
                  });
                  promise.then(function(response) {
                    var promise = $kinvey.File.stream(response._id);
                    promise.then(function(file) {
                      // console.log(file);
                      var url = file._downloadURL;
                      $scope.newPresArray.pres_image_array.push({'pres_link': url,'pres_name': response._filename});
                      $scope.presArraynew = angular.copy($scope.newPresArray);
                      prescriptionService.setData($scope.presArraynew);
                      localStorage.setItem("binary", "true");
                      $state.go('menu.presDetails');
                      $ionicLoading.hide();
                    });
                  }, function(err) {
                    $ionicLoading.hide();
                  });
                }

              }, function (err) {
                // console.log(err);
                $ionicLoading.hide();

              });
                //  Camera Image upload and download

              } else {

              }

              return true;
            }
          });
}

$scope.sendData = function(item) {
  $scope.items = angular.copy(item);
  localStorage.setItem("binary", "false");
  prescriptionService.setData($scope.items);
  $state.go('menu.presDetails');
}

    // base64 string to blob

    function b64toBlob(b64Data, contentType, sliceSize) {
     contentType = contentType || '';
     sliceSize = sliceSize || 512;

     var byteCharacters = atob(b64Data);
     var byteArrays = [];

     for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
       var slice = byteCharacters.slice(offset, offset + sliceSize);

       var byteNumbers = new Array(slice.length);
       for (var i = 0; i < slice.length; i++) {
         byteNumbers[i] = slice.charCodeAt(i);
       }

       var byteArray = new Uint8Array(byteNumbers);

       byteArrays.push(byteArray);
     }

     var blob = new Blob(byteArrays, {type: contentType});
     return blob;
   }

    // base64 string to blob


  }

} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('presDetailsController', function ($scope, $ionicModal, $ionicSlideBoxDelegate, $state, $cordovaCamera, $ionicLoading, userDataService, $kinvey, $ionicActionSheet, prescriptionService) {

        //  localization using localStorage --onMenuToggle


        if(typeof analytics !== 'undefined') { analytics.trackView("presDetailsController"); }

        $scope.initEvent = function() {
          if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
        }
        selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
        if(selectedLang==null){
          selectedLang="en";
          localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
        }
        $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

          //  localization using localStorage


          var currentTime = new Date();
          var uniqueDateTime = currentTime.getTime();

          $scope.user = userDataService.getData();

          $scope.presArray = prescriptionService.getData();
          $scope.images = $scope.presArray.pres_image_array;


          $ionicModal.fromTemplateUrl('templates/image-modal-slideshow.html', {
           scope: $scope,
           animation: 'slide-in-up'
         }).then(function(modal) {
           $scope.modal = modal;
         });

         $scope.openModal = function() {
           $ionicSlideBoxDelegate.slide(0);
           $scope.modal.show();
         };

         $scope.closeModal = function() {
           $scope.modal.hide();
         };

                   // Cleanup the modal when we're done with it!
                   $scope.$on('$destroy', function() {
                     $scope.modal.remove();
                   });
                   // Execute action on hide modal
                   $scope.$on('modal.hide', function() {
                   // Execute action
                 });
                   // Execute action on remove modal
                   $scope.$on('modal.removed', function() {
                   // Execute action
                 });
                   $scope.$on('modal.shown', function() {
                   });

                   // Call this functions if you need to manually control the slides
                   $scope.next = function() {
                     $ionicSlideBoxDelegate.next();
                   };

                   $scope.previous = function() {
                     $ionicSlideBoxDelegate.previous();
                   };

                   $scope.goToSlide = function(index) {
                     $scope.modal.show();
                     $ionicSlideBoxDelegate.slide(index);
                   }

                   // Called each time the slide changes
                   $scope.slideChanged = function(index) {
                     $scope.slideIndex = index;
                   };


                   if ($scope.presArray.pres_uploaded_by == $scope.user.Kidoro_Code) {

                     $scope.binary = localStorage.getItem("binary");
                     if ($scope.binary  == "true") {
                       // console.log($scope.binary);
                       $scope.showdone = true;
                       $scope.showBtn = true;
                     } else if ($scope.binary  == "false") {
                       // console.log($scope.binary);
                       $scope.showdone = false;
                       $scope.showBtn = true;
                     }

                   } else {
                     $scope.showdone = false;
                     $scope.showBtn = false;
                   }

                   $scope.appendMore = function() {
                     $ionicActionSheet.show({
                // titleText: 'ActionSheet Example',
                buttons: [
                { text: '<i class="icon ion-images"></i> Gallery' },
                { text: '<i class="icon ion-ios-camera"></i> Camera' },
                ],
                cancel: function() {
                  // console.log('CANCELLED by user');

                },
                buttonClicked: function(index) {

                  if (index == 0) {
                    // console.log("Gallery Code");


                                    //  Gallery Code upload and download
                                    var options = {
                                     quality: 20,
                                     destinationType: Camera.DestinationType.DATA_URL,
                                     sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                      //  allowEdit: true,
                                      encodingType: Camera.EncodingType.JPEG,
                                      //  targetWidth: 300,
                                      //  targetHeight: 300,
                                      popoverOptions: CameraPopoverOptions,
                                      allowEdit: true,
                                      saveToPhotoAlbum: false,
                                      correctOrientation: true

                                    };

                                    $cordovaCamera.getPicture(options).then(function (imageData) {

                                     $scope.lastimgURI = "data:image/jpeg;base64," + imageData;

                                     $ionicLoading.show({});

                                     if($scope.lastimgURI!==undefined){
                                       var contentType = 'image/png';
                                       var blob = b64toBlob(imageData, contentType);
                                       // console.log(blob);
                                       var fileContent = blob;
                                       var data = {
                                        _filename : uniqueDateTime,
                                        mimeType  : 'image/png',
                                        size      : fileContent.length
                                      }

                                      var promise = $kinvey.File.upload(fileContent, data, {
                                        public:true
                                      });
                                      promise.then(function(response) {
                                        var promise = $kinvey.File.stream(response._id);
                                        promise.then(function(file) {
                                          // console.log(file);
                                          var url = file._downloadURL;
                                          $scope.presArray.pres_image_array.push({'pres_link': url,'pres_name': response._filename});
                                          $scope.showdone = true;
                                          $ionicLoading.hide();
                                        });
                                      }, function(err) {
                                        $ionicLoading.hide();
                                      });
                                    }

                                  }, function (err) {
                                    // console.log(err);
                                    $ionicLoading.hide();

                                  });

                                       //  Gallery Code upload and download

                                     } else if (index == 1) {
                                      // console.log("Camera Code");

                    //  Camera Image upload and download

                    var options = {
                      quality: 20,
                      destinationType: Camera.DestinationType.DATA_URL,
                      sourceType: Camera.PictureSourceType.CAMERA,
                      encodingType: Camera.EncodingType.JPEG,
                     //  targetWidth: 512,
                     //  targetHeight: 1024,
                     saveToPhotoAlbum: false,
                     correctOrientation: true
                   };

                   $cordovaCamera.getPicture(options).then(function (imageData) {

                    $scope.lastimgURI = "data:image/jpeg;base64," + imageData;

                    $ionicLoading.show({});

                    if($scope.lastimgURI!==undefined){
                      var contentType = 'image/png';
                      var blob = b64toBlob(imageData, contentType);
                      // console.log(blob);
                      var fileContent = blob;
                      var data = {
                       _filename : uniqueDateTime,
                       mimeType  : 'image/png',
                       size      : fileContent.length
                     }

                     var promise = $kinvey.File.upload(fileContent, data, {
                       public:true
                     });
                     promise.then(function(response) {
                       var promise = $kinvey.File.stream(response._id);
                       promise.then(function(file) {
                         // console.log(file);
                         var url = file._downloadURL;
                         $scope.presArray.pres_image_array.push({'pres_link': url,'pres_name': response._filename});
                         $scope.showdone = true;
                         $ionicLoading.hide();
                       });
                     }, function(err) {
                       $ionicLoading.hide();
                     });
                   }

                 }, function (err) {
                   // console.log(err);
                   $ionicLoading.hide();

                 });
                   //  Camera Image upload and download


                 } else {

                 }

                 return true;
               }
             });

}

$scope.submitData = function() {
  $scope.tempArray = [];
  $scope.tempArray = angular.copy($scope.presArray.pres_image_array);
  // console.log($scope.tempArray);
  if ($scope.presArray._id) {
    // console.log("if");
    $scope.newPresArray = {
      _id: $scope.presArray._id,
      pres_id: uniqueDateTime,
      pres_image_array: $scope.tempArray,
      pres_uploaded_by: $scope.presArray.pres_uploaded_by,
      pres_patient_id:$scope.newPresArray.pres_uploaded_by,
      pres_uploader_name: $scope.presArray.pres_uploader_name
    }
    // console.log($scope.newPresArray);


  } else {

    $scope.newPresArray = {
      pres_id: $scope.presArray.pres_id,
      pres_image_array: $scope.tempArray,
      pres_uploaded_by: $scope.presArray.pres_uploaded_by,
      pres_patient_id:$scope.newPresArray.pres_uploaded_by,
      pres_uploader_name: $scope.presArray.pres_uploader_name
    }
  }
  // console.log($scope.newPresArray);


  // console.log("Submit");
  // console.log($scope.newPresArray );

  if ($scope.newPresArray._id) {
    // console.log($scope.newPresArray);
    // console.log("Old Data");

    $ionicLoading.show({});
    var promise = $kinvey.DataStore.save('KidoroPrescriptions', {
      _id: $scope.newPresArray._id,
      pres_id: $scope.newPresArray.pres_id,
      pres_image_array: $scope.newPresArray.pres_image_array,
      pres_uploaded_by: $scope.newPresArray.pres_uploaded_by,
      pres_patient_id:$scope.newPresArray.pres_uploaded_by,
      pres_uploader_name: $scope.newPresArray.pres_uploader_name
    });

    promise.then(function(model) {
     // console.log(model);
     prescriptionService.setData(model);
     $state.go('menu.prescriptions')
     $ionicLoading.hide();
   }, function(err) {
     $ionicLoading.hide();
     // console.log(err);
   });

  } else {
    // console.log($scope.newPresArray);
    // console.log("New Data");

    $ionicLoading.show({});
    var promise = $kinvey.DataStore.save('KidoroPrescriptions', $scope.newPresArray);
    promise.then(function(model) {
     // console.log(model);
     $state.go('menu.prescriptions')
     prescriptionService.setData(model);

     $ionicLoading.hide();
   }, function(err) {
     $ionicLoading.hide();
     // console.log(err);

   });
  }

}

              // base64 string to blob

              function b64toBlob(b64Data, contentType, sliceSize) {
               contentType = contentType || '';
               sliceSize = sliceSize || 512;

               var byteCharacters = atob(b64Data);
               var byteArrays = [];

               for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                 var slice = byteCharacters.slice(offset, offset + sliceSize);

                 var byteNumbers = new Array(slice.length);
                 for (var i = 0; i < slice.length; i++) {
                   byteNumbers[i] = slice.charCodeAt(i);
                 }

                 var byteArray = new Uint8Array(byteNumbers);

                 byteArrays.push(byteArray);
               }

               var blob = new Blob(byteArrays, {type: contentType});
               return blob;
             }

              // base64 string to blob


            })
            //************************************************************Kidoro Health Patient****************************************************************//
            // Method Name:groupChatCtrl
            // Developed By: TarantulaLabs
            // Dependencies:$scope, $cordovaKeyboard, $ionicScrollDelegate,$ionicNavBarDelegate,saveChatNotification, $kinvey, $ionicLoading, Message, userDataService,docDataService,$state,$ionicHistory,$ionicSlideBoxDelegate
            // Description:
            //************************************************************Kidoro Health Patient****************************************************************//

.controller('groupChatCtrl', function($scope, $cordovaKeyboard, $ionicScrollDelegate,$ionicNavBarDelegate,saveChatNotification, $kinvey, $ionicLoading, Message, userDataService,docDataService,$state,$ionicHistory,$ionicSlideBoxDelegate){

  var currentUser = $kinvey.getActiveUser()
  $scope.showBadges=false;
 // $ionicNavBarDelegate.showBackButton(false);
 // $ionicHistory.clearHistory();
// console.log();
// if($scope.chatNotificationData!=null){
//   $scope.chatNotificationData.data=JSON.parse($scope.chatNotificationData.data);
//   if($scope.chatNotificationData.data.kidoroCode.indexOf("DOC")>-1){
//     $scope.showBadges=true;
//   }
//   }
          //  localization using localStorage --onMenuToggle
          if(typeof analytics !== 'undefined') { analytics.trackView("groupChatCtrl"); }

          $scope.initEvent = function() {
            if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
          }
          selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
          if(selectedLang==null){
            selectedLang="en";
            localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
          }
          $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

              //  localization using localStorage
              $scope.loginButton=function(){
                $ionicHistory.nextViewOptions({
                  disableBack: false
                });
                $state.go('menu.loginHome');
              }
              if(currentUser.username == "TarantulaAdmin"){
                $scope.showChat = false;
                 $scope.loggedIn=false;
                // console.log("LoginFirst");
              }else{
                $scope.showChat = true;
                $scope.loggedIn=true;
                // console.log("LoggedIn");

                $scope.scrollBottom = function() {
                  $ionicScrollDelegate.scrollBottom(true);
                  cordova.plugins.Keyboard.show();
                  var isVisible = cordova.plugins.Keyboard.isVisible()

                  console.log(isVisible);
                  if (isVisible == true) {
                    // console.log("scrollBottom");

                    $ionicScrollDelegate.scrollBottom(true);

                  }
                }

                $scope.slideIndex=0;
                angular.element( document.querySelector( '#btn_gyno' ) ).css('border-bottom','4px solid #fff');
                angular.element( document.querySelector( '#btn_paed' ) ).css('border-bottom','1px solid #009999');
                angular.element( document.querySelector( '#btn_gyno' ) ).css('color','#fff');
                angular.element( document.querySelector( '#btn_paed' ) ).css('color','#037B7B');
                angular.element( document.querySelector( '#btn_gyno' ) ).css('border-left','0px');
                angular.element( document.querySelector( '#btn_gyno' ) ).css('border-right','0px');
                angular.element( document.querySelector( '#btn_paed' ) ).css('border-left','1px solid #009999');

                $scope.slideHasChanged=function(index){
                  $scope.slideIndex=index;
                  if(index==0){
                   angular.element( document.querySelector( '#btn_gyno' ) ).css('border-bottom','4px solid #fff');
                   angular.element( document.querySelector( '#btn_paed' ) ).css('border-bottom','1px solid #009999');
                   angular.element( document.querySelector( '#btn_gyno' ) ).css('color','#fff');
                   angular.element( document.querySelector( '#btn_paed' ) ).css('color','#037B7B');
                   angular.element( document.querySelector( '#btn_gyno' ) ).css('border-left','0px');
                   angular.element( document.querySelector( '#btn_gyno' ) ).css('border-right','0px');
                   angular.element( document.querySelector( '#btn_paed' ) ).css('border-left','1px solid #009999');
                 }
                 if(index==1){
                   if($scope.chatNotificationData!=null){
                    $scope.chatNotificationData.data=JSON.parse($scope.chatNotificationData.data);
                    if($scope.chatNotificationData.data.kidoroCode.indexOf("DOC")>-1){
                      for (var i = 0; i < $scope.docList.length; i++) {
                        if($scope.docList[i].Doctor_ID==$scope.chatNotificationData.data.kidoroCode){
                          $scope.docList[i]["showBadge"]=true;
                        }
                      }
                    }
                  }

                  angular.element( document.querySelector( '#btn_paed' ) ).css('border-bottom','4px solid #fff');
                  angular.element( document.querySelector( '#btn_gyno' ) ).css('border-bottom','1px solid #009999');
                  angular.element( document.querySelector( '#btn_gyno' ) ).css('color','#037B7B');
                  angular.element( document.querySelector( '#btn_paed' ) ).css('color','#fff');
                  angular.element( document.querySelector( '#btn_gyno' ) ).css('border-right','1px solid #009999');
                  angular.element( document.querySelector( '#btn_paed' ) ).css('border-right','0px');
                  angular.element( document.querySelector( '#btn_paed' ) ).css('border-left','0px');
                }
              }
              $scope.$on('$ionicView.beforeEnter', function(){
  // Anything you can think of
  $scope.chatNotificationData=saveChatNotification.getData();

  if($scope.chatNotificationData!=null){
    $scope.docChatClicked();
    $scope.showBadges=true;
    $scope.chatNotificationData.data=JSON.parse($scope.chatNotificationData.data);
    if($scope.chatNotificationData.data.kidoroCode.indexOf("DOC")>-1){
      for (var i = 0; i < $scope.docList.length; i++) {
        if($scope.docList[i].Doctor_ID==$scope.chatNotificationData.data.kidoroCode){
          $scope.docList[i]["showBadge"]=true;
        }
      }
    }
  }
  else{
    $scope.showBadges=false;
    for (var i = 0; i < $scope.docList.length; i++) {

      $scope.docList[i]["showBadge"]=false;

    }
  }
});
              $scope.momChatClicked=function(){
               $ionicSlideBoxDelegate.slide(0);
             }
             $scope.docChatClicked=function(){
               $ionicSlideBoxDelegate.slide(1);
             }

             // console.log($ionicHistory.viewHistory());
             if($ionicHistory.viewHistory().forwardView!=null&&$ionicHistory.viewHistory().forwardView.stateName=="menu.privateChat"){
              $ionicSlideBoxDelegate.slide(1);
              $scope.docChatClicked();
            }

            var userData = userDataService.getData();
            $scope.cssUser = userData.Kidoro_Code;
            $scope.profilePic = userData.Profile_Pic_URI;
            // console.log($scope.cssUser);

            $scope.messages= Message.all;
            $scope.users = [];

            $ionicLoading.show({});
            $scope.messages.$loaded().then(function(messages) {
              // console.log(messages);
              $ionicScrollDelegate.scrollBottom();

              for (var i = 0; i < messages.length; i++) {
                $scope.users.push(messages[i].kidoroCode);
              }

            // console.log($scope.users);

            // var a = $scope.users.indexOf(userData.Kidoro_Code);
            // $scope.cssUser = $scope.users[a];
            // console.log($scope.cssUser);
            // if ($scope.cssUser === undefined) {
            //   $scope.cssUser = userData.Kidoro_Code;
            //   console.log($scope.cssUser);
            //
            // }

            // $ionicLoading.hide();
          });

            $scope.newmessage = {
              text: "",
              user: "",
              kidoroCode:"",
              time: "",
              displayTime: "",
              dp: ""
            }

            $scope.sendMessage = function(message){
             $ionicScrollDelegate.scrollBottom(true);
             var time = new Date().getTime();

             var date = new Date(time);


            // Hours part from the timestamp
            var hours = date.getHours();
            // Minutes part from the timestamp
            var minutes = "0" + date.getMinutes();
            // Seconds part from the timestamp
            // var seconds = "0" + date.getSeconds();

            // Will display time in 10:30:23 format
            var formattedTime = hours + ':' + minutes.substr(-2);

            if (userData.Profile_Pic_URI === undefined || userData.Profile_Pic_URI == "" || userData.Profile_Pic_URI == null) {

              $scope.newmessage = {
                text: message.text,
                user: userData.User_Name,
                kidoroCode: userData.Kidoro_Code,
                time: time,
                displayTime: formattedTime,
                dp: null

              }
              Message.create($scope.newmessage);

            } else {

              $scope.newmessage = {
                text: message.text,
                user: userData.User_Name,
                kidoroCode: userData.Kidoro_Code,
                time: time,
                displayTime: formattedTime,
                dp: userData.Profile_Pic_URI
              }
              Message.create($scope.newmessage);

            }

            // $scope.newmessage = {
            //   text: message.text,
            //   user: userData.User_Name,
            //   kidoroCode: userData.Kidoro_Code,
            //   time: time,
            //   displayTime: formattedTime,
            //   dp: userData.Profile_Pic_URI
            //
            // }


            // Message.create($scope.newmessage);

            delete $scope.newmessage.text;

            cordova.plugins.Keyboard.disableScroll(true)
            var isVisible = cordova.plugins.Keyboard.isVisible()


            if (isVisible == true) {

              cordova.plugins.Keyboard.close()
              $ionicScrollDelegate.scrollBottom(true);

            }
            cordova.plugins.Keyboard.close()

            //  cordova.plugins.Keyboard.close();
            $ionicScrollDelegate.scrollBottom(true);
          };

        }
//------------------------------------------------userchat doctorlist------------------------------------------------------------//
var user = userDataService.getData();
$scope.docList=[];
var activeUser = $kinvey.getActiveUser();
if(activeUser.username=="TarantulaAdmin"){
  $scope.showPrivateChat = false;
  $scope.messageForUser=$scope.savedlang.plslogintousekidorochat;

}
else{
  $scope.showPrivateChat = true;
  $ionicLoading.show({});
  var query = new $kinvey.Query();

  query.equalTo('User_ID',user.Kidoro_Code);
  var promise = $kinvey.DataStore.find('KidoroAppointment', query);

  promise.then(function(models) {
    if(models.length==0){
      $scope.showPrivateChat = false
      $scope.messageForUser="Book an appointment to chat with doctor";
      $ionicLoading.hide();
    }
    else{

      for (var i = 0; i < models.length; i++) {
        var query = new $kinvey.Query();

        query.equalTo('Doctor_ID',models[i].Doctor_ID);
        var promise = $kinvey.DataStore.find('KidoroDocs', query);

        promise.then(function(models) {

          $ionicLoading.hide();
          if(models.length>0){
            models[0]["showBadge"]=false;
            if($scope.chatNotificationData!=null){
              if($scope.chatNotificationData.data.kidoroCode==models[0].Doctor_ID){
                models[0]["showBadge"]=true;
              }
              else{
               models[0]["showBadge"]=false;
             }
           }
           if ((models[0].Doc_Pic == undefined) || (models[0].Doc_Pic == null)) {
            models[0]['docPic'] = "./img/noimage2.png";
          }
          else{
            models[0]['docPic'] = models[0].Doc_Pic;
          }

          if(!contains($scope.docList,models[0])){
            $scope.docList.push(models[0]);
          }
        }


            // if (!models.length) {
            //   $scope.showError = true;
            //   $scope.appointmentErrorList = $scope.savedlang.noAptmntexists;

            // } else {
            //   $scope.showError = false;
            //   $scope.appointmentList = models;

            // }

          },
          function(err) {
           $ionicLoading.hide();

           var errorAlert = $ionicPopup.alert({
             title: 'Kidoro Health',
             template: $scope.savedlang.sometngwentwrng,
             cssClass:'custom-popup-alert',
             buttons: [{
              text: $scope.savedlang.okTxt,
              type: 'button-alert-ok',
              onTap: function(e) {
                errorAlert.close();


              }
            }]
          });

         });
      }

    }
  },
  function(err) {
   $ionicLoading.hide();

   var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });

 });
}
$scope.openChat=function(item){
 if( $scope.showBadges==true){
  $scope.showBadges=false;
}
// console.log(item);
docDataService.setData(item);
saveChatNotification.setData(null);
for (var i = 0; i < $scope.docList.length; i++) {
  if($scope.docList[i].Doctor_ID==item.Doctor_ID&&$scope.docList[i].showBadge==true){
    $scope.docList[i].showBadge==false;
  }
}
$state.go('menu.privateChat');
}
function contains(a, obj) {
  if(a!=null){
   for (var i = 0; i < a.length; i++) {
    if (a[i].Doctor_ID === obj.Doctor_ID) {
      return true;
    }
  }
}

return false;
}


})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:privateChatCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, docDataService, userDataService, $state,$ionicNavBarDelegate, $kinvey, $ionicPopup, $ionicLoading, $ionicScrollDelegate,privateMessage
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("privateChatCtrl", function($scope, docDataService, userDataService, $state,$ionicNavBarDelegate, $kinvey, $ionicPopup, $ionicLoading, $ionicScrollDelegate,privateMessage){

  try {

  //  localization using localStorage --onMenuToggle


// $ionicNavBarDelegate.showBackButton(true);
// $scope.myGoBack=function(){
//   $state.go('menu.groupChat');
// }

if(typeof analytics !== 'undefined') { analytics.trackView("privateChatCtrl"); }

$scope.initEvent = function() {
  if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
}


selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
if(selectedLang==null){
  selectedLang="en";
  localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
}
$scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage

    // $scope.$on('$ionicView.afterEnter', function(){
    //     $scope.hideBar = "true";
    // });

    $scope.selctedDoctorDetails = docDataService.getData();

           // private chat with doctors
           var currentUser = $kinvey.getActiveUser()


           // $ionicModal.fromTemplateUrl('templates/privateChat.html', {
           //   scope: $scope
           // }).then(function(modal) {
           //   $scope.modal = modal;
           // });


           if(currentUser.username == "TarantulaAdmin"){
            //  $scope.showChat = false;

             // $scope.openChatModal  = function() {
               ionicToast.show($scope.savedlang.logintochat, 'bottom', false, 1500);

             // }
           }else{



            //  $scope.showChat = true;

             // $scope.openChatModal  = function() {
             //     $scope.modal.show();
             //     $ionicScrollDelegate.scrollBottom();
             // }

             // $scope.closeChatModal = function() {
             //   $ionicScrollDelegate.scrollTop();

             //   $scope.modal.hide();

             // }

             $scope.scrollBottom = function() {
               cordova.plugins.Keyboard.show();
               var isVisible = cordova.plugins.Keyboard.isVisible()


               if (isVisible == true) {


                 $ionicScrollDelegate.scrollBottom(true);

               }
             }
             var userData = userDataService.getData();
             $scope.cssUser = userData.Kidoro_Code;
             $scope.profilePic = userData.Profile_Pic_URI;
             // console.log($scope.cssUser);
             $scope.senderID = $scope.selctedDoctorDetails.Doctor_ID + "&" + $scope.cssUser;
             $scope.messages= privateMessage.all($scope.senderID);

             $ionicLoading.show({});
             $scope.messages.$loaded().then(function(messages) {
               // console.log($scope.messages);
               $ionicScrollDelegate.scrollBottom();
               $ionicLoading.hide();

             });

             $scope.newmessage = {
               text: "",
               user: "",
               kidoroCode:"",
               time: "",
               displayTime: "",
               dp: ""
             }

             $scope.sendMessage = function(message){

               var time = new Date().getTime();
               // console.log(time);
               var date = new Date(time);
               // console.log(date);

                   // Hours part from the timestamp
                   var hours = date.getHours();
                   // Minutes part from the timestamp
                   var minutes = "0" + date.getMinutes();
                   // Seconds part from the timestamp
                   // var seconds = "0" + date.getSeconds();

                   // Will display time in 10:30:23 format
                   var formattedTime = hours + ':' + minutes.substr(-2);

                   if (userData.Profile_Pic_URI === undefined || userData.Profile_Pic_URI == "" || userData.Profile_Pic_URI == null) {

                     $scope.newmessage = {
                       text: message.text,
                       user: userData.User_Name,
                       kidoroCode: userData.Kidoro_Code,
                       time: time,
                       displayTime: formattedTime,
                       dp: null

                     }
                     // console.log($scope.senderID);
                     privateMessage.create($scope.newmessage, $scope.senderID);

                   } else {

                     $scope.newmessage = {
                       text: message.text,
                       user: userData.User_Name,
                       kidoroCode: userData.Kidoro_Code,
                       time: time,
                       displayTime: formattedTime,
                       dp: userData.Profile_Pic_URI,
                     }
                     // console.log($scope.selctedDoctorDetails);
                     privateMessage.create($scope.newmessage, $scope.senderID);

                   }

                   // console.log($scope.newmessage);
                   $scope.messages= privateMessage.all($scope.senderID);

                   $scope.messages.$loaded().then(function(messages) {
                    $ionicScrollDelegate.scrollBottom();
                    // console.log($scope.messages);
                  });

                   delete $scope.newmessage.text;
                   cordova.plugins.Keyboard.disableScroll(true)
                   var isVisible = cordova.plugins.Keyboard.isVisible()

                   // console.log(isVisible);
                   if (isVisible == true) {
                     // console.log("closeKeyboard");
                     cordova.plugins.Keyboard.close()
                     $ionicScrollDelegate.scrollBottom(true);

                   }
                   cordova.plugins.Keyboard.close()

                   //  cordova.plugins.Keyboard.close();
                   $ionicScrollDelegate.scrollBottom(true);
                 };


               }

           // private chat with doctors


         } catch (err) {
          var errorAlert = $ionicPopup.alert({
           title: 'Kidoro Health',
           template: $scope.savedlang.sometngwentwrng,
           cssClass:'custom-popup-alert',
           buttons: [{
            text: $scope.savedlang.okTxt,
            type: 'button-alert-ok',
            onTap: function(e) {
              errorAlert.close();


            }
          }]
        });
        }

      })




      //************************************************************Kidoro Health Patient****************************************************************//
      // Method Name:HomeCtrl
      // Developed By: TarantulaLabs
      // Dependencies:$ionicPlatform,  $rootScope, $ionicPopover, $cordovaAppRate, $scope, $ionicPopup, $state,$rootScope, $ionicUser, $kinvey,$ionicHistory, userDataService
      // Description:
      //************************************************************Kidoro Health Patient****************************************************************//


.controller('HomeCtrl', function ($ionicPlatform,  $rootScope, $ionicPopover, $cordovaAppRate, $scope, $ionicPopup, $state,$rootScope, $ionicUser, $kinvey,$ionicHistory, userDataService) {
 $ionicHistory.clearHistory();
 try {

        //  localization using localStorage

//The essential purpose of badge numbers is to enable an application to inform its users that it has something for them — for example, unread messages — when the application isn’t running in the foreground.
        $scope.showNotifyBadge=false;
        if(typeof analytics !== 'undefined') { analytics.trackView("HomeCtrl"); }
//use the trackView per view. So in each controller I pass the name of the page
        $scope.initEvent = function() {
          if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
        }
$scope.$on('update',function(){
         // console.log("update");
        if($rootScope.badgeNo==0){
            $scope.showNotifyBadge=false;
          }
          else{
            $scope.showNotifyBadge=true;
            $scope.notifyNo=$rootScope.badgeNo;
          }
       });
$scope.updateBadge=function(){
   if($rootScope.badgeNo==0){
            $scope.showNotifyBadge=false;
          }
          else{
            $scope.showNotifyBadge=true;
            $scope.notifyNo=$rootScope.badgeNo;
          }
}

        $scope.languageArray = [];
        selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
        if(selectedLang==null){
          selectedLang="en";
          localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
        }
        $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];
        $scope.languageArray = $scope.savedlang.languageList;
          //  localization using localStorage

          // langauage popover --start
          $ionicPopover.fromTemplateUrl('templates/popoverLanguageChange.html', {
            scope: $scope,
          }).then(function(popover) {
            $scope.popover = popover;
          });
        $scope.goToNotifications=function(){
          $state.go('menu.notification');
        }
          $scope.openLanguageList = function() {

            var myPopup = $ionicPopup.show({
              title: $scope.savedlang.selectalang,
              scope: $scope,
              template:  '<ion-list>                                '+
              '  <ion-item class = "row lang_css" ng-click="selectLang(item.key)" ng-repeat="item in languageArray" ng-class = "{selectedLang: item.key == selectedLanguage}"> '+
              '    <div class="col col-50">{{item.lang}}</div> <div class="col col-50" style="text-align:center;"><i ng-show = "item.key == selectedLanguage" class="icon ion-checkmark"></i></div>'+
              '  </ion-item>                             '+
              '</ion-list>                               ',
              cssClass: 'custom_popup_language',


            });

            $scope.selectedLanguage = localStorage.getItem('selectedLocalizeLang');

            $scope.selectLang = function(lang) {
              $scope.selectedLanguage = lang;
              localStorage.setItem('selectedLocalizeLang',lang);
              Localize.setLanguage($scope.selectedLanguage);
              myPopup.close();
            }
          }
          // langauage popover --end
        }
        catch(err) {
         var errorAlert = $ionicPopup.alert({
           title: 'Kidoro Health',
           template: $scope.savedlang.sometngwentwrng,
           cssClass:'custom-popup-alert',
           buttons: [{
            text: $scope.savedlang.okTxt,
            type: 'button-alert-ok',
            onTap: function(e) {
              errorAlert.close();

            }
          }]
        });

       }
     })
     //************************************************************Kidoro Health Patient****************************************************************//
     // Method Name:controller-tempSplashCtrl
     // Developed By: TarantulaLabs
     // Dependencies:$scope,$state,$timeout,$rootScope, $ionicPopup, $ionicUser, $kinvey,$ionicHistory,FeedList, locationDataService
     // Description:Automatically generate a splash screen with kidoro logo
     //************************************************************Kidoro Health Patient****************************************************************//

.controller('tempSplashCtrl', function ($scope,$state,$timeout,$rootScope, $ionicPopup, $ionicUser, $kinvey,$ionicHistory,FeedList, locationDataService) {
  try {


  }
  catch(err) {

        //  localization using localStorage --onMenuToggle

        selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
        if(selectedLang==null){
          selectedLang="en";
          localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
        }
        $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

          //  localization using localStorage

          var errorAlert = $ionicPopup.alert({
           title: 'Kidoro Health',
           template: $scope.savedlang.sometngwentwrng,
           cssClass:'custom-popup-alert',
           buttons: [{
            text: $scope.savedlang.okTxt,
            type: 'button-alert-ok',
            onTap: function(e) {
              errorAlert.close();

            }
          }]
        });

        }

      })


      //************************************************************Kidoro Health Patient****************************************************************//
      // Method Name:controller-FeedCtrl
      // Developed By: TarantulaLabs
      // Dependencies:$state,loadRssDataService,$timeout,$ionicLoading,$cordovaToast,$ionicHistory,saveParentState, ionicToast, $scope,$rootScope,$ionicPopover, $kinvey, FeedList,$interval,$ionicPopup,$location,saveRssDataService,rssPassDataService,userDataService
      // Description:
      //************************************************************Kidoro Health Patient****************************************************************//

.controller('FeedCtrl',function ($state,loadRssDataService,$timeout,$ionicLoading,$cordovaToast,$ionicHistory,saveParentState, ionicToast, $scope,$rootScope,$ionicPopover, $kinvey, FeedList,$interval,$ionicPopup,$location,saveRssDataService,rssPassDataService,userDataService) {
  try {
    var fav=null;

              //  localization using localStorage --onMenuToggle
              if(typeof analytics !== 'undefined') { analytics.trackView("FeedCtrl"); }

              $scope.initEvent = function() {
                if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
              }
              selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
              if(selectedLang==null){
                selectedLang="en";
                localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
              }
              $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

                //  localization using localStorage

                var user = $kinvey.getActiveUser();
                var currentStateName = $ionicHistory.currentStateName();
                $ionicPopover.fromTemplateUrl('templates/popoverRSS.html', {
                  scope: $scope,
                }).then(function(popover) {
                  $scope.popover = popover;
                });

                $scope.$on('FeedList', function (event, data) {
                  $scope.feeds = data;
                });

                $scope.populateList = function() {

                  $scope.feed = saveRssDataService.getData();
                  if ($scope.feed.length != undefined) {
                    $scope.feed.sort(function(a, b) {
                      return parseFloat(b.time) - parseFloat(a.time);
                    });
                  }
                  // fav code by saras

                  if(user.username == "TarantulaAdmin"){
                    saveParentState.setState(currentStateName);

                  }else{


                    var currentUser=userDataService.getData();

                    for (var i = 0; i < $scope.feed.length; i++) {
                     $scope.feed[i].liked = false;
                   }

                   if(currentUser.Fav_Feeds!=undefined){
                     for (var i = 0; i < $scope.feed.length; i++) {
                       for (var j = 0; j < currentUser.Fav_Feeds.length; j++) {
                         if (($scope.feed[i].time == currentUser.Fav_Feeds[j].feed.time) && ($scope.feed[i].contentSnippet == currentUser.Fav_Feeds[j].feed.contentSnippet) ) {
                          $scope.feed[i].liked = true;
                        }

                      }
                    }
                  }

                }

                if ($scope.feed.length != undefined) {
                  $scope.feed.sort(function(a, b) {
                    return parseFloat(b.time) - parseFloat(a.time);
                  });
                }


                $scope.$broadcast('scroll.refreshComplete');
              }

              $scope.noMoreItemsAvailable = false;

              $scope.loadMore = function() {

                var newData=loadRssDataService.loadData($scope.items.length,$scope.feed);
                for (var i = 0; i <newData.length; i++) {
                 $scope.items.push(newData[i]);
               };

               $scope.$broadcast('scroll.infiniteScrollComplete');

             };

             $scope.items = [];

             $scope.openFavList=function(){

              if(user.username == "TarantulaAdmin"){
                ionicToast.show($scope.savedlang.plslogintousethisfeature, 'bottom', false, 1500);

              }else{
                var currentUser = userDataService.getData();
                if (currentUser.Fav_Feeds!=undefined || currentUser.Fav_Feeds!==null) {
                  userDataService.setData(currentUser);
                  $state.go('menu.fav');
                } else {

                  ionicToast.show($scope.savedlang.nofavfeedselcted, 'bottom', false, 1500);

                }
              }


            }
          // Anything you can think of on back button

          $scope.$on('$ionicView.leave', function(){
           var favFeeds=[];
           if(user.username == "TarantulaAdmin"){

           }else{
            var currentUser=userDataService.getData();
            if(currentUser.Fav_Feeds==null){
             currentUser.Fav_Feeds=[];
           }

           for (var i = 0; i < currentUser.Fav_Feeds.length; i++) {
            var favFeed={
             author: null,
             categories: null,
             content:null,
             contentSnippet:null,
             image:null,
             link: null,
             publishedDate: null,
             thumb: null,
             time: null,
             title: null,
             liked: null
           };

           favFeed.author=currentUser.Fav_Feeds[i].feed.author;
           favFeed.categories=currentUser.Fav_Feeds[i].feed.categories;
           favFeed.content=currentUser.Fav_Feeds[i].feed.content;
           favFeed.contentSnippet=currentUser.Fav_Feeds[i].feed.contentSnippet;
           favFeed.image=currentUser.Fav_Feeds[i].feed.image;
           favFeed.link=currentUser.Fav_Feeds[i].link;
           favFeed.publishedDate=currentUser.Fav_Feeds[i].feed.publishedDate;
           favFeed.thumb=currentUser.Fav_Feeds[i].feed.thumb;
           favFeed.time=currentUser.Fav_Feeds[i].feed.time;
           favFeed.title=currentUser.Fav_Feeds[i].feed.title;
           favFeed.liked=currentUser.Fav_Feeds[i].feed.liked;
           var tempFeed={"feed":favFeed};
           favFeeds.push(tempFeed);
         };
         if(favFeeds.length==0){
          favFeeds=null;
        }
        var promise = $kinvey.DataStore.save('KidoroUser', {

          _id:currentUser._id,
          User_ID: currentUser.User_ID,
          User_Name : currentUser.User_Name,
          User_Phone : currentUser.User_Phone,
          User_Mail : currentUser.User_Mail,
          Kidoro_Code :currentUser.Kidoro_Code,
          Child_Date : currentUser.Child_Date,
          Child_Due_Date: currentUser.Child_Due_Date,
          Child_Name : currentUser.Child_Name,
          Relationship_ID : currentUser.Relationship_ID,
          Kidoro_Mom_Code : currentUser.Kidoro_Mom_Code,
          Profile_Pic_URI: currentUser.Profile_Pic_URI,
          Fav_Feeds : favFeeds,
          Documents: currentUser.Documents,
          Vaccination_Record:currentUser.Vaccination_Record
        });
        promise.then(function(model) {

         userDataService.setData(model);

       }, function(err) {
        var errorAlert = $ionicPopup.alert({
          title: 'Kidoro Health',
          template: $scope.savedlang.sometngwentwrng,
          cssClass:'custom-popup-alert',
          buttons: [{
            text: $scope.savedlang.okTxt,
            type: 'button-alert-ok',
            onTap: function(e) {
             errorAlert.close();

           }
         }]
       });

      });
      }
    });

          $scope.$on('$ionicView.beforeEnter', function(){
            if($ionicHistory.forwardView()!=null){
              if($ionicHistory.forwardView().stateName=='menu.fav'){
                $scope.populateList();
                $scope.items=[];
                $scope.loadMore();
              }
            }

          });

          $scope.$on('$ionicView.loaded', function(){
            $ionicLoading.show()
            $scope.populateList();
            $scope.items=[];
            $scope.loadMore();
            $ionicLoading.hide()
          });

          $scope.favSelected=function(feed){

            if(user.username == "TarantulaAdmin"){
             ionicToast.show($scope.savedlang.plslogintousethisfeature, 'bottom', false, 1500);
           }
           else{
            var currentUser=userDataService.getData();
            $scope.selectedFeed = feed;
            feed.liked = feed.liked === false ? true: false;

            fav=feed;
            var favJson={"feed":fav};
            if(currentUser.Fav_Feeds==null){
              currentUser.Fav_Feeds=[];
            }

            var matchCount=0;
            if(currentUser.Fav_Feeds.length>0){
              for (var i = 0; i < currentUser.Fav_Feeds.length; i++) {
                if(currentUser.Fav_Feeds[i].feed.link==favJson.feed.link){
                  matchCount++;
                  break;
                }
              };
              if(matchCount==0){
                currentUser.Fav_Feeds.push(favJson);
              }
            }
            else{
              currentUser.Fav_Feeds.push(favJson);
            }

          }
        }

        $scope.browse = function(content,date,title,image,link,categories) {

         rssPassDataService.setData(content,date,title,image,link,categories);
         $state.go('menu.rsscontent');

       }



       $scope.OtherShare=function(contentsnippet, feedtitle, feedurl){

              // window.plugins.socialsharing.share(message,subject,file,url);

              window.plugins.socialsharing.share(contentsnippet, feedtitle, null, feedurl);
            }

          }
          catch(err) {
            var errorAlert = $ionicPopup.alert({
             title: 'Kidoro Health',
             template: $scope.savedlang.sometngwentwrng,
             cssClass:'custom-popup-alert',
             buttons: [{
              text: $scope.savedlang.okTxt,
              type: 'button-alert-ok',
              onTap: function(e) {
                errorAlert.close();

              }
            }]
          });

          }

        })
        //************************************************************Kidoro Health Patient****************************************************************//
        // Method Name:favListCtrl
        // Developed By: TarantulaLabs
        // Dependencies:$scope, $state, $ionicPopup, $ionicHistory,$ionicPopover,$ionicLoading,$kinvey, $location,rssPassDataService,userDataService
        // Description:
        //************************************************************Kidoro Health Patient****************************************************************//

.controller('favListCtrl',function ($scope, $state, $ionicPopup, $ionicHistory,$ionicPopover,$ionicLoading,$kinvey, $location,rssPassDataService,userDataService) {

  try {

          //  localization using localStorage --onMenuToggle

          if(typeof analytics !== 'undefined') { analytics.trackView("favListCtrl"); }

          $scope.initEvent = function() {
            if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
          }

          selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
          if(selectedLang==null){
            selectedLang="en";
            localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
          }
          $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

            //  localization using localStorage

            var currentUser=userDataService.getData();
            $scope.feeds = [];

            $scope.populateList = function() {
              for (var i = 0; i < currentUser.Fav_Feeds.length; i++) {
                $scope.feeds.push(currentUser.Fav_Feeds[i].feed);
              };
            }

            $scope.populateList();


            $scope.browse = function(content,date,title,image,link,categories) {
             rssPassDataService.setData(content,date,title,image,link,categories);
             $state.go('menu.rsscontent');
           }

           $scope.countDeleted = 0;
           $scope.favSelected = function(feed) {
            $scope.count = $scope.feeds.length;
            $scope.feeds.splice($scope.feeds.indexOf(feed), 1);
            $scope.countDeleted =  $scope.countDeleted + ($scope.count - $scope.feeds.length);
            $scope.feed = angular.copy($scope.feeds);

          }



          $scope.deleteFavSelected = function() {

            var favFeeds=[];

            for (var i = 0; i < $scope.feed.length; i++) {

             var tempFeed={"feed":$scope.feed[i]};
             favFeeds.push(tempFeed);

           };

           if ($scope.countDeleted != 0) {
            if (favFeeds.length == 0) {
              favFeeds = null;
            }
            var updatedUserData ={
              _id:currentUser._id,
              User_ID: currentUser.User_ID,
              User_Name : currentUser.User_Name,
              User_Phone : currentUser.User_Phone,
              User_Mail : currentUser.User_Mail,
              Kidoro_Code :currentUser.Kidoro_Code,
              Child_Date : $scope.user.Child_Date,
              Child_Due_Date: $scope.user.Child_Due_Date,
              Child_Name : currentUser.Child_Name,
              Relationship_ID : currentUser.Relationship_ID,
              Kidoro_Mom_Code : currentUser.Kidoro_Mom_Code,
              Profile_Pic_URI: currentUser.Profile_Pic_URI,
              Fav_Feeds : favFeeds,
              Documents: currentUser.Documents,
              Vaccination_Record:currentUser.Vaccination_Record
            };
            var promise = $kinvey.DataStore.save('KidoroUser', updatedUserData);
            $ionicLoading.show({});
            promise.then(function(model) {
             $scope.countDeleted = 0;
             userDataService.setData(updatedUserData);
             $ionicLoading.hide();
           }, function(err) {
             $ionicLoading.hide();
             var errorAlert = $ionicPopup.alert({
               title: 'Kidoro Health',
               template: savedlang.sometngwentwrng,
               cssClass:'custom-popup-alert',
               buttons: [{
                text: savedlang.okTxt,
                type: 'button-alert-ok',
                onTap: function(e) {
                  errorAlert.close();

                }
              }]
            });
           });
          }

        }
      }
      catch(err) {
        var errorAlert = $ionicPopup.alert({
         title: 'Kidoro Health',
         template: savedlang.sometngwentwrng,
         cssClass:'custom-popup-alert',
         buttons: [{
          text: savedlang.okTxt,
          type: 'button-alert-ok',
          onTap: function(e) {
            errorAlert.close();

          }
        }]
      });

      }

    })

    //************************************************************Kidoro Health Patient****************************************************************//
    // Method Name:FeedContentCtrl
    // Developed By: TarantulaLabs
    // Dependencies:$scope,rssPassDataService, $ionicPopup
    // Description:
    //************************************************************Kidoro Health Patient****************************************************************//


.controller('FeedContentCtrl', function ($scope,rssPassDataService, $ionicPopup) {
  try {

          //  localization using localStorage --onMenuToggle

          if(typeof analytics !== 'undefined') { analytics.trackView("FeedContentCtrl"); }

          $scope.initEvent = function() {
            if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
          }

          selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
          if(selectedLang==null){
            selectedLang="en";
            localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
          }
          $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

            //  localization using localStorage

            $scope.OtherShare=function(content, feedtitle, feedurl){

              window.plugins.socialsharing.share(null, feedtitle, null, feedurl);
            }

            $scope.feedContent = rssPassDataService.getData();

            $scope.title = $scope.feedContent.title;


            $scope.feedContent.content = $scope.feedContent.content.replace(/<\s*img [^\>]*src\s*=\s*(["\'])(.*?)\1*?>/,'');


          }
          catch(err) {
            var errorAlert = $ionicPopup.alert({
             title: 'Kidoro Health',
             template: savedlang.sometngwentwrng,
             cssClass:'custom-popup-alert',
             buttons: [{
              text: savedlang.okTxt,
              type: 'button-alert-ok',
              onTap: function(e) {
                errorAlert.close();

              }
            }]
          });

          }

        })
        //************************************************************Kidoro Health Patient****************************************************************//
        // Method Name:DoctorListCtrl
        // Developed By: TarantulaLabs
        // Dependencies:$scope, $ionicGesture, $ionicPopup, $ionicScrollDelegate, $timeout,$kinvey,$filter,$ionicHistory,$state,$ionicModal,$rootScope, $location,saveParentState,docDataService,locDataService,$ionicLoading
        // Description: Contains the list of doctors in gandhinagr & ahmedabad
        //************************************************************Kidoro Health Patient****************************************************************//

.controller('DoctorListCtrl', function ($scope, $ionicGesture, $ionicPopup, $ionicScrollDelegate, $timeout,$kinvey,$filter,$ionicHistory,$state,$ionicModal,$rootScope, $location,saveParentState,docDataService,locDataService,$ionicLoading) {


  try {


    if(typeof analytics !== 'undefined') { analytics.trackView("DoctorListCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }

    $scope.gyno = true;
    $scope.paed = false;

    $scope.onGesture = function(gesture) {
      $scope.gestureUsed = gesture;
      if ($scope.gestureUsed == 'leftSwipe' && $scope.gyno == true && $scope.paed == false) {
        $scope.gyno = false;
        $scope.paed = true;
        $scope.showPdrtcn();

      } else if ($scope.gestureUsed == 'rightSwipe' && $scope.gyno == false && $scope.paed == true) {
        $scope.gyno = true;
        $scope.paed = false;
        $scope.showgynlst();

      }
    }


  //  localization using localStorage --onMenuToggle

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage

    angular.element( document.querySelector( '#btn_gyno' ) ).css('border-bottom','4px solid #fff');
    angular.element( document.querySelector( '#btn_paed' ) ).css('border-bottom','1px solid #009999');
    angular.element( document.querySelector( '#btn_gyno' ) ).css('color','#fff');
    angular.element( document.querySelector( '#btn_paed' ) ).css('color','#037B7B');
    angular.element( document.querySelector( '#btn_gyno' ) ).css('border-left','0px');


    $scope.showgynlst = function() {
      $ionicScrollDelegate.scrollTop();
      angular.element( document.querySelector( '#btn_gyno' ) ).css('border-bottom','4px solid #fff');
      angular.element( document.querySelector( '#btn_paed' ) ).css('border-bottom','1px solid #009999');
      angular.element( document.querySelector( '#btn_gyno' ) ).css('color','#fff');
      angular.element( document.querySelector( '#btn_paed' ) ).css('color','#037B7B');


      $scope.filteredListData=[];
      for (var i = 0; i < $scope.listData.length; i++) {
        if($scope.listData[i].Speciality=="Gynecologist"){
          $scope.filteredListData.push($scope.listData[i]);
        }

      }
    }

    $scope.showPdrtcn = function() {
      $ionicScrollDelegate.scrollTop();
      angular.element( document.querySelector( '#btn_paed' ) ).css('border-bottom','4px solid #fff');
      angular.element( document.querySelector( '#btn_gyno' ) ).css('border-bottom','1px solid #009999');
      angular.element( document.querySelector( '#btn_gyno' ) ).css('color','#037B7B');
      angular.element( document.querySelector( '#btn_paed' ) ).css('color','#fff');
      // angular.element( document.querySelector( '#btn_gyno' ) ).css('border-right','1px solid #009999');
      // angular.element( document.querySelector( '#btn_paed' ) ).css('border-right','0px');
      // angular.element( document.querySelector( '#btn_paed' ) ).css('border-left','0px');

      $scope.filteredListData=[];
      for (var i = 0; i < $scope.listData.length; i++) {
        if($scope.listData[i].Speciality=="Pediatrician"){
          $scope.filteredListData.push($scope.listData[i]);
        }
      }
    }


    $ionicLoading.show({
      template:'<ion-spinner icon="android" class="spinner-energized"></ion-spinner> <span class="loading_cust"> '+$scope.savedlang.loadingdoctrs+' </span>',
    });
    var promise = $kinvey.DataStore.find('KidoroLocation');

    promise.then(function(test) {
      $scope.localityArray = [];
      $scope.specializationArray = [];

      $scope.listData = test;
      $scope.specialization="";
      $scope.qualification="";
      $scope.recommendations;
      for (var i = 0; i < $scope.listData.length; i++) {

                      // locality data

                      if($scope.localityArray.length==0){
                        $scope.localityArray.push({'location':$scope.listData[i].Locality});
                      }
                      else{
                        var match = 0;
                        for(var j =0;j<$scope.localityArray.length;j++){
                          if($scope.localityArray[j].location==$scope.listData[i].Locality){
                            match=1;
                            break;
                          }
                        }
                        if(match==0){
                          $scope.localityArray.push({'location':$scope.listData[i].Locality});
                        }

                      }

                      $scope.selectedlocality= $scope.savedlang.alllocality;

                      // locality data


                      if (($scope.listData[i].Doc_Pic == undefined) || ($scope.listData[i].Doc_Pic == null)) {
                        $scope.listData[i]['docPic'] = "./img/noimage2.png";
                      }
                      else{
                        $scope.listData[i]['docPic'] = $scope.listData[i].Doc_Pic;
                      }

                      if ($scope.listData[i].Location_Cover == null || $scope.listData[i].Location_Cover == undefined) {
                        $scope.listData[i]['locCover'] = "./img/cover3.png";
                      } else {
                        $scope.listData[i]['locCover'] = $scope.listData[i].Location_Cover;
                      }

                      if ($scope.listData[i].Specialization != null) {
                        $scope.listData[i].Specialization = $scope.listData[i].Specialization.replace("^", ", ");
                      }

                      if ($scope.listData[i].Speciality != null) {
                        $scope.listData[i].Speciality = $scope.listData[i].Speciality;
                      }




                      if ($scope.listData[i].Qualification != null) {
                        $scope.qualification = $scope.listData[i].Qualification.split("^");
                        $scope.listData[i]["degree"]="";
                        for (var j = 0; j< $scope.qualification.length; j++) {
                          if($scope.qualification[j].length>0){
                            $scope.listData[i]["degree"] = $scope.listData[i]["degree"]+$scope.qualification[j].split(" ",1)+", ";
                          }
                        };
                        $scope.listData[i]["degree"] =  $scope.listData[i]["degree"].substring(0,  $scope.listData[i]["degree"].length - 2);
                        if ($scope.listData[i].Recommendations != null) {
                         $scope.recommendations=$scope.listData[i].Recommendations.split("^").length;
                         $scope.listData[i]["recommendationsCount"]= $scope.recommendations;
                       }
                     }

                     $scope.listData[i]["locationDays"] = "";

                     var arrD = new Array("MON","TUE","WED","THU","FRI","SAT","SUN");
                     for (var j = 0; j < $scope.listData[i].Timing.length; j++) {
                       $scope.listData[i].locationDays=$scope.listData[i].locationDays+($scope.listData[i].Timing[j].days)+"|";
                       $scope.listData[i].locationDays=$scope.listData[i].locationDays.replace("|",'\n');
                       for (var k = 0; k < $scope.listData[i].Timing[j].binaryArray.length; k++) {

                         if($scope.listData[i].Timing[j].binaryArray[k]==1){
                           for (var l = 0; l < $scope.listData[i].Timing[j].timing.length; l++) {
                             if(l==0){
                              $scope.listData[i][arrD[k]]=$scope.listData[i].Timing[j].timing[l].fromTime+" - "+$scope.listData[i].Timing[j].timing[l].toTime;
                            }
                            else{
                              $scope.listData[i][arrD[k]]=$scope.listData[i][arrD[k]]+" | "+$scope.listData[i].Timing[j].timing[l].fromTime+" - "+$scope.listData[i].Timing[j].timing[l].toTime;
                            }
                          };
                        }

                        else {
                          if($scope.listData[i][arrD[k]]==undefined){
                          $scope.listData[i][arrD[k]]="closed";
                          }


                       }
                     };
                   };
                   $scope.showgynlst();
                   $ionicLoading.hide();
                 };
               },
               function(err) {
                $ionicLoading.hide();
                var errorAlert = $ionicPopup.alert({
                  title: 'Kidoro Health',
                  template: $scope.savedlang.sometngwentwrng,
                  cssClass:'custom-popup-alert',
                  buttons: [{
                   text: $scope.savedlang.okTxt,
                   type: 'button-alert-ok',
                   onTap: function(e) {
                     errorAlert.close();

                   }
                 }]
               });

              });

$scope.sendData = function(data) {
 docDataService.setData(data);
 $state.go('menu.docdetails');
}


$scope.instantBook=function(data){
  var user = $kinvey.getActiveUser()

  docDataService.setData(data);

  if(user.username == "TarantulaAdmin"){
    saveParentState.setState('menu.calendar');
    $ionicHistory.nextViewOptions({
      disableBack: false
    });
    $state.go('menu.loginHome');
  }else{
    $state.go('menu.calendar');
  }
}

$scope.submitToDoctorList = function(item) {
  $scope.modal.searchText = "";
  $scope.selectedlocality=item.location;
  $scope.modal.hide();
};

$scope.selectAllLocality = function(language) {
  $scope.modal.searchText = "";
  $scope.selectedlocality=language.alllocality;
  $scope.modal.hide();
}


$ionicModal.fromTemplateUrl('templates/modalLocation.html', {
  scope: $scope,
  animation: 'slide-in-up',
  backdropClickToClose: false,
  hardwareBackButtonClose: false,
  focusFirstInput: true
}).then(function(modal) {

  $scope.modal = modal;
  $scope.modal.searchText = "";

})
$scope.clearSearch = function() {
  $scope.modal.searchText = "";

}

$scope.openModal = function() {
  $scope.modal.show();
}

$scope.closeModal = function() {
  $scope.modal.searchText = "";
  $scope.modal.hide();
};

$scope.$on('$destroy', function() {
  $scope.modal.remove();
});

    // locationModal by saras --end

    // Specialization filter Modal --start

    $ionicModal.fromTemplateUrl('templates/splfltrmodal.html', {
      scope: $scope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
      hardwareBackButtonClose: false,
      focusFirstInput: true
    }).then(function(filtermodal) {

      $scope.filtermodal = filtermodal;
      $scope.filtermodal.searchDoc = "";

    })


    $scope.openDocSplFiltrModal = function() {

      $scope.filtermodal.show();
    }

    $scope.clearSearchDoctor = function() {
     $scope.filtermodal.searchDoc = "";

   }

   $scope.closeDocSplFiltrModal = function() {
    $scope.filtermodal.searchDoc = "";
    $scope.filtermodal.hide();
  };

  $scope.sendSearchData = function(data) {

    docDataService.setData(data);
    $scope.filtermodal.searchDoc = "";

    $scope.filtermodal.hide();

    $timeout(function(){
      $state.go('menu.docdetails');
    },500);

  }



  $scope.$on('$destroy', function() {
    $scope.filtermodal.remove();
  });

// Specialization filter Modal --end


} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});

}

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:DoctorDetailsCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, privateMessage, $ionicLoading, ionicToast, $ionicScrollDelegate, $cordovaKeyboard, modalService, $ionicModal, $ionicNavBarDelegate, $ionicPopup, $location,$kinvey,userDataService, docDataService,locDataService,$state, $ionicHistory, saveParentState
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('DoctorDetailsCtrl', function ($scope, privateMessage, $ionicLoading, ionicToast, $ionicScrollDelegate, $cordovaKeyboard, modalService, $ionicModal, $ionicNavBarDelegate, $ionicPopup, $location,$kinvey,userDataService, docDataService,locDataService,$state, $ionicHistory, saveParentState) {

  try {


   // localization using localStorage --onMenuToggle
   if(typeof analytics !== 'undefined') { analytics.trackView("DoctorDetailsCtrl"); }

   $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage

    $scope.$on('$ionicView.afterEnter', function(){
      $scope.hideBar = "true";
    });

    $scope.selctedDoctorDetails = docDataService.getData();

           // private chat with doctors
           var currentUser = $kinvey.getActiveUser()



           $scope.mapAddress = $scope.selctedDoctorDetails.Address.replace(/[#^']/,'');

           var arrD = new Array("SUN","MON","TUE","WED","THU","FRI","SAT");
           var da = new Date();
           var datime = da.getDay();
           $scope.timingDay = $scope.selctedDoctorDetails[arrD[datime]];


           if($scope.selctedDoctorDetails.Recommendations!=null){
            $scope.selctedDoctorDetails.Recommendations = $scope.selctedDoctorDetails.Recommendations.replace("^", '<br/>-');
          }


          $scope.showFullImage = function(imageUrl) {
            if (imageUrl == null || imageUrl === undefined) {
            } else {
              modalService.init('templates/image-modal.html',imageUrl, $scope).then(function(modal) {
                modal.show();
              });
            }
          }

          $scope.GoToLink = function (url) {
            window.open(url,'_system');
          }
// show more and less jquery
$(".show-more a").on("click", function() {
  var $link = $(this);
  var $content = $link.parent().prev("div.text-content");
  var linkText = $link.text();

  $content.toggleClass("short-text, full-text");

  $link.text(getShowLinkText(linkText));

  return false;
});

function getShowLinkText(currentText) {
  var newText = '';

  if (currentText.toUpperCase() === "SHOW MORE") {
    newText = "Show fewer";
  } else {
    newText = "Show more";
  }

  return newText;
}

// show more and less jqueryy

var limitStep = 2;
$scope.limit = limitStep;
$scope.showMore = true;
$scope.incrementLimit = function() {
  $scope.limit = $scope.selctedDoctorDetails.Services.length;
  $scope.showMore = false;
};
$scope.decrementLimit = function() {
  $scope.limit = limitStep;
  $scope.showMore = true;
};

var limitFeaturesStep = 2;
$scope.limitFeature = limitFeaturesStep;
$scope.showMoreFeature = true;
$scope.incrementFeatureLimit = function() {
  $scope.limitFeature = $scope.selctedDoctorDetails.Special_Features.length;
  $scope.showMoreFeature = false;
};
$scope.decrementFeatureLimit = function() {
  $scope.limitFeature = limitFeaturesStep;
  $scope.showMoreFeature = true;
};




$scope.sendData = function() {
  var user = $kinvey.getActiveUser()
  docDataService.setData($scope.selctedDoctorDetails);
  if(user.username == "TarantulaAdmin"){
    saveParentState.setState('menu.calendar');
    $ionicHistory.nextViewOptions({
      disableBack: false
    });
    $state.go('menu.loginHome');
  }else{
    $state.go('menu.calendar');
  }
}


} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:CalendarController
// Developed By: TarantulaLabs
// Dependencies:$scope,$compile, $timeout, $ionicModal, ionicToast, $ionicActionSheet, $ionicPopup, $ionicLoading, docDataService,locDataService,saveCouponData,$kinvey, $state, $document, appointmentCnfDataService,userDataService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("CalendarController", function($scope,$compile, $timeout, $ionicModal, ionicToast, $ionicActionSheet, $ionicPopup, $ionicLoading, docDataService,locDataService,saveCouponData,$kinvey, $state, $document, appointmentCnfDataService,userDataService) {

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("CalendarController"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }


  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage

    $ionicModal.fromTemplateUrl('templates/kidoroCouponModal.html', {
      scope: $scope,
      backdropClickToClose: false
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.couponCode = null;
    });

    $scope.openModal = function(){
      $scope.couponCode = null;
      $scope.modal.show();
    }

    $scope.closeModal = function(){
      $scope.couponCode = null;
      $scope.modal.hide();
    }


    //  show/hide div Mom Tab/ Kidoro Tab/ Family Tab and CSS on them

    $scope.div1 = true;
    angular.element( document.querySelector( '#btn_mrng' ) ).css('border-bottom-color','#009999');
    angular.element( document.querySelector( '#btn_mrng' ) ).css('border-radius','0px');
    angular.element( document.querySelector( '#btn_mrng' ) ).css('color','#009999');
    angular.element( document.querySelector( '#btn_mrng' ) ).css('font-size','13px');
    angular.element( document.querySelector( '#btn_aftn' ) ).css('font-size','13px');
    angular.element( document.querySelector( '#btn_evng' ) ).css('font-size','13px');


    $scope.close = $scope.savedlang.closedDctrnotavlablemsg;
    $scope.showMorning = function(){
      $scope.close = $scope.savedlang.closedDctrnotavlablemsg;
      $scope.div1 = true;
      $scope.div2 = false;
      $scope.div3 = false;
      angular.element( document.querySelector( '#btn_mrng' ) ).css('border-bottom-color','#009999');
      angular.element( document.querySelector( '#btn_mrng' ) ).css('border-radius','0px');
      angular.element( document.querySelector( '#btn_mrng' ) ).css('color','#009999');
      angular.element( document.querySelector( '#btn_aftn' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_aftn' ) ).css('color','#ccc');
      angular.element( document.querySelector( '#btn_evng' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_evng' ) ).css('color','#ccc');
    };

    $scope.showAfternoon = function(){
      $scope.close = $scope.savedlang.closedDctrnotavlablemsg;

      $scope.div1 = false;
      $scope.div2 = true;
      $scope.div3 = false;
      angular.element( document.querySelector( '#btn_mrng' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_mrng' ) ).css('color','#ccc');
      angular.element( document.querySelector( '#btn_aftn' ) ).css('border-bottom-color','#009999');
      angular.element( document.querySelector( '#btn_aftn' ) ).css('color','#009999');
      angular.element( document.querySelector( '#btn_evng' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_evng' ) ).css('color','#ccc');
    };

    $scope.showEvening = function(){
      $scope.close = $scope.savedlang.closedDctrnotavlablemsg;

      $scope.div1 = false;
      $scope.div2 = false;
      $scope.div3 = true;
      angular.element( document.querySelector( '#btn_mrng' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_mrng' ) ).css('color','#ccc');
      angular.element( document.querySelector( '#btn_aftn' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_aftn' ) ).css('color','#ccc');
      angular.element( document.querySelector( '#btn_evng' ) ).css('border-bottom-color','#009999');
      angular.element( document.querySelector( '#btn_evng' ) ).css('border-radius','0px');
      angular.element( document.querySelector( '#btn_evng' ) ).css('color','#009999');
    };


    function maxDays(mm, yyyy){
      var mDay;
      if((mm == 3) || (mm == 5) || (mm == 8) || (mm == 10)){
        mDay = 30;
      }
      else{
        mDay = 31
        if(mm == 1){
          if (yyyy/4 - parseInt(yyyy/4) != 0){
            mDay = 28
          }
          else{
            mDay = 29
          }
        }
      }
      return mDay;
    }
    function pad(d) {
      return (d < 10) ? '0' + d.toString() : d.toString();
    }

    function contains(a, obj) {
      for (var i = 0; i < a.length; i++) {
        if (a[i] === obj) {
          return true;
        }
      }
      return false;
    }
    var now = new Date();

    var dd = now.getDate();
    var mm = now.getMonth();
    var dow = now.getDay();
    var yyyy = now.getFullYear();
    var arrM = new Array("January","February","March","April","May","June","July","August","September","October","November","December")
    var arrY = new Array();
    for (ii=0;ii<=4;ii++){
      arrY[ii] = yyyy - 2 + ii
    }
    var arrD = new Array("SUN","MON","TUE","WED","THU","FRI","SAT");
    var arrDFull = new Array("Sunday","Monday","Tueday","Wednesday","Thursday","Friday","Saturday");


    var currM = mm;
    var prevM;
    if (currM!=0){
      prevM = currM - 1
    }
    else{
      prevM = 11
    }

    var currY = yyyy;
    var mmyyyy = new Date();
    mmyyyy.setFullYear(currY);
    mmyyyy.setMonth(currM);


    var day1 = mmyyyy.getDay();


    var cal = [];
    var noOfDays = maxDays(currM,currY);
    var date=mmyyyy.getDate();

    for (ii=0;ii<7;ii++){

      cal.push({'date':date,'day':arrD[day1],'month':currM,'year':currY});
      day1++;
      if(day1>6){
        day1 =0;
      }
      date++;
      if(date>noOfDays){
        date=1;
        currM++;
      }
    }

    $scope.tempCal=cal;
    for (var i=0;i<cal.length;i++) {

      var dateString = cal[i].date+"."+cal[i].month;
      // var eventDateString = cal[i].date+","+cal[i].month;
      var btnhtml = '<td><center><p class = "cust_day_booking">'+cal[i].day+'</p><p id="day'+i+'" class = "activeDay" ng-class = "{activatedDay:'+dateString+'==selectedDay}" ng-click="createEvent('+(cal[i].date+","+cal[i].month)+')"> '+cal[i].date+'</p></center> </td>';
      var temp = $compile(btnhtml)($scope);
      angular.element(document.getElementById('myTr')).append(temp);
    }

    var getTimeFromString = function(timeString){

      var tString = timeString.trim().split(" ");

      var min;
      if(tString[1]=="AM"){
       var timeValString = tString[0].split(":");

       if(Number(timeValString[0])==12){
         min = ((Number(timeValString[0])-12)*60)+(Number(timeValString[1]));
       }
       else{
         min = ((Number(timeValString[0]))*60)+(Number(timeValString[1]));
       }

     }
     else{
       var timeValString = tString[0].split(":");

       if(Number(timeValString[0])==12){
         min = ((Number(timeValString[0]))*60)+(Number(timeValString[1]));
       }
       else{
         min = ((Number(timeValString[0])+12)*60)+(Number(timeValString[1]));
       }

     }

     return min;

   }

   $scope.selectedDay = null;
   $scope.createEvent = function(eventDate,eventMonth) {

    $ionicLoading.show({
    })

    $scope.selectedDay = eventDate+"."+eventMonth;

    var dateId=null;

    for (var i = 0; i < $scope.tempCal.length; i++) {
      if($scope.tempCal[i].date==eventDate.toString()){
        dateId="day"+i;
        break;
      }
    }

    var queryResult = $document[0].getElementById(dateId);


    var dateSubsting = event.toString().split('.');
    var da = new Date();
    da.setDate(Number(eventDate));
    da.setMonth(Number(eventMonth));


    $scope.dateLabel = arrDFull[da.getDay()]+"  -  "+da.getDate()+" "+arrM[da.getMonth()]+", "+da.getFullYear();

    $scope.LocationDataArray=docDataService.getData();


    if($scope.LocationDataArray[arrD[da.getDay()]]!="closed" && $scope.LocationDataArray[arrD[da.getDay()]]!=null ){
      var selectedDateString = da.getDate().toString()+"-"+pad(da.getMonth()+1)+"-"+da.getFullYear().toString();
      var query = new $kinvey.Query();
      query.equalTo('Location_ID',$scope.LocationDataArray['Location_ID']).and().equalTo('Appointment_Date', selectedDateString);
      var promise = $kinvey.DataStore.find('KidoroAppointment', query);
      promise.then(function(models) {
        var reservedTimeSlots=[];
        for (var i = models.length - 1; i >= 0; i--) {
              if(models[i].Appointment_Status=="Confirmed"){      //Maanik Changes
                reservedTimeSlots.push(models[i].Appointment_Time);
              }
            };

            populateTimeGrid(reservedTimeSlots,da,selectedDateString);

          }, function(err) {
            $ionicLoading.hide();

            var errorAlert = $ionicPopup.alert({
              title: 'Kidoro Health',
              template: $scope.savedlang.sometngwentwrng,
              cssClass:'custom-popup-alert',
              buttons: [{
               text: $scope.savedlang.okTxt,
               type: 'button-alert-ok',
               onTap: function(e) {
                 errorAlert.close();

               }
             }]
           });

          });
    }

    else {
      $scope.close = $scope.savedlang.closedDctrnotavlablemsg;
      $scope.morningTimeArray=[];
      $scope.afternoonTimeArray=[];
      $scope.eveninhTimeArray=[];
      $ionicLoading.hide();

    }


  }


  var populateTimeGrid = function(reservedTimeSlots,da,selectedDateString){
    $scope.LocationDataArray=docDataService.getData();
    var range=[];
    var startTimeString;
    var endTimeString;
    $scope.morningTimeArray =[];
    $scope.afternoonTimeArray =[];
    $scope.eveninhTimeArray = [];
    var startTimeinMins;
    var endTimeinMins;

    if($scope.LocationDataArray[arrD[da.getDay()]]!="closed" && $scope.LocationDataArray[arrD[da.getDay()]]!=null ){
     var timeRange = $scope.LocationDataArray[arrD[da.getDay()]].split("|");
     var timeSlot = $scope.LocationDataArray.Timing[0].time_slot.split(" ")[0];
     var timeSlotInMins = Number(timeSlot);

     for (var j = 0; j <timeRange.length; j++) {
      range = timeRange[j].split("-");
      startTimeString = range[0];
      endTimeString = range[1];

      startTimeinMins=getTimeFromString(startTimeString);
      endTimeinMins=getTimeFromString(endTimeString);

      var dateCurrent = new Date();
      var currentStartTime = (dateCurrent.getHours()*60)+dateCurrent.getMinutes();
      var extraMin=currentStartTime%timeSlotInMins;
      if(extraMin>0){
        currentStartTime=((currentStartTime/timeSlotInMins)*timeSlotInMins)+timeSlotInMins;
        currentStartTime=currentStartTime-extraMin;
      }
      if(startTimeinMins<=currentStartTime && da.getDate() ==  dateCurrent.getDate()){

        startTimeinMins=currentStartTime;
      }

      for (var time = startTimeinMins; time < endTimeinMins; time=time+timeSlotInMins) {
        if (time < (12*60)) {
          var timeToinsert = Math.floor(time/60).toString()+":"+pad(Math.floor(time%60).toString());
          if(!contains(reservedTimeSlots,timeToinsert)){
            $scope.morningTimeArray.push({'time': timeToinsert,
              'Location_ID' :$scope.LocationDataArray['Location_ID'],
              'Hospital_Name':$scope.LocationDataArray['Hospital_Name'],
              'Appointment_Date':selectedDateString,
              'timeId':"AM"
            });
          }
        }
        if (time>=(12*60) && time < (13*60)) {
          var timeToinsert = Math.floor((time)/60).toString()+":"+pad(Math.floor((time)%60).toString());
          if(!contains(reservedTimeSlots,timeToinsert)){
            $scope.afternoonTimeArray.push({'time': timeToinsert,
              'Location_ID' :$scope.LocationDataArray['Location_ID'],
              'Hospital_Name':$scope.LocationDataArray['Hospital_Name'],
              'Appointment_Date':selectedDateString,
              'timeId':"PM"
            });
          }

        }
        if (time>=(13*60) && time < (17*60)) {
          var timeToinsert = Math.floor(((time)/60)-12).toString()+":"+pad(Math.floor((time)%60).toString());
          if(!contains(reservedTimeSlots,timeToinsert)){
            $scope.afternoonTimeArray.push({'time': timeToinsert,
              'Location_ID' :$scope.LocationDataArray['Location_ID'],
              'Hospital_Name':$scope.LocationDataArray['Hospital_Name'],
              'Appointment_Date':selectedDateString,
              'timeId':"PM"
            });
          }
        }
        if(time>(17*60))
        {


          var timeToinsert = Math.floor(((time)/60)-12).toString()+":"+pad(Math.floor((time)%60).toString());
          if(!contains(reservedTimeSlots,timeToinsert)){
            $scope.eveninhTimeArray.push({'time': timeToinsert,
              'Location_ID' :$scope.LocationDataArray['Location_ID'],
              'Hospital_Name':$scope.LocationDataArray['Hospital_Name'],
              'Appointment_Date':selectedDateString,
              'timeId':"PM"
            });
          }

        }

      };


    };

  }

  $ionicLoading.hide();
}

function zeroPadding(n) {
  if(n<10)
    return ("000" + n);
  else if (n>9 && n<100)
    return ("00" + n);
  else if (n>99 && n<1000)
    return("0" + n);
  else return(n);
}


var da = new Date();
var defaultObj = da.getDate().toString()+","+da.getMonth().toString();

$scope.createEvent(da.getDate().toString(),da.getMonth().toString());

$scope.selectedTime = null;

$scope.timeSelect = function(time){

  $scope.selectedTime=time;

}

$scope.confirmBooking=function(){
 var user = userDataService.getData();
 if($scope.selectedTime==null)
 {

  var myPopup = $ionicPopup.alert({
    title: $scope.savedlang.oops,
    template: $scope.savedlang.selecttimeslot,
    cssClass:'custom-popup-alert',
    buttons: [{
     text: $scope.savedlang.okTxt,
     type: 'button-alert-ok',
     onTap: function(e) {
       myPopup.close();

     }
   }]
 });

  $ionicLoading.hide();

} else {
//----------------------------------------CuponCode-------------------------------------------------------
$scope.openModal();
$scope.sendcouponCode = function(code) {
 $scope.couponCode = code;
 if ($scope.couponCode == null || $scope.couponCode == undefined || $scope.couponCode == "") {
   $scope.couponCode = null;
   $scope.finalSubmit();
   $scope.modal.hide();
 } else {
   var query = new $kinvey.Query();
   query.equalTo('coupon_code', $scope.couponCode);
   $ionicLoading.show({});
   var promise = $kinvey.DataStore.find('KidoroCoupon', query);
   promise.then(function(coupon) {
    $ionicLoading.hide();
    if (coupon.length == 0) {
      ionicToast.show($scope.savedlang.invalidcouponcode, 'bottom', false, 1500);

    } else {

      $scope.couponData = coupon[0];
if ($scope.couponData.used_by==null){
  $scope.couponData.used_by=[];
}
      if ($scope.couponData.used_by.indexOf(user.Kidoro_Code)==-1) {
        var currentDate = new Date();
        var current_ms = currentDate.getTime();


        var valid_to = $scope.couponData.valid_to.split('-');
        var temp_valid_to = new Date();
        temp_valid_to.setDate(Number(valid_to[0]));
        temp_valid_to.setMonth((Number(valid_to[1]))-1);
        temp_valid_to.setFullYear(Number(valid_to[2]));
        temp_valid_to.setHours(23);
        temp_valid_to.setMinutes(59);
        temp_valid_to.setSeconds(59);
        var temp_valid_to_ms = temp_valid_to.getTime();

        var valid_from = $scope.couponData.valid_from.split('-');
        var temp_valid_from = new Date();
        temp_valid_from.setDate(Number(valid_from[0]));
        temp_valid_from.setMonth((Number(valid_from[1]))-1);
        temp_valid_from.setFullYear(Number(valid_from[2]));
        temp_valid_from.setHours(0);
        temp_valid_from.setMinutes(0);
        temp_valid_from.setSeconds(0);
        var temp_valid_from_ms = temp_valid_from.getTime();
        var usedBy=[];
        if(coupon.used_by==null||coupon.used_by==undefined){
          usedBy.push(user.Kidoro_Code);
        }
        else{
          usedBy=coupon.used_by;
          usedBy.push(user.Kidoro_Code);
        }

        if ((temp_valid_to_ms>current_ms) && (current_ms>temp_valid_from_ms) && $scope.couponData.type==$scope.savedlang.couponTypeDoctor ) {

                            // ionicToast.show("Applicable", 'bottom', false, 1500);
                            var updatedCouponData = {
                              _id: $scope.couponData._id,
                              coupon_id: $scope.couponData.coupon_id,
                              coupon_code: $scope.couponData.coupon_code,
                              valid_to: $scope.couponData.valid_to,
                              valid_from: $scope.couponData.valid_from,
                              discount: $scope.couponData.discount,
                              used_by: usedBy,
                              type:$scope.couponData.type
                            }
                            var promise = Kinvey.DataStore.save('KidoroCoupon', updatedCouponData);
                            promise.then(function(entity) {
                             $scope.modal.hide();
                             var res = $scope.savedlang.discountmessage.replace("%", updatedCouponData.discount);
                             var Alert = $ionicPopup.alert({
                              title: 'Kidoro Health',
                              template: res,
                              cssClass:'custom-popup-alert',
                              buttons: [{
                               text: $scope.savedlang.okTxt,
                               type: 'button-alert-ok',
                               onTap: function(e) {
                                 Alert.close();
          $scope.finalSubmit();

        }
      }]
    });


                           }, function(error) {
                           });

                          } else {
                            ionicToast.show($scope.savedlang.invalidcouponcode, 'bottom', false, 1500);

                          }

                        } else {
                          ionicToast.show($scope.savedlang.couponcodealrdyused, 'bottom', false, 1500);
                        }

                      }

                    }, function(err) {
                      $ionicLoading.hide();
                      $scope.modal.hide();

                    });

 }
}


// appointment Id generation

$scope.finalSubmit = function() {
  $ionicLoading.show();
saveCouponData.setData($scope.couponData);
var discount;
              if ($scope.couponData == null || $scope.couponData == undefined || $scope.couponData == "") {
                discount = "No discount available";
              }else{
                discount = $scope.couponData.discount;
              }
  var totalCount;
  var promise = $kinvey.DataStore.count('KidoroAppointment');
  promise.then(function(response) {
    $scope.totalCount = ++response;

    $scope.count = "AP"+zeroPadding($scope.totalCount);

    var promise = $kinvey.DataStore.save('KidoroAppointment', {

      Appointment_ID: $scope.count,
      Doctor_ID : $scope.LocationDataArray.Doctor_ID,
      Location_ID : $scope.selectedTime.Location_ID,
      Appointment_by : user.Kidoro_Code,
      Hospital_Name : $scope.selectedTime.Hospital_Name,
      Appointment_Time : $scope.selectedTime.time,
      Appointment_Time_Id: $scope.selectedTime.timeId,
      Doctor_Name : $scope.LocationDataArray.Doc_Name,
      Appointment_Date : $scope.selectedTime.Appointment_Date,
      User_ID : user.Kidoro_Code,
      Appointment_Status : "Confirmed",
      cancelled_by : null,
      Kidoro_Discount:discount,
      Coupon_Applied: $scope.couponCode

    });

    promise.then(function(model) {

      $ionicLoading.hide();
      if(model.message!="Error"){
        appointmentCnfDataService.setData(model);
        $state.go('menu.cnfAppointment');
      }
      else{
        alert("Time Slot Already Taken");
      }


    }, function(err) {
      $ionicLoading.hide()

      var errorAlert = $ionicPopup.alert({
        title: 'Kidoro Health',
        template: $scope.savedlang.sometngwentwrng,
        cssClass:'custom-popup-alert',
        buttons: [{
         text: $scope.savedlang.okTxt,
         type: 'button-alert-ok',
         onTap: function(e) {
           errorAlert.close();


         }
       }]
     });

    });

  }, function(err) {
    $ionicLoading.hide();
    var errorAlert = $ionicPopup.alert({
      title: 'Kidoro Health',
      template: $scope.savedlang.sometngwentwrng,
      cssClass:'custom-popup-alert',
      buttons: [{
       text: $scope.savedlang.okTxt,
       type: 'button-alert-ok',
       onTap: function(e) {
         errorAlert.close();


       }
     }]
   });
  });


}

}
}

} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:profileCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $ionicLoading, $ionicGesture, $ionicPopup, $ionicPopup, $timeout, $cordovaCamera, $ionicSideMenuDelegate, $ionicPlatform, $document, $filter, $kinvey,locationDataService, userDataService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("profileCtrl", function($scope, $ionicLoading, $ionicGesture, $ionicPopup, $ionicPopup, $timeout, $cordovaCamera, $ionicSideMenuDelegate, $ionicPlatform, $document, $filter, $kinvey,locationDataService, userDataService){

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("profileCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


    //  localization using localStorage


    $scope.user = userDataService.getData();

    $scope.imgURI = $scope.user.Profile_Pic_URI;


    var query = new $kinvey.Query();
    query.equalTo('Kidoro_Code', $scope.user.Relationship_ID.kidoroCode);
    var promise = $kinvey.DataStore.find('KidoroUser', query);

    promise.then(function(models) {

      $scope.partnerData = models[0];

    }, function(err) {

      var errorAlert = $ionicPopup.alert({
        title: 'Kidoro Health',
        template: $scope.savedlang.sometngwentwrng,
        cssClass:'custom-popup-alert',
        buttons: [{
          text: $scope.savedlang.okTxt,
          type: 'button-alert-ok',
          onTap: function(e) {
           errorAlert.close();


         }
       }]
     });

    });



    $scope.div1 = true;
    angular.element( document.querySelector( '#btn_mom' ) ).css('border-bottom-color','#009999');
    $scope.showMom = function(){
      $scope.div1 = true;
      $scope.div2 = false;
      $scope.div3 = false;
      angular.element( document.querySelector( '#btn_mom' ) ).css('border-bottom-color','#009999');
      angular.element( document.querySelector( '#btn_kid' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_family' ) ).css('border-bottom-color','#fff');
    };

    $scope.showKidoro = function(){
      $scope.div1 = false;
      $scope.div2 = true;
      $scope.div3 = false;
      angular.element( document.querySelector( '#btn_mom' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_kid' ) ).css('border-bottom-color','#009999');
      angular.element( document.querySelector( '#btn_family' ) ).css('border-bottom-color','#fff');
    };

    $scope.showFamily = function(){
      $scope.div1 = false;
      $scope.div2 = false;
      $scope.div3 = true;
      angular.element( document.querySelector( '#btn_mom' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_kid' ) ).css('border-bottom-color','#fff');
      angular.element( document.querySelector( '#btn_family' ) ).css('border-bottom-color','#009999');
    };



    $scope.divedit = true;
    $scope.mom = true;
    $scope.baby = true;
    $scope.partner = true;

    $scope.showEdit = function() {

      $scope.momName = $scope.user.User_Name;
      $scope.momEmail = $scope.user.User_Mail;
      $scope.babyName = $scope.user.Child_Name;


      $scope.divdone=true;
      $scope.divedit = false;

      $scope.mom = false;

      angular.element( document.querySelector( '#mom_itm0' ) ).css('color','#666');
      angular.element( document.querySelector( '#mom_itm1' ) ).css('color','#009999');
      angular.element( document.querySelector( '#mom_itm2' ) ).css('color','#666');
      angular.element( document.querySelector( '#mom_itm3' ) ).css('color','#009999');



      $scope.baby = false;
      angular.element( document.querySelector( '#kid_itm0' ) ).css('color','#666');
      angular.element( document.querySelector( '#kid_itm1' ) ).css('color','#009999');
      angular.element( document.querySelector( '#kid_itm2' ) ).css('color','#666');

      $scope.partner = false;
      angular.element( document.querySelector( '#partner_itm0' ) ).css('color','#666');
      angular.element( document.querySelector( '#partner_itm1' ) ).css('color','#666');
      angular.element( document.querySelector( '#partner_itm2' ) ).css('color','#666');

    };

    $scope.showDone = function() {

      // profile update code

      if (($scope.user.User_Name != $scope.momName) || ($scope.user.User_Mail != $scope.momEmail) || ($scope.user.Child_Name != $scope.babyName)) {

        $ionicLoading.show({});
        var promise = $kinvey.DataStore.save('KidoroUser', {
          _id : $scope.user._id,
          User_ID: $scope.user.User_ID,
          User_Name : $scope.user.User_Name,
          Kidoro_Code : $scope.user.Kidoro_Code,
          User_Phone : Number($scope.user.User_Phone),
          User_Mail : $scope.user.User_Mail,
          Child_Name : $scope.user.Child_Name,
          Child_Date : $scope.user.Child_Date,
          Child_Due_Date: $scope.user.Child_Due_Date,
          Relationship_ID: $scope.user.Relationship_ID,
          Fav_Feeds: $scope.user.Fav_Feeds,
          Kidoro_Mom_Code : $scope.user.Kidoro_Mom_Code,
          Profile_Pic_URI: $scope.user.Profile_Pic_URI,
          Documents: $scope.user.Documents,
          Vaccination_Record:$scope.user.Vaccination_Record

        });
        promise.then(function(model) {
         userDataService.setData(model);
         $ionicLoading.hide();
       }, function(err) {
         $ionicLoading.hide();

         var errorAlert = $ionicPopup.alert({
           title: 'Kidoro Health',
           template: $scope.savedlang.sometngwentwrng,
           cssClass:'custom-popup-alert',
           buttons: [{
            text: $scope.savedlang.okTxt,
            type: 'button-alert-ok',
            onTap: function(e) {
              errorAlert.close();

            }
          }]
        });

       });
      }
      else {

      }

      // profile update code

      $scope.divdone = false;
      $scope.divedit = true;

      $scope.mom = true;
      angular.element( document.querySelector( '#mom_itm0' ) ).css('color','#111');
      angular.element( document.querySelector( '#mom_itm1' ) ).css('color','#111');
      angular.element( document.querySelector( '#mom_itm2' ) ).css('color','#111');
      angular.element( document.querySelector( '#mom_itm3' ) ).css('color','#111');

      $scope.baby = true;
      angular.element( document.querySelector( '#kid_itm0' ) ).css('color','#111');
      angular.element( document.querySelector( '#kid_itm1' ) ).css('color','#111');
      angular.element( document.querySelector( '#kid_itm2' ) ).css('color','#111');

      $scope.partner = true;
      angular.element( document.querySelector( '#partner_itm0' ) ).css('color','#111');
      angular.element( document.querySelector( '#partner_itm1' ) ).css('color','#111');
      angular.element( document.querySelector( '#partner_itm2' ) ).css('color','#111');

    }

    // Camera Code

    $scope.showPopup = function() {
      var myPopup = $ionicPopup.show({
        title: $scope.savedlang.addphoto,

        cssClass: 'custom_popup_profile',
        scope: $scope,
        buttons: [
        {
         text: '<i class="icon ion-camera"></i>',
         type: 'button_camera',
         onTap: function(e) {

          var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: false
          };

          $cordovaCamera.getPicture(options).then(function (imageData) {
            $scope.lastimgURI = "data:image/jpeg;base64," + imageData;

            $ionicLoading.show({});

                                // profile pics update code
                                if($scope.lastimgURI!=undefined){
                                 var promise = $kinvey.DataStore.save('KidoroUser', {
                                  _id : $scope.user._id,
                                  User_ID: $scope.user.User_ID,
                                  User_Name : $scope.user.User_Name,
                                  Kidoro_Code : $scope.user.Kidoro_Code,
                                  User_Phone : Number($scope.user.User_Phone),
                                  User_Mail : $scope.user.User_Mail,
                                  Child_Name : $scope.user.Child_Name,
                                  Child_Date : $scope.user.Child_Date,
                                  Child_Due_Date: $scope.user.Child_Due_Date,
                                  Relationship_ID: $scope.user.Relationship_ID,
                                  Fav_Feeds: $scope.user.Fav_Feeds,
                                  Kidoro_Mom_Code : $scope.user.Kidoro_Mom_Code,
                                  Profile_Pic_URI: $scope.lastimgURI,
                                  Documents: $scope.user.Documents,
                                  Vaccination_Record:$scope.user.Vaccination_Record

                                });
                                 promise.then(function(model) {
                                   userDataService.setData(model);
                                   $ionicLoading.hide();
                                   $scope.imgURI = "data:image/jpeg;base64," + imageData;
                                 }, function(err) {
                                   $ionicLoading.hide();

                                   var errorAlert = $ionicPopup.alert({
                                     title: 'Kidoro Health',
                                     template: $scope.savedlang.sometngwentwrng,
                                     cssClass:'custom-popup-alert',
                                     buttons: [{
                                      text: $scope.savedlang.okTxt,
                                      type: 'button-alert-ok',
                                      onTap: function(e) {
                                        errorAlert.close();


                                      }
                                    }]
                                  });

                                 });
                               }

                             }, function (err) {

                              $ionicLoading.hide();

                            });
          myPopup.close();

        }
      },
      {
       text: '<i class="icon ion-image"></i>',
       type: 'button_camera',
       onTap: function(e) {
        e.preventDefault();

        var options = {
          quality: 75,
          destinationType: Camera.DestinationType.DATA_URL,
          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
          allowEdit: true,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 300,
          targetHeight: 300,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function (imageData) {

          $scope.lastimgURI = "data:image/jpeg;base64," + imageData;

          $ionicLoading.show({});

          if($scope.lastimgURI!=undefined){

           var promise = $kinvey.DataStore.save('KidoroUser', {
            _id : $scope.user._id,
            User_ID: $scope.user.User_ID,
            User_Name : $scope.user.User_Name,
            Kidoro_Code : $scope.user.Kidoro_Code,
            User_Phone : Number($scope.user.User_Phone),
            User_Mail : $scope.user.User_Mail,
            Child_Name : $scope.user.Child_Name,
            Child_Date : $scope.user.Child_Date,
            Child_Due_Date: $scope.user.Child_Due_Date,
            Relationship_ID: $scope.user.Relationship_ID,
            Fav_Feeds: $scope.user.Fav_Feeds,
            Kidoro_Mom_Code : $scope.user.Kidoro_Mom_Code,
            Profile_Pic_URI: $scope.lastimgURI,
            Documents: $scope.user.Documents,
            Vaccination_Record:$scope.user.Vaccination_Record

          });
           promise.then(function(model) {
             userDataService.setData(model);
             $ionicLoading.hide();
             $scope.imgURI = "data:image/jpeg;base64," + imageData;

           }, function(err) {
             $ionicLoading.hide();

             var errorAlert = $ionicPopup.alert({
               title: 'Kidoro Health',
               template: $scope.savedlang.sometngwentwrng,
               cssClass:'custom-popup-alert',
               buttons: [{
                text: $scope.savedlang.okTxt,
                type: 'button-alert-ok',
                onTap: function(e) {
                  errorAlert.close();


                }
              }]
            });


           });
         }
       }, function (err) {

        $ionicLoading.hide();

      });
        myPopup.close();

      }
    },
    {
      text: '<i class="icon ion-close"></i>',
      type: 'button_camera',
      onTap: function(e) {

        myPopup.close();
      }
    }
    ]
  });

};

    // Gallery Image Code

  } catch (err) {
    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }


})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:appointmentCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope,$kinvey, $state, sendSMSServices, $ionicPlatform, $ionicSideMenuDelegate, $ionicPopup, $ionicHistory, $ionicLoading, sendMail, userDataService, appointmentDataService, appointmentDetailsService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("appointmentCtrl", function($scope,$kinvey, $state, sendSMSServices, $ionicPlatform, $ionicSideMenuDelegate, $ionicPopup, $ionicHistory, $ionicLoading, sendMail, userDataService, appointmentDataService, appointmentDetailsService){

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("appointmentCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


    //  localization using localStorage

    $scope.bookAppointment = function() {
      $state.go('menu.doclist');
    }



    $scope.OtherShare=function(docName, hospitalName, aptDate, aptTime, aptTimeId){

      $scope.docName = docName;
      $scope.hospitalName = hospitalName;
      $scope.aptDate = aptDate;
      $scope.aptTime = aptTime;
      $scope.aptTimeId = aptTimeId;
      $scope.subject = "Appointment Details"
      $scope.message = "Hospital Name" + ":" + $scope.hospitalName + "\n" + "Doctor Name" + ":" + $scope.docName + "\n" + "Appointment Date" +":"+ $scope.aptDate + "\n" + "Appointment Time" +":"+ $scope.aptTime + " " +$scope.aptTimeId;

      window.plugins.socialsharing.share($scope.message, $scope.subject, null, null);
    }

    var user = userDataService.getData();

    var activeUser = $kinvey.getActiveUser();
    if(activeUser.username=="TarantulaAdmin"){
      $scope.showLogin = true;
    }
    else{
      $scope.showLogin = false;
      $ionicLoading.show({});
      var query = new $kinvey.Query();

      query.equalTo('User_ID',user.Kidoro_Code);
      var promise = $kinvey.DataStore.find('KidoroAppointment', query);

      promise.then(function(models) {

        $scope.Confirmed = "Confirmed";
        $scope.Cancelled = "Cancelled";
        $scope.Visited = "Visited";
        $ionicLoading.hide();

        if (!models.length) {
          $scope.showError = true;
          $scope.appointmentErrorList = $scope.savedlang.noAptmntexists;

        } else {
          $scope.showError = false;
          $scope.appointmentList = models;

        }

      },
      function(err) {
       $ionicLoading.hide();

       var errorAlert = $ionicPopup.alert({
         title: 'Kidoro Health',
         template: $scope.savedlang.sometngwentwrng,
         cssClass:'custom-popup-alert',
         buttons: [{
          text: $scope.savedlang.okTxt,
          type: 'button-alert-ok',
          onTap: function(e) {
            errorAlert.close();


          }
        }]
      });

     });



      $scope.appointmentDetails = function(data) {
        if (data.Appointment_Status == "Cancelled") {
        } else if(data.Appointment_Status == "Confirmed") {
          appointmentDetailsService.setData(data);
          $state.go('menu.appointmentDetails');
        }

      }

      $scope.goToHome = function() {
        $state.go('menu.home');
      }

      $scope.showConfirm = function(data) {
       var confirmPopup = $ionicPopup.confirm({
         title: $scope.savedlang.cancelApt,
         template: $scope.savedlang.cancelAptconfirmTxt,
         cssClass:'custom-popup',
         buttons: [{
          text: $scope.savedlang.noTxt,
          type: 'button-no',
          onTap: function(e) {

          }
        }, {
          text: $scope.savedlang.yesTxt,
          type: 'button-yes',
          onTap: function(e) {
            $ionicLoading.show({});
            var newData={
              _id:data._id,
              Appointment_ID: data.Appointment_ID,
              Doctor_ID : data.Doctor_ID,
              Location_ID : data.Location_ID,
              Appointment_by : data.Appointment_by,
              Hospital_Name : data.Hospital_Name,
              Appointment_Time : data.Appointment_Time,
              Appointment_Time_Id: data.Appointment_Time_Id,
              Doctor_Name : data.Doctor_Name,
              Appointment_Date : data.Appointment_Date,
              User_ID : data.User_ID,
              Appointment_Status:"Cancelled",
              cancelled_by: data.Appointment_by
                // Coupon_Applied: data.Coupon_Applied,

              };

              var promise = $kinvey.DataStore.save('KidoroAppointment',newData);
              promise.then(function(model) {

                var query1 = new $kinvey.Query();
                query1.equalTo('Doctor_ID',data.Doctor_ID);
                var promise = $kinvey.DataStore.find('KidoroDocs', query1);
                promise.then(function(models) {


                       // Mail to Doctor after appointment is Cancelled //

                       var subject = "Appointment Cancellation Details with" +" "+ user.User_Name +" has been Cancelled";


                       var email = models[0].Doc_Email;
                       var name = user.User_Name;
                       var status = data.Appointment_Status;
                       var id =   data.Appointment_ID;
                       var date =  data.Appointment_Date;
                       var time = data.Appointment_Time + " " + data.Appointment_Time_Id;
                       var cname = data.Hospital_Name;
                       var doctorName = data.Doctor_Name;
                       var template_name = "doctor-appointment";
                       var bccAddress=$scope.savedlang.bccAddress;
                       sendMail.getRespose(subject, template_name, email, name, status, id, date, time, cname, doctorName,null,bccAddress);


                       // Mail to Doctor after appointment is Cancelled //

                     },
                     function(err) {
                       $ionicLoading.hide();

                       var errorAlert = $ionicPopup.alert({
                         title: 'Kidoro Health',
                         template: $scope.savedlang.sometngwentwrng,
                         cssClass:'custom-popup-alert',
                         buttons: [{
                          text: $scope.savedlang.okTxt,
                          type: 'button-alert-ok',
                          onTap: function(e) {
                            errorAlert.close();


                          }
                        }]
                      });

                     });

                 // Send mail to User after User Cancelled the Appointment

                 var subject = "Appointment Cancellation Details with" +" "+ data.Doctor_Name +" has been Cancelled";
                 var email = user.User_Mail;
                 var name =  user.User_Name;
                 var status = data.Appointment_Status;
                 var id =   data.Appointment_ID;
                 var date =  data.Appointment_Date;
                 var time = data.Appointment_Time + " " + data.Appointment_Time_Id;
                 var cname = data.Hospital_Name;
                 var doctorName = data.Doctor_Name;
                 var template_name = "appointment";
                 var bccAddress=$scope.savedlang.bccAddress;
                 sendMail.getRespose(subject, template_name, email, name, status, id, date, time, cname, doctorName,null,bccAddress);

                 // Send mail to User after User Cancelled the Appointment

                 // SMS to User after appointment is Cancelled //

                 var number = user.User_Phone;
                 var messageCnf = "APPOINTMENT CANCELLED." + "\n" + "Appointment ID" + ":   " + data.Appointment_ID + " for " + data.Appointment_Date + " , " + data.Appointment_Time + " " +data.Appointment_Time_Id + " at " + data.Hospital_Name + " with " + data.Doctor_Name +".\n" + $scope.savedlang.aptmessage;
                 sendSMSServices.getRespose(number, messageCnf);

                 // SMS to User after appointment is Cancelled //

                 var index=-1;
                 for (var i = 0; i < $scope.appointmentList.length; i++) {
                   if($scope.appointmentList[i]._id==data._id){
                     index=i;
                     break;
                   }
                 }
                 if(index!=-1){
                   $scope.appointmentList.splice(index,1);

                 }

                      //  reload the view on the same page!
                      $state.transitionTo('menu.appointments', null, {'reload':true});
                       //  reload the view on the same page!
                       $ionicLoading.hide();

                     }, function(err) {
                      $ionicLoading.hide();

                      var errorAlert = $ionicPopup.alert({
                        title: 'Kidoro Health',
                        template: $scope.savedlang.sometngwentwrng,
                        cssClass:'custom-popup-alert',
                        buttons: [{
                         text: $scope.savedlang.okTxt,
                         type: 'button-alert-ok',
                         onTap: function(e) {
                           errorAlert.close();


                         }
                       }]
                     });

                    });
}
}]
});

};

}

} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:appointmentDetailsCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, userDataService, $ionicPopup, appointmentDetailsService, $kinvey, $ionicLoading
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("appointmentDetailsCtrl", function($scope, userDataService, $ionicPopup, appointmentDetailsService, $kinvey, $ionicLoading){
  try {

    //  localization using localStorage --onMenuToggle
    if(typeof analytics !== 'undefined') { analytics.trackView("appointmentDetailsCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }

    selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
    if(selectedLang==null){
      selectedLang="en";
      localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
    }
    $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


      //  localization using localStorage

      $ionicLoading.show({});//An overlay that can be used to indicate activity while blocking user interaction

      $scope.userData = userDataService.getData();

      $scope.aptData = appointmentDetailsService.getData();

    //  Call only for Doctor Clinic Address and Clinic Contact Doctor Contact

    var query = new $kinvey.Query();
    query.equalTo('Location_ID', $scope.aptData.Location_ID);
    var promise = $kinvey.DataStore.find('KidoroLocation', query);

    promise.then(function(models) {
      $ionicLoading.hide();
      $scope.docLocList = models[0];


    },
    function(err) {
     $ionicLoading.hide();

     var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: $scope.savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
       buttons: [{
        text: $scope.savedlang.okTxt,
        type: 'button-alert-ok',
        onTap: function(e) {
          errorAlert.close();


        }
      }]
    });

   });

      //  Call only for Doctor Clinic Address and Clinic Contact Doctor Contact

    } catch (err) {
      var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: $scope.savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
       buttons: [{
        text: $scope.savedlang.okTxt,
        type: 'button-alert-ok',
        onTap: function(e) {
          errorAlert.close();


        }
      }]
    });
    }


  })
  //************************************************************Kidoro Health Patient****************************************************************//
  // Method Name:medicalReportsCtrl
  // Developed By: TarantulaLabs
  // Dependencies:$scope, $state, $kinvey, userDataService, prescriptionService, $ionicLoading, $kinvey, $ionicPopup
  // Description:
  //************************************************************Kidoro Health Patient****************************************************************//

.controller("medicalReportsCtrl", function($scope, $state, $kinvey, userDataService, prescriptionService, $ionicLoading, $kinvey, $ionicPopup){

  try {

    $scope.hideErrorMsg = true;
    //  localization using localStorage --onMenuToggle
    if(typeof analytics !== 'undefined') { analytics.trackView("medicalReportsCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }
    selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
    if(selectedLang==null){
      selectedLang="en";
      localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
    }
    $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

      //  localization using localStorage

      var activeUser = $kinvey.getActiveUser();
      if(activeUser.username=="TarantulaAdmin"){
        $scope.showMedReport = true;
        $scope.hideErrorMsg = false;
        $scope.medReportError = $scope.savedlang.logintoviewmedreprt;

      }
      else{
        $scope.userData = userDataService.getData();
        var query = new $kinvey.Query();
        query.equalTo('Kidoro_Code', $scope.userData.Kidoro_Code);
        var promise = $kinvey.DataStore.find('KidoroChemistOrderDetails', query);
        $ionicLoading.show({});
        promise.then(function(models) {

          if (models.length == 0) {
            $ionicLoading.hide();
            $scope.showMedReport = false;
            $scope.showMedReportAvail = $scope.savedlang.medReportNotAvlble;
          } else {
            $scope.showMedReport = true;
            $ionicLoading.hide();
            $scope.medicalRecords = models;
          }
          $scope.goToMedReportDetail = function(item) {
            $scope.itemArray = item;
            prescriptionService.setData($scope.itemArray);
            $state.go('menu.medicalReportDetail');
          }
        },
        function(err) {
         $ionicLoading.hide();
         var errorAlert = $ionicPopup.alert({
           title: 'Kidoro Health',
           template: $scope.savedlang.sometngwentwrng,
           cssClass:'custom-popup-alert',
           buttons: [{
            text: $scope.savedlang.okTxt,
            type: 'button-alert-ok',
            onTap: function(e) {
              errorAlert.close();
            }
          }]
        });
       });
      }
    } catch (err) {
      var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: $scope.savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
       buttons: [{
        text: $scope.savedlang.okTxt,
        type: 'button-alert-ok',
        onTap: function(e) {
          errorAlert.close();
        }
      }]
    });
    }

  })

  //************************************************************Kidoro Health Patient****************************************************************//
  // Method Name:medicalReportDetailCtrl
  // Developed By: TarantulaLabs
  // Dependencies:$scope, $ionicModal, $ionicSlideBoxDelegate, $ionicLoading, $ionicPopup, prescriptionService
  // Description:
  //************************************************************Kidoro Health Patient****************************************************************//

.controller("medicalReportDetailCtrl", function($scope, $ionicModal, $ionicSlideBoxDelegate, $ionicLoading, $ionicPopup, prescriptionService){

  try {

    //  localization using localStorage --onMenuToggle
    if(typeof analytics !== 'undefined') { analytics.trackView("medicalReportDetailCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }

    selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
    if(selectedLang==null){
      selectedLang="en";
      localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
    }
    $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

      //  localization using localStorage

      $scope.medRepDetails = prescriptionService.getData();
      $scope.images = $scope.medRepDetails.Record.preRecord.pres_link_array;


      $ionicModal.fromTemplateUrl('templates/image-modal-slideshow.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modal = modal;
      });

      $scope.openModal = function() {
        $ionicSlideBoxDelegate.slide(0);
        $scope.modal.show();
      };

      $scope.closeModal = function() {
        $scope.modal.hide();
      };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function() {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
      // Execute action
    });
    $scope.$on('modal.shown', function() {
    });

    // Call this functions if you need to manually control the slides
    $scope.next = function() {
      $ionicSlideBoxDelegate.next();
    };

    $scope.previous = function() {
      $ionicSlideBoxDelegate.previous();
    };

    $scope.goToSlide = function(index) {
      $scope.modal.show();
      $ionicSlideBoxDelegate.slide(index);
    }

    // Called each time the slide changes
    $scope.slideChanged = function(index) {
      $scope.slideIndex = index;
    };


  } catch (err) {

    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }
})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:aboutUsCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $ionicPlatform, $ionicPopup, $ionicSideMenuDelegate
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("aboutUsCtrl", function($scope, $ionicPlatform, $ionicPopup, $ionicSideMenuDelegate){


  try {

          //  localization using localStorage --onMenuToggle
          if(typeof analytics !== 'undefined') { analytics.trackView("aboutUsCtrl"); }

          $scope.initEvent = function() {
            if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
          }

          selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
          if(selectedLang==null){
            selectedLang="en";
            localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
          }
          $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


            //  localization using localStorage

            $scope.GoToLink = function (url) {
              window.open(url,'_system');
            }
          } catch (err) {

            var errorAlert = $ionicPopup.alert({
             title: 'Kidoro Health',
             template: $scope.savedlang.sometngwentwrng,
             cssClass:'custom-popup-alert',
             buttons: [{
              text: $scope.savedlang.okTxt,
              type: 'button-alert-ok',
              onTap: function(e) {
                errorAlert.close();


              }
            }]
          });
          }



        })


        //************************************************************Kidoro Health Patient****************************************************************//
        // Method Name:
        // Developed By: TarantulaLabs
        // Dependencies:
        // Description:
        //************************************************************Kidoro Health Patient****************************************************************//

.controller('loginHome', function($cordovaOauth, $ionicPopup, $ionicLoading, $cordovaFacebook, $cordovaGooglePlus, $scope, $kinvey, $state, userDataService ,$ionicHistory, FeedList)
{

  try {

    //  localization using localStorage --onMenuToggle
    if(typeof analytics !== 'undefined') { analytics.trackView("loginHome"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }

    selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
    if(selectedLang==null){
      selectedLang="en";
      localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
    }
    $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


      //  localization using localStorage

      $scope.GoToLink = function (url) {
        window.open(url,'_system');
      }


      $scope.user={
        fullName:null,

        mobileNumber:null,
        email:null,
        kidoroCode:null,
        password:null,
        picture:null
      };



  // facebook login new for device --start

  // facebook login new for device --start

  $scope.fbLogin = function() {

    $cordovaFacebook.login(["public_profile", "email", "user_friends"])
    .then(function(success) {
        /*
         * Get user data here.
         * For more, explore the graph api explorer here: https://developers.facebook.com/tools/explorer/
         * "me" refers to the user who logged in. Dont confuse it as some hardcoded string variable.
         *
         */
        //To know more available fields go to https://developers.facebook.com/tools/explorer/
        $cordovaFacebook.api("me?fields=id,name,picture.width(300).height(300),email", [])
        .then(function(response){

          /*
           * As an example, we are fetching the user id, user name, and the users profile picture
           * and assiging it to an object and then we are logging the response.
           */
           var userData = {
            id: response.id,
            name: response.name,
            pic: response.picture.data.url,
          }
          //Do what you wish to do with user data. Here we are just displaying it in the view
          // $scope.fbData = JSON.stringify(result, null, 4);
          // alert(result.name)
          $scope.user={
            fullName:"",
            mobileNumber:"",
            email:"",
            kidoroCode:"",
            password:"",
            picture:""
          };

          var user = {};
          user.id = response.id;
          user.name = response.name;
          user.email = response.email;
          $scope.user.fullName=user.name;
          $scope.user.email=user.email;
          $scope.user.picture = response.picture.data.url;
          userDataService.setData($scope.user);
          $state.go('menu.registerHome');

        }, function(error){

          var errorAlert = $ionicPopup.alert({
            title: 'Kidoro Health',
            template: $scope.savedlang.sometngwentwrng,
            cssClass:'custom-popup-alert',
            buttons: [{
             text: $scope.savedlang.okTxt,
             type: 'button-alert-ok',
             onTap: function(e) {
               errorAlert.close();


             }
           }]
         });


        })

      }, function (error) {

      });

  }

  // facebook login new for device --end

  // google login new for device --start
  $scope.google_data = {};
  $scope.googleLogin = function(){
    $ionicLoading.show({});
    $cordovaGooglePlus.login()
    .then(function(data){
              // $scope.imgUrl = $scope.google_data.imageUrl.replace(/[#^]/,'=300');
              $scope.user={
                fullName:"",
                mobileNumber:"",
                email:"",
                kidoroCode:"",
                password:""
              };
              $ionicLoading.hide();
              $scope.google_data = data;
              $scope.user.fullName=$scope.google_data.displayName;
              $scope.user.email=$scope.google_data.email;
              // $scope.user.picture = $scope.imgUrl;
              userDataService.setData($scope.user);
              $state.go('menu.registerHome');
              // $scope.googleData = JSON.stringify(data, null, 4);


            }, function(error){

              $ionicLoading.hide();

            });

  }

  $scope.navigatePage = function() {
    $state.go('menu.welcomeLogin');

  }

  $scope.goToRegister = function() {
    $scope.user={
      fullName:"",
      mobileNumber:"",
      email:"",
      kidoroCode:"",
      password:""
    };
    $state.go('menu.registerHome');
  }


} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('welcomeLogin', function ($scope, $ionicPopup, $state,$kinvey,$cordovaLocalNotification,$rootScope,$ionicHistory,$ionicLoading,userDataService,ionicToast,FeedList,saveParentState)
{

  try {

    //  localization using localStorage --onMenuToggle
    if(typeof analytics !== 'undefined') { analytics.trackView("welcomeLogin"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }


    selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
    if(selectedLang==null){
      selectedLang="en";
      localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
    }
    $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


      //  localization using localStorage

      $scope.user={
        username:null,
        kidoroCode:null
      };

      $scope.forgotPassword = function() {
        $state.go('menu.forgotPassword');

      }


      $scope.goToHomePage = function() {
        $ionicLoading.show({});

        var promise = $kinvey.User.logout();
        promise.then(function() {
          var promise = $kinvey.User.login({
            username: Number($scope.user.username),
            password: $scope.user.kidoroCode
          });
          promise.then(
            function (response) {
              if (response.AccountType == "Patient") {

                                    //Kinvey login finished with success
                                    $scope.submittedError = false;
                                    var query = new $kinvey.Query();
                                    query.equalTo('Kidoro_Code', response.kidoroCode);
                                    var promise = $kinvey.DataStore.find('KidoroUser', query);
                                    promise.then(function(models) {

                                      $ionicLoading.hide();

                                      userDataService.setData(models[0]);
                                      registerNotificationForUserChat($cordovaLocalNotification,$rootScope,$ionicHistory,userDataService);
                                      registerNotificationForGroupChat($cordovaLocalNotification,$rootScope,$ionicHistory,userDataService);

                                      var feeds = FeedList.get();
                                      $state.go(saveParentState.getState());
                                      return models;
                                    }, function(err) {
                                      $ionicLoading.hide();

                                      var errorAlert = $ionicPopup.alert({
                                        title: 'Kidoro Health',
                                        template: $scope.savedlang.sometngwentwrng,
                                        cssClass:'custom-popup-alert',
                                        buttons: [{
                                         text: $scope.savedlang.okTxt,
                                         type: 'button-alert-ok',
                                         onTap: function(e) {
                                           errorAlert.close();


                                         }
                                       }]
                                     });

                                    });
                                  } else {
                                    $ionicLoading.hide();
                                    var promise = $kinvey.User.logout();
                                    promise.then(function(model) {

                                      var promise = $kinvey.User.login('TarantulaAdmin', 'admin');
                                      promise.then(function(user) {
                                      },
                                      function(error) {

                                        var errorAlert = $ionicPopup.alert({
                                          title: 'Kidoro Health',
                                          template: $scope.savedlang.sometngwentwrng,
                                          cssClass:'custom-popup-alert',
                                          buttons: [{
                                           text: $scope.savedlang.okTxt,
                                           type: 'button-alert-ok',
                                           onTap: function(e) {
                                             errorAlert.close();


                                           }
                                         }]
                                       });


                                      });
                                    },
                                    function(error) {

                                      var errorAlert = $ionicPopup.alert({
                                        title: 'Kidoro Health',
                                        template: $scope.savedlang.sometngwentwrng,
                                        cssClass:'custom-popup-alert',
                                        buttons: [{
                                         text: $scope.savedlang.okTxt,
                                         type: 'button-alert-ok',
                                         onTap: function(e) {
                                           errorAlert.close();


                                         }
                                       }]
                                     });

                                    });

                                    ionicToast.show($scope.savedlang.invalidcredntals, 'bottom', false, 1500);

                                  }



                                },
                                function (error) {
                                  //Kinvey login finished with error
                                  $ionicLoading.hide();
                                  var promise = $kinvey.User.login('TarantulaAdmin', 'admin');
                                  promise.then(function(user) {
                                  },
                                  function(error) {

                                    var errorAlert = $ionicPopup.alert({
                                      title: 'Kidoro Health',
                                      template: $scope.savedlang.sometngwentwrng,
                                      cssClass:'custom-popup-alert',
                                      buttons: [{
                                       text: $scope.savedlang.okTxt,
                                       type: 'button-alert-ok',
                                       onTap: function(e) {
                                         errorAlert.close();


                                       }
                                     }]
                                   });


                                  });
                                  $scope.submittedError = true;
                                  $scope.errorDescription = error.description;

                                  ionicToast.show($scope.savedlang.invalidcredntals, 'bottom', false, 1500);


                                }
                                );

}, function(err) {
  $ionicLoading.hide();

  var errorAlert = $ionicPopup.alert({
    title: 'Kidoro Health',
    template: $scope.savedlang.sometngwentwrng,
    cssClass:'custom-popup-alert',
    buttons: [{
     text: $scope.savedlang.okTxt,
     type: 'button-alert-ok',
     onTap: function(e) {
       errorAlert.close();


     }
   }]
 });

});

}

} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('forgotPassword', function ($scope, $ionicPopup, $state,$kinvey,$http,$ionicLoading,ionicToast)
{

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("forgotPassword"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


    //  localization using localStorage

    $scope.user={"mobileNumber":null};
    $scope.goToLogin = function() {
      $ionicLoading.show({});
  // check for existing user

  var promise = $kinvey.User.exists(Number($scope.user.mobileNumber));
  promise.then(function(response) {
    if(response==true){

     var query = new $kinvey.Query();
     query.equalTo('User_Phone',Number($scope.user.mobileNumber));
     var promise = $kinvey.DataStore.find('KidoroUser', query);

     promise.then(function(models) {

       var kidoroCode=models[0].Kidoro_Code;
       var authKey="91802AFF83DNnxK55fc18a1";

       $http.get("https://control.msg91.com/api/sendhttp.php?authkey="+authKey+"&mobiles="+models[0].User_Phone+"&message="+"Your Kidoro Code is "+kidoroCode+"&sender=Kidoro&route=4&country=91")
       .success(function(result) {
         $ionicLoading.hide();

         $state.go('menu.welcomeLogin');
       })
       .error(function(data) {
         $ionicLoading.hide();

         $state.go('menu.welcomeLogin');
         return data;
       });
     },
     function(err) {
      $ionicLoading.hide();

      var errorAlert = $ionicPopup.alert({
        title: 'Kidoro Health',
        template: $scope.savedlang.sometngwentwrng,
        cssClass:'custom-popup-alert',
        buttons: [{
         text: $scope.savedlang.okTxt,
         type: 'button-alert-ok',
         onTap: function(e) {
           errorAlert.close();


         }
       }]
     });
    });




   }
   else {
    $ionicLoading.hide();
    ionicToast.show($scope.savedlang.moblenotregstrd, 'bottom', false, 1500);

  }

}, function(err) {
  $ionicLoading.hide();

  var errorAlert = $ionicPopup.alert({
    title: 'Kidoro Health',
    template: $scope.savedlang.sometngwentwrng,
    cssClass:'custom-popup-alert',
    buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });

});

}

} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})


//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('registerHome', function ($scope, $state, $ionicPopup, $timeout, ionicToast, userDataService, $ionicLoading, $kinvey) {

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("registerHome"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage


    function formatNumber(n){
      return n > 9 ? "" + n: "0" + n;
    }

    $scope.user={
      fullName:null,
      babyDueDate:null,
      babyBirthDate:null,
      mobileNumber:null,
      email:null,
      kidoroCode:null,
      password:null
    };

    if(userDataService.getData()!=null){
      $scope.user=userDataService.getData();
    }
    var monthList = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];

    $scope.goToVerification = function() {
      $ionicLoading.show({});

      var promise = $kinvey.User.exists(Number($scope.user.mobileNumber));
      promise.then(function(response) {
        if(response==true){
          $ionicLoading.hide();
          ionicToast.show($scope.savedlang.mobilenoalredyregistrd, 'bottom', false, 1500);

        }
        else {
          $ionicLoading.hide();
          userDataService.setData($scope.user);
          $state.go('menu.verificationHome');
        }

      }, function(err) {
        $ionicLoading.hide();
        var errorAlert = $ionicPopup.alert({
          title: 'Kidoro Health',
          template: $scope.savedlang.sometngwentwrng,
          cssClass:'custom-popup-alert',
          buttons: [{
            text: $scope.savedlang.okTxt,
            type: 'button-alert-ok',
            onTap: function(e) {
             errorAlert.close();

           }
         }]
       });


      });



    }

  // Triggered on a button click, or some other target
  $scope.showBirthPopup = function() {


   var myPopup = $ionicPopup.show({
    templateUrl: 'templates/popup-template.html',
    cssClass: 'popup_dob',
    title: $scope.savedlang.birthpopup,
    scope: $scope,
  });

   $scope.data = {}

   // An elaborate, custom popup
  $scope.datepickerObject = { titleLabel: 'Select Date', //Optional
  todayLabel: 'Today', //Optional
  closeLabel: 'Close', //Optional
  setLabel: 'Set', //Optional
  setButtonType : 'button-royal-3', //Optional
  todayButtonType : 'button-royal-3', //Optional
  closeButtonType : 'button-royal-3', //Optional
  inputDate: new Date(), //Optional
  mondayFirst: true,  //Optional

  templateType: 'popup', //Optional
  modalHeaderColor: 'bar-positive', //Optional
  modalFooterColor: 'bar-positive', //Optional
  from: new Date(2000, 1, 1),  //Optional
  to: new Date(2025, 12, 30), //Optional
  callback: function (val) { //Mandatory
    datePickerCallback(val);
  }
};
var datePickerCallback = function (val) {

 if (typeof(val) === 'undefined') {

 }
 else {
  var selecteddate = new Date(val);

  var today = new Date();

  if (selecteddate <= today) {
    $scope.user.babyBirthDate = formatNumber(selecteddate.getDate())+"-"+monthList[selecteddate.getMonth()]+"-"+selecteddate.getFullYear();
    $scope.user.babyDueDate = null;

  } else {
    $scope.user.babyDueDate = formatNumber(selecteddate.getDate())+"-"+monthList[selecteddate.getMonth()]+"-"+selecteddate.getFullYear();
    $scope.user.babyBirthDate = null;

  }
  $scope.user.birthDate=formatNumber(selecteddate.getDate())+"-"+monthList[selecteddate.getMonth()]+"-"+selecteddate.getFullYear();
  myPopup.close();
}
};


};

} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})


//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('verificationHome', function ($scope, $state,userDataService, $ionicPopup, SmsServices,ionicToast) {

  try {

    //  localization using localStorage --onMenuToggle
    if(typeof analytics !== 'undefined') { analytics.trackView("verificationHome"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }

    selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
    if(selectedLang==null){
      selectedLang="en";
      localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
    }
    $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


      //  localization using localStorage


      $scope.user=userDataService.getData();

      $scope.Otp={generatedOtp:null,
        enteredOtp:null
      };

      $scope.send = function(){

       $scope.Otp.generatedOtp=SmsServices.getRespose($scope.user.mobileNumber);

     }
     $scope.send();
     $scope.goToWelcomeHome = function() {

      if($scope.Otp.generatedOtp==$scope.Otp.enteredOtp){
        // $state.go('menu.welcomeHome');
             $state.go('menu.myCurrentDetails');
      }
      else{
        ionicToast.show($scope.savedlang.incorrectotp, 'bottom', false, 1500);

      }


    }

  } catch (err) {
    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('welcomeHome', function ($scope,saveParentState,$cordovaLocalNotification,saveKidoroCode,cordovaDeviceService,$cordovaLocalNotification,$rootScope,$ionicHistory, ionicToast, $state,$ionicPopup,$kinvey,userDataService,sendMail,$ionicLoading, FeedList, VaccinationUpdateService,MilestoneService)
{

  try {


    //  localization using localStorage --onMenuToggle
    if(typeof analytics !== 'undefined') { analytics.trackView("welcomeHome"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }

    selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
    if(selectedLang==null){
      selectedLang="en";
      localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
    }
    $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

      //  localization using localStorage


      $scope.OtherShare=function(kidoroCode){

        $scope.kidoroCode = kidoroCode;

        $scope.subject = "Connect with your Kidoro Family"
        $scope.message = "Enter Kidoro Code "+ "" + $scope.kidoroCode + ""+ " on Sign Up to connect with your Family and Friends" + "\n" +"Install free app at https://goo.gl/R2TPjw";

        window.plugins.socialsharing.share($scope.message, $scope.subject, null, null);
      }

      $scope.user=userDataService.getData();
      $scope.partner={kidoroCode:null};

      function formatNumber(n){
        return n > 9 ? "" + n: "0" + n;
      }


      var getKidoroCode = function(){
        var monthList = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        var hashids = new Hashids(($scope.user.mobileNumber).toString(), 5, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
        var birthDate=$scope.user.birthDate;
        var birthDateArr=birthDate.split("-");
        var month=formatNumber(monthList.indexOf(birthDateArr[1])+1);
        var dob=Number(birthDateArr[0].toString()+month.toString()+(Number(birthDateArr[2])%100).toString());

        var id = hashids.encode(dob);
        var code="KID"+id;
        return code;
      }

  // mom kidoro code

  var getKidoroMomCode = function(){
    var monthList = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    var hashids = new Hashids(($scope.user.mobileNumber).toString(), 5, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
    var birthDate=$scope.user.birthDate;
    var birthDateArr=birthDate.split("-");
    var month=formatNumber(monthList.indexOf(birthDateArr[1])+1);
    var dob=Number(birthDateArr[0].toString()+month.toString()+(Number(birthDateArr[2])%100).toString());

    var id = hashids.encode(dob);
    var code="MOM"+id;
    return code;
  }

  $scope.kidoroMomCode=getKidoroMomCode();

  // mom kidoro code

  $scope.kidoroCode=getKidoroCode();

  $scope.goToHomePage = function() {

    $ionicLoading.show({});

    var user = $kinvey.getActiveUser();


// userId

function zeroPadding(n) {
  if(n<10)
    return ("000" + n);
  else if (n>9 && n<100)
    return ("00" + n);
  else if (n>99 && n<1000)
    return("0" + n);
  else return(n);
}

// userId

// user Id generation

$scope.ContinueSignUp = function(){

  var totalCount;
  var promise = $kinvey.DataStore.count('KidoroUser');
  promise.then(function(response) {
    $scope.totalCount = ++response;
    $scope.count = "US"+zeroPadding($scope.totalCount);

    $ionicLoading.show({});
    // var promise = $kinvey.User.logout();
    // promise.then(function() {
     var promise = $kinvey.User.signup({
       username: Number($scope.user.mobileNumber),
       password: $scope.kidoroCode,
       email:$scope.user.email,
       fullName:$scope.user.fullName,
       MobileNumber:Number($scope.user.mobileNumber),
       kidoroCode:$scope.kidoroCode,
       AccountType:"Patient"
     });

     promise.then(
      function (model) {


        var query = new $kinvey.Query();
        query.equalTo('User_Phone',  Number(model.username));
        var promise = $kinvey.DataStore.find('KidoroUser', query);

        promise.then(function(models) {
          $scope.userExistFlag=false;
          if(models.length!=0){
            $scope.userExistFlag=true;
                    // $scope.tempCode=models[0].Kidoro_Code;
                    // $scope.kidoroCode=$scope.tempCode.replace("TEMP", "KID");
                    saveKidoroCode.setData($scope.kidoroCode);
                    var userData = {
                      _id:models[0]._id,
                      User_ID:  models[0].User_ID,
                      User_Name : $scope.user.fullName,
                      User_Phone : Number($scope.user.mobileNumber),
                      User_Mail : $scope.user.email,
                      Kidoro_Code :$scope.kidoroCode,
                      Child_Date : $scope.user.babyBirthDate,
                      Child_Due_Date: $scope.user.babyDueDate,
                      Relationship_ID : $scope.partner,
                      Kidoro_Mom_Code : $scope.kidoroMomCode,
                      Profile_Pic_URI : $scope.user.picture,
                      User_Type:"App User",
                      Vaccination_Record:null

                    };
                  }
                  else{
                    $scope.userExistFlag=false;
                    var userData = {
                      User_ID:  $scope.count,
                      User_Name : $scope.user.fullName,
                      User_Phone : Number($scope.user.mobileNumber),
                      User_Mail : $scope.user.email,
                      Kidoro_Code :$scope.kidoroCode,
                      Child_Date : $scope.user.babyBirthDate,
                      Child_Due_Date: $scope.user.babyDueDate,
                      Relationship_ID : $scope.partner,
                      Kidoro_Mom_Code : $scope.kidoroMomCode,
                      Profile_Pic_URI : $scope.user.picture,
                      User_Type:"App User",
                      Vaccination_Record:null

                    };
                  }
                //Kinvey signup finished with success
                $scope.submittedError = false;
                if($scope.userExistFlag==true){
                  var queryUpdateAppountments = new Kinvey.Query();
                  var queryUpdateMedicalRecords = new Kinvey.Query();

                  queryUpdateAppountments.equalTo('User_ID', $scope.tempCode);
                  queryUpdateMedicalRecords.equalTo('Kidoro_Code', $scope.tempCode);
                  var promiseUpdateAppointment = Kinvey.DataStore.find('KidoroAppointment', queryUpdateAppountments);
                  promiseUpdateAppointment.then(function(entities) {
                    for (var i = 0; i < entities.length; i++) {
                      entities[i].User_ID=saveKidoroCode.getData();
                      var promise = $kinvey.DataStore.save('KidoroAppointment',entities[i]);
                      promise.then(function(model) {

                        // console.log(model);

                      }, function(err) {


                        var errorAlert = $ionicPopup.alert({
                          title: 'Kidoro Health',
                          template: $scope.savedlang.sometngwentwrng,
                          cssClass:'custom-popup-alert',
                          buttons: [{
                           text: $scope.savedlang.okTxt,
                           type: 'button-alert-ok',
                           onTap: function(e) {
                             errorAlert.close();


                           }
                         }]
                       });


                      });
                    }

                  }, function(error) {
                   var errorAlert = $ionicPopup.alert({
                    title: 'Kidoro Health',
                    template: $scope.savedlang.sometngwentwrng,
                    cssClass:'custom-popup-alert',
                    buttons: [{
                     text: $scope.savedlang.okTxt,
                     type: 'button-alert-ok',
                     onTap: function(e) {
                       errorAlert.close();


                     }
                   }]
                 });
                 });
                  var promiseUpdateMedicalRecords = Kinvey.DataStore.find('KidoroMedicalRecord', queryUpdateAppountments);
                  promiseUpdateMedicalRecords.then(function(entities) {
                   for (var i = 0; i < entities.length; i++) {
                    entities[i].Kidoro_Code=saveKidoroCode.getData();
                    var promise = $kinvey.DataStore.save('KidoroMedicalRecord',entities[i]);
                    promise.then(function(model) {

                      // console.log(model);

                    }, function(err) {


                      var errorAlert = $ionicPopup.alert({
                        title: 'Kidoro Health',
                        template: $scope.savedlang.sometngwentwrng,
                        cssClass:'custom-popup-alert',
                        buttons: [{
                         text: $scope.savedlang.okTxt,
                         type: 'button-alert-ok',
                         onTap: function(e) {
                           errorAlert.close();


                         }
                       }]
                     });


                    });
                  }

                }, function(error) {
                 var errorAlert = $ionicPopup.alert({
                  title: 'Kidoro Health',
                  template: $scope.savedlang.sometngwentwrng,
                  cssClass:'custom-popup-alert',
                  buttons: [{
                   text: $scope.savedlang.okTxt,
                   type: 'button-alert-ok',
                   onTap: function(e) {
                     errorAlert.close();


                   }
                 }]
               });
               });

                   var promiseUpdateChemistOrderRecords = Kinvey.DataStore.find('KidoroChemistOrderDetails', queryUpdateMedicalRecords);
                  promiseUpdateMedicalRecords.then(function(entities) {
                   for (var i = 0; i < entities.length; i++) {
                    entities[i].Kidoro_Code=saveKidoroCode.getData();
                    var promise = $kinvey.DataStore.save('KidoroChemistOrderDetails',entities[i]);
                    promise.then(function(model) {

                      // console.log(model);

                    }, function(err) {


                      var errorAlert = $ionicPopup.alert({
                        title: 'Kidoro Health',
                        template: $scope.savedlang.sometngwentwrng,
                        cssClass:'custom-popup-alert',
                        buttons: [{
                         text: $scope.savedlang.okTxt,
                         type: 'button-alert-ok',
                         onTap: function(e) {
                           errorAlert.close();


                         }
                       }]
                     });


                    });
                  }

                }, function(error) {
                 var errorAlert = $ionicPopup.alert({
                  title: 'Kidoro Health',
                  template: $scope.savedlang.sometngwentwrng,
                  cssClass:'custom-popup-alert',
                  buttons: [{
                   text: $scope.savedlang.okTxt,
                   type: 'button-alert-ok', //unknown error
                   onTap: function(e) {
                     errorAlert.close();


                   }
                 }]
               });
               });
                }

                var promise = $kinvey.DataStore.save('KidoroUser',userData);
                promise.then(function(model) {
//-----------------------------------------------------------------Aman-Aakriti changes-----------------------------------------------------
                  VaccinationUpdateService.reminder(model.Child_Date,model.Child_Due_Date);
                  MilestoneService.milestone(model.Child_Due_Date);
//--------------------------------------------------------------------Vacination Reminders--------------------------------------------------
//                     var oneDay = 24*60*60*1000;
//
//                      var selectedDate  = new Date(model.Child_Date);
//                         // console.log(selectedDate);
//
//                      var currentDate = new Date();
//                          // console.log(currentDate);
//
//                      var diffdays = Math.round(Math.abs((selectedDate.getTime() - currentDate.getTime())/(oneDay)));
//                         // console.log(diffdays);
//
//                      var value=null;
//                      loadImmunJSON(function(response) {
//                           // Do Something with the response e.g.
//                           value = JSON.parse(response);
//                           var caseValue;
//                           if (diffdays >= 0 && diffdays < 7) {
//                             caseValue=0;
//                           }
//                           else if (diffdays>7&&diffdays<=42){
//                             caseValue=42;
//                           }
//                           else if (diffdays>42&& diffdays<=70) {
//                             caseValue = 70;
//                           }
//                           else if (diffdays>70 && diffdays <=98) {
//                             caseValue = 98;
//                           }
//                           else if (diffdays>98 && diffdays <=180) {
//                             caseValue = 180;
//                           }
//                           else if (diffdays>180 && diffdays <=270) {
//                             caseValue = 270;
//                           }
//                           else if (diffdays>270 && diffdays <=365) {
//                             caseValue = 365;
//                           }
//                           else if (diffdays>365 && diffdays <=450) {
//                             caseValue = 450;
//                           }
//                           else if (diffdays>450 && diffdays <=480) {
//                             caseValue = 480;
//                           }
//                           else if (diffdays>480 && diffdays <=670) {
//                             caseValue = 670;
//                           }
//                           else if (diffdays>670 && diffdays <=1460) {
//                             caseValue = 1460;
//                           }
//                           else if (diffdays>1460 && diffdays <= 3650) {
//                             caseValue = 3650;
//                           }
//  //--------------------------------
//  var notificationArray =   JSON.parse(localStorage.getItem('kidoroNotification'));
//
//             if(notificationArray == null){
//               notificationArray=[];
//             }
//
//
// //-----------------------
//                       switch (caseValue) {
//                         case 0:
//                               var someDate = new Date();
//                                 var numberOfDaysToAdd = 0;
//                                 someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                               var notificationData={
//                                 id: 7,
//                                 date: someDate,
//                                 message: "Vaccination Reminder" ,
//                                 title: "Kidoro Health",
//                                 autoCancel: true,
//                                 sound: true,
//                                 data: value[0].Birth,
//                                 json:null
//                               };
//                               $cordovaLocalNotification.add(notificationData).then(function () {
//                                // console.log("notification aadded");
//                             });
//
//
//                         case 42:
//                               var someDate = new Date();
//                                 var numberOfDaysToAdd = 42 - diffdays;
//                                 someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                               var notificationData={
//                                 id: 42,
//                                 date: someDate,
//                                 message: "Vaccination Reminder" ,
//                                 title: "Kidoro Health",
//                                 autoCancel: true,
//                                 sound: true,
//                                 data: value[1]["6weeks"],
//                                 json:null
//                               };
//                               $cordovaLocalNotification.add(notificationData).then(function () {
//                                // console.log("notification aadded");
//                             });
//
//
//                         case 70:
//                               var someDate = new Date();
//                                 var numberOfDaysToAdd = 70 - diffdays;
//                                 someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                               var notificationData={
//                                 id: 70,
//                                 date: someDate,
//                                 message: "Vaccination Reminder" ,
//                                 title: "Kidoro Health",
//                                 autoCancel: true,
//                                 sound: true,
//                                 data: value[2]["10weeks"],
//                                 json:null
//                               };
//                               $cordovaLocalNotification.add(notificationData).then(function () {
//                                // console.log("notification aadded");
//                             });
//
//
//                         case 98:
//                             var someDate = new Date();
//                               var numberOfDaysToAdd = 98 - diffdays;
//                               someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                             var notificationData={
//                               id: 98,
//                               date: someDate,
//                               message: "Vaccination Reminder" ,
//                               title: "Kidoro Health",
//                               autoCancel: true,
//                               sound: true,
//                               data: value[3]["14weeks"],
//                               json:null
//                             };
//                             $cordovaLocalNotification.add(notificationData).then(function () {
//                              // console.log("notification aadded");
//                           });
//
//                         case 180:
//                             var someDate = new Date();
//                               var numberOfDaysToAdd =180 - diffdays;
//                               someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                             var notificationData={
//                               id: 180,
//                               date: someDate,
//                               message: "Vaccination Reminder" ,
//                               title: "Kidoro Health",
//                               autoCancel: true,
//                               sound: true,
//                               data: value[4]["6months"],
//                               json:null
//                             };
//                             $cordovaLocalNotification.add(notificationData).then(function () {
//                              // console.log("notification aadded");
//                           });
//
//
//                         case 270:
//                             var someDate = new Date();
//                               var numberOfDaysToAdd = 270 - diffdays;
//                               someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                             var notificationData={
//                               id: 270,
//                               date: someDate,
//                               message: "Vaccination Reminder" ,
//                               title: "Kidoro Health",
//                               autoCancel: true,
//                               sound: true,
//                               data: value[5]["9-12months"],
//                               json:null
//                             };
//                             $cordovaLocalNotification.add(notificationData).then(function () {
//                              // console.log("notification aadded");
//                           });
//
//                         case 365:
//                             var someDate = new Date();
//                               var numberOfDaysToAdd = 365 - diffdays;
//                               someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                             var notificationData={
//                               id: 365,
//                               date: someDate,
//                               message: "Vaccination Reminder" ,
//                               title: "Kidoro Health",
//                               autoCancel: true,
//                               sound: true,
//                               data: value[6]["12months"],
//                               json:null
//                             };
//                             $cordovaLocalNotification.add(notificationData).then(function () {
//                              // console.log("notification aadded");
//                           });
//
//                       case 450:
//                           var someDate = new Date();
//                             var numberOfDaysToAdd = 450 - diffdays;
//                             someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                           var notificationData={
//                             id: 450,
//                             date: someDate,
//                             message: "Vaccination Reminder" ,
//                             title: "Kidoro Health",
//                             autoCancel: true,
//                             sound: true,
//                             data: value[7]["15months"],
//                             json:null
//                           };
//                           $cordovaLocalNotification.add(notificationData).then(function () {
//                            // console.log("notification aadded");
//                         });
//
//                        case 480:
//                           var someDate = new Date();
//                             var numberOfDaysToAdd = 480 - diffdays;
//                             someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                           var notificationData={
//                             id: 480,
//                             date: someDate,
//                             message: "Vaccination Reminder" ,
//                             title: "Kidoro Health",
//                             autoCancel: true,
//                             sound: true,
//                             data: value[8]["16to18months"],
//                             json:null
//                           };
//                           $cordovaLocalNotification.add(notificationData).then(function () {
//                            // console.log("notification aadded");
//                         });
//
//                       case 670:
//
//                           var someDate = new Date();
//                             var numberOfDaysToAdd = 670 - diffdays;
//                             someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                           var notificationData={
//                             id: 670,
//                             date: someDate,
//                             message: "Vaccination Reminder" ,
//                             title: "Kidoro Health",
//                             autoCancel: true,
//                             sound: true,
//                             data: value[9]["18months-2years"],
//                             json:null
//                           };
//                           $cordovaLocalNotification.add(notificationData).then(function () {
//                            // console.log("notification aadded");
//                         });
//
//                     case 1460:
//                           var someDate = new Date();
//                             var numberOfDaysToAdd = 1460 - diffdays;
//                             someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                           var notificationData={
//                             id: 1460,
//                             date: someDate,
//                             message: "Vaccination Reminder" ,
//                             title: "Kidoro Health",
//                             autoCancel: true,
//                             sound: true,
//                             data: value[10]["4to6years"],
//                             json:null
//                           };
//                           $cordovaLocalNotification.add(notificationData).then(function () {
//                            // console.log("notification aadded");
//                         });
//
//                         case 3650:
//
//                           var someDate = new Date();
//                             var numberOfDaysToAdd = 3650 -  diffdays;
//                             someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
//
//                           var notificationData={
//                             id: 3650,
//                             date: someDate,
//                             message: "Vaccination Reminder" ,
//                             title: "Kidoro Health",
//                             autoCancel: true,
//                             sound: true,
//                             data: value[11]["10to12years"],
//                             json:null
//                           };
//                           $cordovaLocalNotification.add(notificationData).then(function () {
//                            // console.log("notification aadded");
//                         });
//
//                     }
//
//              switch(caseValue){
//                 case 3605:
//                           var vaccinationArray = value[10]["4to6years"];
//
//                           for (var i = 0; i < vaccinationArray.length; i++) {
//                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:3650, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
//
//                           notificationArray.push(vaccinationObject);
//                             }
//                 case 1460: var vaccinationArray = value[11]["10to12years"];
//
//                           for (var i = 0; i < vaccinationArray.length; i++) {
//                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:1460, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
//                            notificationArray.push(vaccinationObject);
//                             }
//                 case 670: var vaccinationArray =  value[9]["18months-2years"];
//
//                           for (var i = 0; i < vaccinationArray.length; i++) {
//                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:670, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
//                            notificationArray.push(vaccinationObject);
//                         }
//
//                 case 480: var vaccinationArray =  value[8]["16to18months"];
//
//                           for (var i = 0; i < vaccinationArray.length; i++) {
//                             var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:480, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
//                             notificationArray.push(vaccinationObject);
//                         }
//
//                 case 450: var vaccinationArray =  value[7]["15months"];
//
//                           for (var i = 0; i < vaccinationArray.length; i++) {
//                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:450, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
//                            notificationArray.push(vaccinationObject);
//                         }
//                 case 365: var vaccinationArray =  value[6]["12months"];
//
//                           for (var i = 0; i < vaccinationArray.length; i++) {
//                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:365, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
//                            notificationArray.push(vaccinationObject);
//                         }
              //
              //   case 270: var vaccinationArray =  value[5]["9-12months"];
              //
              //             for (var i = 0; i < vaccinationArray.length; i++) {
              //              var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:270, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
              //              notificationArray.push(vaccinationObject);
              //           }
              //
              //   case 180: var vaccinationArray =  value[4]["6months"];
              //
              //             for (var i = 0; i < vaccinationArray.length; i++) {
              //              var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:180, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
              //              notificationArray.push(vaccinationObject);
              //           }
              //   case 98: var vaccinationArray =  value[3]["14weeks"];
              //
              //             for (var i = 0; i < vaccinationArray.length; i++) {
              //              var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:98, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
              //              notificationArray.push(vaccinationObject);
              //           }
              //   case 70: var vaccinationArray =  value[2]["10weeks"];
              //
              //             for (var i = 0; i < vaccinationArray.length; i++) {
              //              var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:70, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
              //              notificationArray.push(vaccinationObject);
              //           }
              //   case 42: var vaccinationArray =  value[1]["6weeks"];
              //
              //             for (var i = 0; i < vaccinationArray.length; i++) {
              //              var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:42, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
              //              notificationArray.push(vaccinationObject);
              //           }
              //   case 0:  var vaccinationArray =  value[0].Birth;
              //
              //             for (var i = 0; i < vaccinationArray.length; i++) {
              //              var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:7, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
              //              notificationArray.push(vaccinationObject);
              //           }
              // }
              //  localStorage.setItem('kidoroNotification', JSON.stringify(notificationArray));
              //           });


//--------------------------------------------------------------------Vacination Reminders--------------------------------------------------

                  $scope.user = model;
                  // send welcome mail on sign up
                  var html = "<p><b>Dear" + " " + $scope.user.User_Name + ",</b></p></br>" + "<p>Thank you for sign up with <b>Kidoro Health.</b></p>";
                  var text = "Congratulations! Welcome to Kidoro Health";
                  var subject = "Welcome to Kidoro Health";
                  var email = $scope.user.User_Mail;
                  var template_name = "welcome-email-kidoro";
                  var name = $scope.user.User_Name;
                  sendMail.getRespose(subject, template_name, email, name);
                  // send welcome mail on sign up

                  $ionicLoading.hide();
                  userDataService.setData(model);
                  registerNotificationForUserChat($cordovaLocalNotification,$rootScope,$ionicHistory,userDataService);
                  registerNotificationForGroupChat($cordovaLocalNotification,$rootScope,$ionicHistory,userDataService);

                  var feeds = FeedList.get();
                  $state.go(saveParentState.getState());

                }, function(err) {
                  $ionicLoading.hide();

                  var errorAlert = $ionicPopup.alert({
                     title: 'Kidoro Health',
                     template: $scope.savedlang.sometngwentwrng,
                     cssClass:'custom-popup-alert',
                     buttons: [{
                     text: $scope.savedlang.okTxt,
                     type: 'button-alert-ok',
                     onTap: function(e) {
                       errorAlert.close();
                     }
                   }]
                 });
                });
              }, function(err) {

                var errorAlert = $ionicPopup.alert({
                   title: 'Kidoro Health',
                   template: $scope.savedlang.sometngwentwrng,
                   cssClass:'custom-popup-alert',
                   buttons: [{
                   text: $scope.savedlang.okTxt,
                   type: 'button-alert-ok',
                   onTap: function(e) {
                     errorAlert.close();
                   }
                 }]
               });
                $ionicLoading.hide();
              });
   },
   function(error) {
      $ionicLoading.hide();
      $scope.submittedError = true;
      alert("Oops !" +error.description);
      $scope.errorDescription = error.description;
         }
       );
      });
    // },
    // function(error) {
    //   $ionicLoading.hide();
    //   var errorAlert = $ionicPopup.alert({
    //     title: 'Kidoro Health',
    //     template: $scope.savedlang.sometngwentwrng,
    //     cssClass:'custom-popup-alert',
    //     buttons: [{
    //      text: $scope.savedlang.okTxt,
    //      type: 'button-alert-ok',
    //      onTap: function(e) {
    //        errorAlert.close();

    //      }
    //    }]
    //  });
    // });

    }

    if($scope.partner.kidoroCode!=null){
      $scope.showConfirm = function(partner) {
        $ionicLoading.hide();

        var confirmPopup = $ionicPopup.confirm({
         title: $scope.savedlang.kidoro,
         template: $scope.savedlang.cnfprtnradd+' <b>'+partner.User_Name+' </b>' +$scope.savedlang.asurprtnr,
         cssClass:'custom-popup',
         buttons: [{
          text: $scope.savedlang.noTxt,
          type: 'button-no',
          onTap: function(e) {

            confirmPopup.close();
          }
        }, {
          text: $scope.savedlang.yesTxt,
          type: 'button-yes',
          onTap: function(e) {

            $scope.ContinueSignUp();

          }
        }]
      });
      };

      var user = $kinvey.getActiveUser();


      var query = new $kinvey.Query();
      query.equalTo('Kidoro_Code', $scope.partner.kidoroCode);
      var promise = $kinvey.DataStore.find('KidoroUser', query);

      promise.then(function(models) {
        if(models.length!=0){
         $scope.showConfirm(models[0]);
       }
       else{
        $ionicLoading.hide();
        ionicToast.show($scope.savedlang.prtnridnotexists, 'bottom', false, 1500);

      }
    }, function(err) {

      var errorAlert = $ionicPopup.alert({
        title: 'Kidoro Health',
        template: $scope.savedlang.sometngwentwrng,
        cssClass:'custom-popup-alert',
        buttons: [{
         text: $scope.savedlang.okTxt,
         type: 'button-alert-ok',
         onTap: function(e) {
           errorAlert.close();


         }
       }]
     });

      $ionicLoading.hide();


});
}
else{
 $scope.ContinueSignUp();

}

}

} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}
})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:labTestCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $ionicPopup, $ionicPlatform, $ionicLoading, $filter, $state, $kinvey,saveTestsService, sendDataService, $rootScope, $stateParams, $q, $location, $window, $timeout
// Description:
//************************************************************Kidoro Health Patient****************************************************************//
.controller('labTestCtrl', function ($scope, $ionicPopup, $ionicPlatform, $ionicLoading, $filter, $state, $kinvey,saveTestsService, sendDataService, $rootScope, $stateParams, $q, $location, $window, $timeout)

{



  try {

    if(typeof analytics !== 'undefined') { analytics.trackView("labTestCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }


  //  localization using localStorage --onMenuToggle



  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));

  if(selectedLang==null){

    selectedLang="en";

    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));

  }

  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];





    //  localization using localStorage



    $scope.checkedShow = true;



    $scope.clearSearch = function() {

      $scope.search = '';



    };



    $scope.labTestList=[];



     // search implementation using filter



     $scope.items2 = $scope.labTestList;



     $scope.$watch('search', function(val)

     {

       $scope.labTestList = $filter('filter')($scope.items2, val);

     });



     // search implementation using filter





     $ionicLoading.show({

       template:'<ion-spinner icon="android" class="spinner-energized"></ion-spinner> <span class="loading_cust"> '+$scope.savedlang.loadinglabtest+'</span>',

     });

     var promise = $kinvey.DataStore.find('KidoroTest');



     promise.then(function(models) {

      $ionicLoading.hide();



      for (var i = models.length - 1; i >= 0; i--) {



        var test ={ text: models[i].Test_name, checked: false ,data:models[i]}



        $scope.labTestList.push(test);



      };





    },

    function(err) {

     var errorAlert = $ionicPopup.alert({

       title: 'Kidoro Health',

       template: $scope.savedlang.sometngwentwrng,

       cssClass:'custom-popup-alert',

       buttons: [{

        text: $scope.savedlang.okTxt,

        type: 'button-alert-ok',

        onTap: function(e) {

          errorAlert.close();





        }

      }]

    });



   });



     $scope.checkBoxChecked=function(){



       var matchCount=0;

       for (var i = 0; i < $scope.labTestList.length; i++) {

         if($scope.labTestList[i].checked==true){

           matchCount=1;

           break;

         }

       };

       if (matchCount==0) {

         $scope.checkedShow=true;

       }else{

         $scope.checkedShow=false;

       }

     }

     $scope.donePressed = function(){

      var saveTestData=[];

      var selectedTests=[];

      for (var i = $scope.labTestList.length - 1; i >= 0; i--) {



        if($scope.labTestList[i].checked==true){





          selectedTests.push($scope.labTestList[i].text);

          saveTestData.push($scope.labTestList[i]);

        }

      };



      sendDataService.setData(selectedTests);

      saveTestsService.setData(saveTestData);

      $state.go('menu.LabList');





    }



  } catch (err) {

    var errorAlert = $ionicPopup.alert({

     title: 'Kidoro Health',

     template: $scope.savedlang.sometngwentwrng,

     cssClass:'custom-popup-alert',

     buttons: [{

      text: $scope.savedlang.okTxt,

      type: 'button-alert-ok',

      onTap: function(e) {

        errorAlert.close();





      }

    }]

  });

  }



})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:LabListCtrl
// Developed By: TarantulaLabs
// Dependencies:$ionicPlatform, saveLabsDataService, $ionicHistory,userDataService,saveTestsService,sendMail,$http,saveParentState,savePharmaData, $ionicModal, $scope, $ionicLoading, $state,$ionicPopup,ionicToast, $location, $kinvey,sendDataService,labDataService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('LabListCtrl', function ($ionicPlatform, saveLabsDataService, $ionicHistory,userDataService,saveTestsService,sendMail,$http,saveParentState,savePharmaData, $ionicModal, $scope, $ionicLoading, $state,$ionicPopup,ionicToast, $location, $kinvey,sendDataService,labDataService)
{

  try {

    //  localization using localStorage --onMenuToggle
    if(typeof analytics !== 'undefined') { analytics.trackView("LabListCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }


    selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
    if(selectedLang==null){
      selectedLang="en";
      localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
    }
    $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


      //  localization using localStorage

      $scope.$on('$ionicView.enter', function(e) {
        var history = $ionicHistory.viewHistory();
              // angular.forEach(history.views, function(view, index){
              // });
              // angular.forEach(history.histories[$ionicHistory.currentHistoryId()].stack, function(view, index){
              // });
              var historyStackLength = history.histories[$ionicHistory.currentHistoryId()].stack.length;
              if(history.histories[$ionicHistory.currentHistoryId()].stack[historyStackLength-2].stateName=="menu.welcomeLogin" || history.histories[$ionicHistory.currentHistoryId()].stack[historyStackLength-2].stateName=="menu.welcomeHome"){
                $scope.callPharam(savePharmaData.getData());
              }
            });


// Maanik Changes

$scope.bookLabTest = function(data){
 var user = $kinvey.getActiveUser();
 if(user.username == "TarantulaAdmin"){
   savePharmaData.setData(data);
   saveParentState.setState('menu.LabList');
   $state.go('menu.loginHome');

 } else{
  saveLabsDataService.setData(data);
  $state.go('menu.labTestAddress');
                 // makeBooking(userDataService.getData(),data,$http,sendMail);

               }

             }

             $scope.callPharam = function(data){

              var pharma=data.Lab_Ext;

              var user = $kinvey.getActiveUser();
              if(user.username == "TarantulaAdmin"){
               savePharmaData.setData(data);
               saveParentState.setState('menu.LabList');
               $state.go('menu.loginHome');

             } else{
              var confirmPopup = $ionicPopup.confirm({
               title: $scope.savedlang.callExtTxt,
               template: $scope.savedlang.dialExtTxt+' '+pharma.toString(),
               cssClass:'custom-popup',
               buttons: [{
                text: $scope.savedlang.noTxt,
                type: 'button-no',
                onTap: function(e) {

                  confirmPopup.close();
                }
              }, {
                text: $scope.savedlang.callTxt,
                type: 'button-yes',
                onTap: function(e) {

                  var call = "tel:" + $scope.savedlang.commonPhoneNumber;

                  document.location.href = call;
                }
              }]
            });

            }
          }


          $scope.clearLabSearch = function() {
            $scope.search = '';

          };

          $ionicLoading.show({
            template:'<ion-spinner icon="android" class="spinner-energized"></ion-spinner> <span class="loading_cust">'+$scope.savedlang.loadinglabs+'</span>',
          });

          var query=new $kinvey.Query();
          query.matches('Lab_ID','^((GUJ)|(GUJ..))[0-9][0-9]*$');
          var promise = $kinvey.DataStore.find('KidoroLabs',query);

          promise.then(function(models) {

      // var models = loadLabsData.getData();
      var labTestList = sendDataService.getData();
      var selectedTests = saveTestsService.getData();

      var i;
      var j;
      var matchCount=0;
      $scope.labLists=models;
      $scope.labList=[];

      $scope.localityArray = [];
      for (i = 0; i <models.length; i++) {
          // locality data pharmacy --start

          if($scope.localityArray.length==0){
           $scope.localityArray.push({'location':$scope.labLists[i].Lab_Locality});
         }
         else{
           var match = 0;
           for(var j =0;j<$scope.localityArray.length;j++){
             if($scope.localityArray[j].location==$scope.labLists[i].Lab_Locality){
               match=1;
               break;
             }
           }
           if(match==0){
             $scope.localityArray.push({'location':$scope.labLists[i].Lab_Locality});
           }
         }

         $scope.selectedlocality= $scope.savedlang.alllocality;

             // locality data pharmacy --end

             var LabTestAmt=0;


             if ((models[i].Lab_Doctor_Profile == undefined) || (models[i].Lab_Doctor_Profile == null)) {
              models[i]['Lab_Doctor_Profile'] = "./img/noimage2.png";
            }
            else{
              models[i]['Lab_Doctor_Profile'] = models[i].Lab_Doctor_Profile;
            }
            if ((models[i].Lab_Pic == undefined) || (models[i].Lab_Pic == null)) {
              models[i]['Lab_Pic'] = "./img/cover3.png";
            }
            else{
              models[i]['Lab_Pic'] = models[i].Lab_Pic;
            }

            for (j = 0; j <labTestList.length; j++) {

              if(models[i].Lab_test.toUpperCase().indexOf(labTestList[j].toUpperCase())<0){
                break;

              }
              else{

                models[i].Lab_test = models[i].Lab_test.replace("\n","");
                var testArray = models[i].Lab_test.split(",");
                $scope.testwithprice = [];
                for (var k =  0; k < labTestList.length; k++) {
                  for (var l = 0; l <testArray.length; l++) {
                    if(testArray[l].indexOf(labTestList[k].toUpperCase())>-1){

                      $scope.testwithprice.push({
                        'name':labTestList[k],
                        'price':Number(testArray[l].split("^")[1])
                      });
                      selectedTests[j].data["rate"]=Number(testArray[l].split("^")[1]);
                      LabTestAmt = LabTestAmt+Number(testArray[l].split("^")[1]);
                    }
                  };

                };
                var tempObject = models[i];
                tempObject["Test_Amount"] = LabTestAmt;
                $ionicLoading.hide();
                $scope.labList.push(models[i]);
                models[i]["allTestsSelected"] = $scope.testwithprice;
              }

            };

        // if(j==labTestList.length){
        //
        //   models[i].Lab_test = models[i].Lab_test.replace("\n","");
        //   var testArray = models[i].Lab_test.split(",");
        //   $scope.testwithprice = [];
        //   for (var k =  0; k < labTestList.length; k++) {
        //     for (var l = 0; l <testArray.length; l++) {
        //       if(testArray[l].indexOf(labTestList[k])>-1){
        //
        //         $scope.testwithprice.push({
        //         'name':labTestList[k],
        //         'price':Number(testArray[l].split("^")[1])
        //       });
        //
        //         LabTestAmt = LabTestAmt+Number(testArray[l].split("^")[1]);
        //       }
        //     };
        //
        //   };
        //   var tempObject = models[i];
        //   tempObject["Test_Amount"] = LabTestAmt;
        //   $scope.labList.push(models[i]);
        //   models[i]["allTestsSelected"] = $scope.testwithprice;
        //
        // }

      };


      // locationModal by saras --end

      $ionicModal.fromTemplateUrl('templates/modalLocation.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
        hardwareBackButtonClose: false,
        focusFirstInput: true
      }).then(function(modal) {

        $scope.modal = modal;
        $scope.modal.searchText = "";

      })
      $scope.clearSearch = function() {
        $scope.modal.searchText = "";

      }

      $scope.openModal = function() {
        $scope.modal.show();
      }

      $scope.closeModal = function() {
        $scope.modal.hide();
      };

      $scope.$on('$destroy', function() {
        $scope.modal.remove();
      });

      $scope.submitToDoctorList = function(item) {
        $scope.selectedlocality=item.location;
        $scope.modal.hide();
      };

      $scope.selectAllLocality = function(language) {

        $scope.selectedlocality=language.alllocality;
        $scope.modal.hide();
      }

          // locationModal by saras --end

        },
        function(err) {
          var errorAlert = $ionicPopup.alert({
            title: 'Kidoro Health',
            template: $scope.savedlang.sometngwentwrng,
            cssClass:'custom-popup-alert',
            buttons: [{
              text: $scope.savedlang.okTxt,
              type: 'button-alert-ok',
              onTap: function(e) {
               errorAlert.close();


             }
           }]
         });

        });

$scope.sendData = function(data) {

  labDataService.setData(data);
  $state.go('menu.labdetails');

}

} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:pharmaListCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $ionicModal, $ionicLoading, $ionicPopup, $state,$kinvey,$http,makeCall,ionicToast,labDataService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('pharmaListCtrl', function ($scope, $ionicModal, $ionicLoading, $ionicPopup, $state,$kinvey,$http,makeCall,ionicToast,labDataService) {

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("pharmaListCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


    //  localization using localStorage



    $scope.GoToLink = function (url) {
      window.open(url,'_system');
    }

    $scope.clearSearchChemist = function() {
      $scope.search = '';
    };


    $ionicLoading.show({
      template:'<ion-spinner icon="android" class="spinner-energized"></ion-spinner> <span class="loading_cust">'+$scope.savedlang.loadingchemists+'</span>',
    });
    $scope.pharmaList=[];
    var promise = $kinvey.DataStore.find('KidoroPharma');

    promise.then(function(models) {
      $ionicLoading.hide();

      $scope.pharmaList = models;

      $scope.localityArray = [];
      for (var i = 0; i < $scope.pharmaList.length; i++) {

  // locality data pharmacy --start


  if($scope.localityArray.length==0){
   $scope.localityArray.push({'location':$scope.pharmaList[i].Pharma_Locality});
 }
 else{
   var match = 0;
   for(var j =0;j<$scope.localityArray.length;j++){
     if($scope.localityArray[j].location==$scope.pharmaList[i].Pharma_Locality){
       match=1;
       break;
     }
   }
   if(match==0){
     $scope.localityArray.push({'location':$scope.pharmaList[i].Pharma_Locality});
   }
 }

 $scope.selectedlocality= $scope.savedlang.alllocality;

     // locality data pharmacy --end

     if (($scope.pharmaList[i].Pharma_Image == undefined) || ($scope.pharmaList[i].Pharma_Image == null)) {
      $scope.pharmaList[i]['pharmaPic'] = "./img/noimage2.png";
    }
    else{
      $scope.pharmaList[i]['pharmaPic'] = $scope.pharmaList[i].Pharma_Image;
    }
  }

// locationModal by saras --end

$ionicModal.fromTemplateUrl('templates/modalLocation.html', {
  scope: $scope,
  animation: 'slide-in-up',
  backdropClickToClose: false,
  hardwareBackButtonClose: false,
  focusFirstInput: true
}).then(function(modal) {

  $scope.modal = modal;
  $scope.modal.searchText = "";

})
$scope.clearSearch = function() {
  $scope.modal.searchText = "";

}

$scope.openModal = function() {
  $scope.modal.show();
}

$scope.closeModal = function() {
  $scope.modal.hide();
};

$scope.$on('$destroy', function() {
  $scope.modal.remove();
});

$scope.submitToDoctorList = function(item) {
  $scope.selectedlocality=item.location;
  $scope.modal.hide();
};

$scope.selectAllLocality = function(language) {

  $scope.selectedlocality=language.alllocality;
  $scope.modal.hide();
}

    // locationModal by saras --end


  },
  function(err) {

   var errorAlert = $ionicPopup.alert({
    title: 'Kidoro Health',
    template: $scope.savedlang.sometngwentwrng,
    cssClass:'custom-popup-alert',
    buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });

 });

    $scope.sendPharmaData = function(data) {
      labDataService.setData(data);
      $state.go('menu.pharmadetails');

    }


  } catch (err) {
    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:pharmadetailsCtrl
// Developed By: TarantulaLabs
// Dependencies:modalService, saveParentState, $ionicPlatform, $state, $ionicLoading, prescriptionService, $kinvey,$ionicPopup, $cordovaCamera, ionicToast, $scope,labDataService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('pharmadetailsCtrl', function(modalService, saveParentState, $ionicPlatform, $state, $ionicLoading, prescriptionService, $kinvey,$ionicPopup, $cordovaCamera, ionicToast, $scope,labDataService) {

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("pharmadetailsCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


    //  localization using localStorage

    $scope.$on('$ionicView.afterEnter', function(){
      $scope.hideBar = "true";
    });

    $scope.showFullImage = function(imageUrl) {
      if (imageUrl == null || imageUrl === undefined) {
      } else {
        modalService.init('templates/image-modal.html',imageUrl, $scope).then(function(modal) {
          modal.show();
        });
      }
    }

    $scope.selectedpharmaDetails = labDataService.getData();

    if ($scope.selectedpharmaDetails.Pharma_Cover == null) {
      $scope.selectedpharmaDetails.Pharma_Cover = "./img/cover3.png";
    }



    $scope.capturePrescription = function(){
      var user = $kinvey.getActiveUser();
      if(user.username == "TarantulaAdmin"){
       saveParentState.setState('menu.previewPrescription');
       $state.go('menu.loginHome');

        //  ionicToast.show($scope.savedlang.plslogintousethisfeature, 'bottom', false, 1500);

      } else{

        $state.go('menu.previewPrescription');
      }
    }

    $scope.GoToLink = function (url) {
      window.open(url,'_system');
    }

  } catch (err) {

    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:previewPrescriptionCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $timeout, $kinvey, $ionicHistory, imageService,saveImages,saveDays, $cordovaCamera, ionicToast, $ionicLoading, $ionicPopup,  $state, prescriptionService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("previewPrescriptionCtrl", function($scope, $timeout, $kinvey, $ionicHistory, imageService,saveImages,saveDays, $cordovaCamera, ionicToast, $ionicLoading, $ionicPopup,  $state, prescriptionService){

  try {
    $scope.images = [];
    $scope.blobImages = [];
    $scope.imageArray = [];

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("previewPrescriptionCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage

    $scope.days = 0;
    $scope.decrementDays = function() {
     if ($scope.days == 0) {
       $scope.days = 0;
       $scope.presDays =   $scope.days;

     } else {
       $scope.days = $scope.days - 1;
       $scope.presDays =   $scope.days;

     }
   }

   $scope.incrementDays = function() {
     if ($scope.days >= 0) {
       $scope.days = $scope.days + 1;
       $scope.presDays =   $scope.days;

     } else {

     }
   }

   $scope.addPrescription = function() {
     var options = {
       quality: 20,
       destinationType: Camera.DestinationType.DATA_URL,
       sourceType: Camera.PictureSourceType.CAMERA,
       encodingType: Camera.EncodingType.JPEG,
    //  targetWidth: 512,
    //  targetHeight: 1024,
    saveToPhotoAlbum: false,
    correctOrientation: true
  };
  $cordovaCamera.getPicture(options).then(function (imageData) {
   $ionicLoading.show({
    template: 'Processing Image',
    duration: 2000
  });
   if (imageData !== null || imageData !== undefined) {
     $scope.images.push({'image':"data:image/jpeg;base64," + imageData});
          // $scope.$apply();
          $scope.blobImages.push({'image':imageData});
          $scope.imageArray.push({'type': 'image/png', 'name':'Prescription_Image', 'content':imageData});
        }
        saveImages.setData($scope.images);
       $ionicLoading.hide();
    }, function (err) {
      $state.go('menu.pharmadetails')
      $ionicLoading.hide();
    });

}
console.log($ionicHistory.viewHistory());
if($ionicHistory.viewHistory().forwardView==null){
$scope.addPrescription();

}
else{
$scope.images=saveImages.getData();
$scope.days=saveDays.getData();
}
$scope.prescriptionBlobArray = [];
$scope.PrescriptionImagesArray = [];
$scope.metadata=[];

$scope.gotoaddress = function() {
  saveImages.setData($scope.images);
  saveDays.setData($scope.days);
$scope.prescriptionBlobArray = [];
$scope.PrescriptionImagesArray = [];
  var confirmpresupload = $ionicPopup.confirm({
   title: $scope.savedlang.uploadprescptn,
   template: $scope.savedlang.areyousuretouploadpres,
   cssClass:'custom-popup',
   buttons: [{
    text: $scope.savedlang.noTxt,
    type: 'button-no',
    onTap: function(e) {
      confirmpresupload.close();
    }
  }, {
    text: $scope.savedlang.yesTxt,
    type: 'button-yes',
    onTap: function(e) {

          // Prescription Upload and Download --start
            confirmpresupload.close();
          $ionicLoading.show({});
          var contentType = 'image/png';

          var currentTime = new Date();
          var uniqueDateTime = currentTime.getDate()+""+(currentTime.getMonth()+1)+""+currentTime.getFullYear()+""+(currentTime.getHours())+""+(currentTime.getMinutes())+""+(currentTime.getSeconds());
          var formattedDateTime = currentTime.getDate()+"-"+(currentTime.getMonth()+1)+"-"+currentTime.getFullYear()+", "+(currentTime.getHours())+":"+(currentTime.getMinutes())+":"+(currentTime.getSeconds());

          for (var i = 0; i < $scope.blobImages.length; i++) {
            var b64Data = $scope.blobImages[i].image;
            var blob = b64toBlob(b64Data, contentType);
            $scope.prescriptionBlobArray.push(blob);
          }
                 prescriptionService.setData($scope.prescriptionBlobArray);
                 imageService.setData($scope.imageArray);
                 $ionicLoading.hide();
                 $state.go('menu.address');

         //  for (var i = 0; i < $scope.prescriptionBlobArray.length; i++) {

         //    var fileContent = $scope.prescriptionBlobArray[i];
         //    var data = {
         //     _filename : uniqueDateTime,
         //     mimeType  : 'image/png',
         //     size      : fileContent.length
         //   }
         //   $scope.metadata.push(data);
         //   var promise = $kinvey.File.upload(fileContent, $scope.metadata[i], {
         //    public:true
         //  });
         //   promise.then(function(response) {

         //     var promise = $kinvey.File.stream(response._id);
         //     promise.then(function(file) {

         //      var url = file._downloadURL;
         //      $scope.PrescriptionImagesArray.push({'pres_link': url,'pres_name': response._filename});


         //      if($scope.metadata.length==$scope.prescriptionBlobArray.length){

         //       if(response._filename==$scope.metadata[$scope.prescriptionBlobArray.length-1]._filename){
         //         $scope.PrescriptionData = {
         //           doc_type: "Prescription",
         //           pres_link_array: $scope.PrescriptionImagesArray,
         //           no_of_days: $scope.presDays,
         //           date: formattedDateTime
         //         };

         //         prescriptionService.setData($scope.PrescriptionData);
         //         imageService.setData($scope.imageArray);
         //         $ionicLoading.hide();
         //         $state.go('menu.address');
         //       }
         //     }

         //   });
         //   }, function(err) {
         //    $ionicLoading.hide();

         //  });

         // }


       }
     }]
   });
         // Prescription Upload and Download --end


      // base64 string to blob

      function b64toBlob(b64Data, contentType, sliceSize) {
       contentType = contentType || '';
       sliceSize = sliceSize || 512;

       var byteCharacters = atob(b64Data);
       var byteArrays = [];

       for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
         var slice = byteCharacters.slice(offset, offset + sliceSize);

         var byteNumbers = new Array(slice.length);
         for (var i = 0; i < slice.length; i++) {
           byteNumbers[i] = slice.charCodeAt(i);
         }

         var byteArray = new Uint8Array(byteNumbers);

         byteArrays.push(byteArray);
       }

       var blob = new Blob(byteArrays, {type: contentType});
       return blob;
     }

      // base64 string to blob
    }

  } catch (err) {
    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:addressCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope,$kinvey,$ionicModal,$ionicHistory, sendMail, imageService, $ionicPopup,labDataService, userDataService, $ionicLoading, $state, $cordovaFileTransfer, prescriptionService,ionicToast
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("addressCtrl", function($scope,$kinvey,$ionicModal,$ionicHistory, sendMail, imageService, $ionicPopup,labDataService, userDataService, $ionicLoading, $state, $cordovaFileTransfer, prescriptionService,ionicToast){

  try {
    if(typeof analytics !== 'undefined') { analytics.trackView("addressCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }

    $ionicModal.fromTemplateUrl('templates/kidoroCouponModal.html', {
      scope: $scope,
      backdropClickToClose: false
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.couponCode = null;
    });

    $scope.openModal = function(){
      $scope.couponCode = null;
      $scope.modal.show();
    }

    $scope.closeModal = function(){
      $scope.couponCode = null;
      $scope.modal.hide();
    }
    $scope.metadata=[];
    $scope.PrescriptionImagesArray=[];
    $scope.userData = userDataService.getData();
    $scope.selectedpharmaDetails = labDataService.getData();
    $scope.PrescriptionData = prescriptionService.getData();
    $scope.chemistRecord = {
      pharma_name: $scope.selectedpharmaDetails.Pharma_Name,
      pharma_address: $scope.selectedpharmaDetails.Pharma_Address,
      pharma_email: $scope.selectedpharmaDetails.Pharma_email
    }
    $scope.userAddress = {
      name: $scope.userData.User_Name,
      pincode:"",
      address:"",
      landmark:"",
      city:"",
      state:"",
      phone: $scope.userData.User_Phone,
      preRecord: $scope.PrescriptionData,
      pharmaRecord: $scope.chemistRecord
    };



  //  localization using localStorage --onMenuToggle

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage
$scope.uploadPrescriptions = function(){
   $ionicLoading.show();
    for (var i = 0; i < $scope.PrescriptionData.length; i++) {

            var fileContent = $scope.PrescriptionData[i];
            var data = {
             _filename : uniqueDateTime,
             mimeType  : 'image/png',
             size      : fileContent.length
           }
           $scope.metadata.push(data);
           var promise = $kinvey.File.upload(fileContent, $scope.metadata[i], {
            public:true
          });
           promise.then(function(response) {

             var promise = $kinvey.File.stream(response._id);
             promise.then(function(file) {
          var currentTime = new Date();
          var uniqueDateTime = currentTime.getDate()+""+(currentTime.getMonth()+1)+""+currentTime.getFullYear()+""+(currentTime.getHours())+""+(currentTime.getMinutes())+""+(currentTime.getSeconds());
          var formattedDateTime = currentTime.getDate()+"-"+(currentTime.getMonth()+1)+"-"+currentTime.getFullYear()+", "+(currentTime.getHours())+":"+(currentTime.getMinutes())+":"+(currentTime.getSeconds());

              var url = file._downloadURL;
              $scope.PrescriptionImagesArray.push({'pres_link': url,'pres_name': response._filename});


              if($scope.metadata.length==$scope.PrescriptionData.length){

               if(response._filename==$scope.metadata[$scope.PrescriptionData.length-1]._filename){
                 $scope.PrescriptionData = {
                   doc_type: "Prescription",
                   pres_link_array: $scope.PrescriptionImagesArray,
                   no_of_days: $scope.presDays,
                   date: formattedDateTime
                 };
                  $scope.savePresData();
                 // prescriptionService.setData($scope.PrescriptionData);
                 // imageService.setData($scope.imageArray);

                 // $state.go('menu.address');
               }
             }

           });
           }, function(err) {
            $ionicLoading.hide();

          });

         }


}

    $scope.confirmBooking=function(){
     var user = userDataService.getData();
//----------------------------------------CuponCode-------------------------------------------------------
$scope.openModal();
$scope.sendcouponCode = function(code) {
 $scope.couponCode = code;
 if ($scope.couponCode == null || $scope.couponCode == undefined || $scope.couponCode == "") {
   $scope.couponCode = null;
                  $scope.uploadPrescriptions();
                  $scope.modal.hide();
                } else {
                 var query = new $kinvey.Query();
                 query.equalTo('coupon_code', $scope.couponCode);
                 $ionicLoading.show({});
                 var promise = $kinvey.DataStore.find('KidoroCoupon', query);
                 promise.then(function(coupon) {
                  $ionicLoading.hide();
                  if (coupon.length == 0) {
                    ionicToast.show($scope.savedlang.invalidcouponcode, 'bottom', false, 1500);

                  } else {

                    $scope.couponData = coupon[0];
if ($scope.couponData.used_by==null){
$scope.couponData.used_by=[];
}
                    if ($scope.couponData.used_by.indexOf(user.Kidoro_Code)==-1) {
                      var currentDate = new Date();
                      var current_ms = currentDate.getTime();


                      var valid_to = $scope.couponData.valid_to.split('-');
                      var temp_valid_to = new Date();
                      temp_valid_to.setDate(Number(valid_to[0]));
                      temp_valid_to.setMonth((Number(valid_to[1]))-1);
                      temp_valid_to.setFullYear(Number(valid_to[2]));
                      temp_valid_to.setHours(23);
                      temp_valid_to.setMinutes(59);
                      temp_valid_to.setSeconds(59);
                      var temp_valid_to_ms = temp_valid_to.getTime();

                      var valid_from = $scope.couponData.valid_from.split('-');
                      var temp_valid_from = new Date();
                      temp_valid_from.setDate(Number(valid_from[0]));
                      temp_valid_from.setMonth((Number(valid_from[1]))-1);
                      temp_valid_from.setFullYear(Number(valid_from[2]));
                      temp_valid_from.setHours(0);
                      temp_valid_from.setMinutes(0);
                      temp_valid_from.setSeconds(0);
                      var temp_valid_from_ms = temp_valid_from.getTime();
                      var usedBy=[];
                      if(coupon.used_by==null||coupon.used_by==undefined){
                        usedBy.push(user.Kidoro_Code);
                      }
                      else{
                        usedBy=coupon.used_by;
                        usedBy.push(user.Kidoro_Code);
                      }

                      if ((temp_valid_to_ms>current_ms) && (current_ms>temp_valid_from_ms) && $scope.couponData.type==$scope.savedlang.couponTypeMedicine ) {

                            // ionicToast.show("Applicable", 'bottom', false, 1500);
                            var updatedCouponData = {
                              _id: $scope.couponData._id,
                              coupon_id: $scope.couponData.coupon_id,
                              coupon_code: $scope.couponData.coupon_code,
                              valid_to: $scope.couponData.valid_to,
                              valid_from: $scope.couponData.valid_from,
                              discount: $scope.couponData.discount,
                              used_by: usedBy,
                              type:$scope.couponData.type
                            }
                            var promise = Kinvey.DataStore.save('KidoroCoupon', updatedCouponData);
                            promise.then(function(entity) {
                             $scope.modal.hide();
                             var res = $scope.savedlang.discountmessage.replace("%", updatedCouponData.discount);
                             var Alert = $ionicPopup.alert({
                              title: 'Kidoro Health',
                              template: res,
                              cssClass:'custom-popup-alert',
                              buttons: [{
                               text: $scope.savedlang.okTxt,
                               type: 'button-alert-ok',
                               onTap: function(e) {
                                 Alert.close();
                              $scope.uploadPrescriptions();

        }
      }]
    });


                           }, function(error) {
                           });

                          } else {
                            ionicToast.show($scope.savedlang.invalidcouponcode, 'bottom', false, 1500);

                          }

                        } else {
                          ionicToast.show($scope.savedlang.couponcodealrdyused, 'bottom', false, 1500);
                        }

                      }
                    },
                    function(error) {
                    });


}
}
}
$scope.placeOrder = function() {
      //
      // if ($scope.userData.Documents === undefined || $scope.userData.Documents === null) {
      //   $scope.PrescriptionDataArray = [];
      // } else {
      //   $scope.PrescriptionDataArray = $scope.userData.Documents;
      // }
      // $scope.PrescriptionDataArray.push($scope.PrescriptionData);
      // console.log($ionicHistory.backView());

      if ($scope.userAddress.pincode != undefined && $scope.userAddress.pincode != "" && $scope.userAddress.address != "" && $scope.userAddress.address != undefined && $scope.userAddress.city != ""  && $scope.userAddress.city != undefined && $scope.userAddress.state != "" && $scope.userAddress.state != undefined && $scope.userAddress.landmark != "" && $scope.userAddress.landmark != undefined && $scope.userAddress.name != "" && $scope.userAddress.phone != "" && $scope.userAddress.phone != undefined && $scope.userAddress.name != undefined) {
       $scope.confirmBooking();
      // $scope.savePresData();
      } else {
        ionicToast.show($scope.savedlang.allinputrqrd, 'bottom', false, 1500);
      }

    }

    var currentTime = new Date();
    var uniqueDateTime = currentTime.getTime();

    $scope.savePresData = function() {
      $ionicLoading.show({});
      var promise = $kinvey.DataStore.save('KidoroPrescriptions', {
       pres_id: uniqueDateTime,
       pres_image_array: $scope.PrescriptionImagesArray,
       pres_uploaded_by: $scope.userData.Kidoro_Code,
       pres_patient_id:$scope.userData.Kidoro_Code,
       pres_uploader_name: $scope.userData.User_Name
     });
      promise.then(function(model) {
       $scope.savePresPharmaData();
     }, function(err) {
       $ionicLoading.hide();
       var errorAlert = $ionicPopup.alert({
        title: 'Kidoro Health',
        template: $scope.savedlang.sometngwentwrng,
        cssClass:'custom-popup-alert',
        buttons: [{
          text: $scope.savedlang.okTxt,
          type: 'button-alert-ok',
          onTap: function(e) {
           errorAlert.close();

         }
       }]
     });

     });
    }


    function zeroPadding(n) {
      if(n<10)
        return ("000" + n);
      else if (n>9 && n<100)
        return ("00" + n);
      else if (n>99 && n<1000)
        return("0" + n);
      else return(n);
    }

    $scope.savePresPharmaData = function() {
      $ionicLoading.show({});

      var totalCount;
      var promise = $kinvey.DataStore.count('KidoroChemistOrderDetails');
      promise.then(function(response) {
        $scope.totalCount = ++response;
        $scope.count = "ORDER"+zeroPadding($scope.totalCount);

        var promise = $kinvey.DataStore.save('KidoroChemistOrderDetails', {

         Order_ID: $scope.count,
         Pharma_ID : $scope.selectedpharmaDetails.Pharma_ID,
         Doc_Kidoro_Code : null,
         Kidoro_Code :$scope.userData.Kidoro_Code,
         Record: $scope.userAddress
       });
        promise.then(function(model) {
             //  sendMail after prescriptionUploaded and saved to KidoroCollection
             //var email = model.Record.pharmaRecord.pharma_email;
             var email = $scope.savedlang.bccAddress;
             var subject = "Prescription Order Details of" +" "+ model.Record.name +" for "+ model.Record.pharmaRecord.pharma_name;
             var id = model.Order_ID;
             var name = model.Record.pharmaRecord.pharma_name;
             var status = model.Record.address +","+ model.Record.landmark +","+ model.Record.city +","+ model.Record.state +","+ "PinCode: " + model.Record.pincode +","+ "Phone Number: " +model.Record.phone;
             var date = model.Record.preRecord.date;
             var time = model.Record.preRecord.no_of_days;
             var cname = model.Record.name;
             var doctorName = "";
             var discount = "";
             var template_name = "chemistorder";
             var imageArray = imageService.getData();
              if ($scope.couponData == null || $scope.couponData == undefined || $scope.couponData == "") {
                discount = "No discount available";
              }else{
                discount = $scope.couponData.discount;
              }
             if (email != null || email !== undefined) {
              var bccAddress="";
              sendMail.getRespose(subject, template_name, email, name, status, id, date, time, cname, doctorName, imageArray,bccAddress,discount);
             }

             //  sendMail after prescriptionUploaded and saved to KidoroCollection
             $state.go('menu.orderConfirmation');
             $ionicLoading.hide();
           }, function(err) {

             $ionicLoading.hide();
             var errorAlert = $ionicPopup.alert({
              title: 'Kidoro Health',
              template: $scope.savedlang.sometngwentwrng,
              cssClass:'custom-popup-alert',
              buttons: [{
                text: $scope.savedlang.okTxt,
                type: 'button-alert-ok',
                onTap: function(e) {
                 errorAlert.close();

               }
             }]
           });

           });

      },
      function(error) {
        $ionicLoading.hide();
        var errorAlert = $ionicPopup.alert({
          title: 'Kidoro Health',
          template: $scope.savedlang.sometngwentwrng,
          cssClass:'custom-popup-alert',
          buttons: [{
           text: $scope.savedlang.okTxt,
           type: 'button-alert-ok',
           onTap: function(e) {
             errorAlert.close();

           }
         }]
       });


      });
    }


  }

  catch (err) {
    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:labTestAddressCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope,$kinvey,$ionicHistory,$timeout, sendMail,$http, $ionicPopup,saveTestsService,saveLabsDataService, userDataService, $ionicLoading, $state, ionicToast
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("labTestAddressCtrl", function($scope,$kinvey,$ionicHistory,$timeout, sendMail,$http, $ionicPopup,saveTestsService,saveLabsDataService, userDataService, $ionicLoading, $state, ionicToast){

  try {
    if(typeof analytics !== 'undefined') { analytics.trackView("labTestAddressCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }
    $scope.userData = userDataService.getData();
// $scope.selectedpharmaDetails = labDataService.getData();
// $scope.PrescriptionData = prescriptionService.getData();
// $scope.chemistRecord = {
//   pharma_name: $scope.selectedpharmaDetails.Pharma_Name,
//   pharma_address: $scope.selectedpharmaDetails.Pharma_Address,
//   pharma_email: $scope.selectedpharmaDetails.Pharma_email
// }
$scope.userAddress = {
  name: $scope.userData.User_Name,
  pincode:"",
  address:"",
  landmark:"",
  city:"",
  state:"",
  phone: $scope.userData.User_Phone,
  age:null,
  gender:null

};


  //  localization using localStorage --onMenuToggle

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage

// $scope.labTestStatus ={'status':null};
//   $scope.$watch('labTestStatus.status', function(newValue, oldValue){
//     alert( $scope.labTestStatus.status);

//   });
$scope.placeOrder = function() {
  $ionicLoading.show();
  $scope.labTestStatus.status=makeBooking(userDataService.getData(),saveLabsDataService.getData(),$scope.userAddress,saveTestsService.getData(),$http,sendMail,$state,$ionicLoading);

 // $timeout(test(), 30000);
}

  //   var currentTime = new Date();
  //   var uniqueDateTime = currentTime.getTime();

  //   $scope.savePresData = function() {
  //     $ionicLoading.show({});
  //     var promise = $kinvey.DataStore.save('KidoroPrescriptions', {
  //      pres_id: uniqueDateTime,
  //      pres_image_array: $scope.PrescriptionData.pres_link_array,
  //      pres_uploaded_by: $scope.userData.Kidoro_Code,
  //      pres_uploader_name: $scope.userData.User_Name
  //      });
  //      promise.then(function(model) {
  //        $scope.savePresPharmaData();
  //      }, function(err) {
  //        $ionicLoading.hide();
  //         var errorAlert = $ionicPopup.alert({
  //         title: 'Kidoro Health',
  //         template: $scope.savedlang.sometngwentwrng,
  //         cssClass:'custom-popup-alert',
  //          buttons: [{
  //       text: $scope.savedlang.okTxt,
  //       type: 'button-alert-ok',
  //       onTap: function(e) {
  //        errorAlert.close();

  //       }
  //       }]
  //       });

  //      });
  //   }


  //     function zeroPadding(n) {
  //                   if(n<10)
  //                   return ("000" + n);
  //                   else if (n>9 && n<100)
  //                   return ("00" + n);
  //                   else if (n>99 && n<1000)
  //                   return("0" + n);
  //                   else return(n);
  //               }

  //   $scope.savePresPharmaData = function() {
  //     $ionicLoading.show({});

  //   var totalCount;
  //   var promise = $kinvey.DataStore.count('KidoroChemistOrderDetails');
  //   promise.then(function(response) {
  //   $scope.totalCount = ++response;
  //   $scope.count = "ORDER"+zeroPadding($scope.totalCount);

  //     var promise = $kinvey.DataStore.save('KidoroChemistOrderDetails', {

  //      Order_ID: $scope.count,
  //      Pharma_ID : $scope.selectedpharmaDetails.Pharma_ID,
  //      Doc_Kidoro_Code : null,
  //      Kidoro_Code :$scope.userData.Kidoro_Code,
  //      Record: $scope.userAddress
  //      });
  //      promise.then(function(model) {
  //            //  sendMail after prescriptionUploaded and saved to KidoroCollection
  //            var email = model.Record.pharmaRecord.pharma_email;
  //            var subject = "Prescription Order Details of" +" "+ model.Record.name +" for "+ model.Record.pharmaRecord.pharma_name;
  //            var id = model.Order_ID;
  //            var name = model.Record.pharmaRecord.pharma_name;
  //            var status = model.Record.address +","+ model.Record.landmark +","+ model.Record.city +","+ model.Record.state +","+ "PinCode: " + model.Record.pincode +","+ "Phone Number: " +model.Record.phone;
  //            var date = model.Record.preRecord.date;
  //            var time = model.Record.preRecord.no_of_days;
  //            var cname = model.Record.name;
  //            var doctorName = "";
  //            var template_name = "chemistorder";
  //            var imageArray = imageService.getData();

  //            if (email != null || email !== undefined) {
  //             //  sendMail.getRespose(subject, template_name, email, name, status, id, date, time, cname, doctorName, imageArray);
  //            }

  //            //  sendMail after prescriptionUploaded and saved to KidoroCollection
  //        $state.go('menu.orderConfirmation');
  //        $ionicLoading.hide();
  //      }, function(err) {

  //        $ionicLoading.hide();
  //         var errorAlert = $ionicPopup.alert({
  //         title: 'Kidoro Health',
  //         template: $scope.savedlang.sometngwentwrng,
  //         cssClass:'custom-popup-alert',
  //          buttons: [{
  //       text: $scope.savedlang.okTxt,
  //       type: 'button-alert-ok',
  //       onTap: function(e) {
  //        errorAlert.close();

  //       }
  //       }]
  //       });

  //      });

  // },
  // function(error) {
  //                 $ionicLoading.hide();
  //                 var errorAlert = $ionicPopup.alert({
  //                 title: 'Kidoro Health',
  //                 template: $scope.savedlang.sometngwentwrng,
  //                 cssClass:'custom-popup-alert',
  //                  buttons: [{
  //              text: $scope.savedlang.okTxt,
  //              type: 'button-alert-ok',
  //              onTap: function(e) {
  //                errorAlert.close();

  //              }
  //            }]
  //          });


  // });
  // }


}

catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});
}

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:orderConfirmationCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $ionicPopup, $ionicHistory, $state
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("orderConfirmationCtrl", function($scope, $ionicPopup, $ionicHistory, $state){

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("orderConfirmationCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }
  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage

    $scope.hideBackButton = true;
    $scope.goToHomePage = function() {
      $ionicHistory.nextViewOptions({
       disableAnimate: true,
       disableBack: true
     });
      $state.go('menu.home')
    }

  } catch (err) {
    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:labTestConfirmationCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $ionicPopup, $ionicHistory, $state
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller("labTestConfirmationCtrl", function($scope, $ionicPopup, $ionicHistory, $state){

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("labTestConfirmationCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

    //  localization using localStorage

    $scope.hideBackButton = true;
    $scope.goToHomePage = function() {
      $ionicHistory.nextViewOptions({
       disableAnimate: true,
       disableBack: true
     });
      $state.go('menu.home')
    }

  } catch (err) {
    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:labDetailsCtrl
// Developed By: TarantulaLabs
// Dependencies: $ionicPlatform, $state, $ionicHistory, userDataService,sendMail,$http,saveParentState, modalService, $kinvey,$ionicPopup, ionicToast, $scope,labDataService,saveLabsDataService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('labDetailsCtrl', function( $ionicPlatform, $state, $ionicHistory, userDataService,sendMail,$http,saveParentState, modalService, $kinvey,$ionicPopup, ionicToast, $scope,labDataService,saveLabsDataService) {

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("labDetailsCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


    //  localization using localStorage

    $scope.$on('$ionicView.afterEnter', function(){
      $scope.hideBar = "true";
    });


    $scope.selectedlabsDetails = labDataService.getData();
    $scope.allTests = $scope.selectedlabsDetails.allTestsSelected

    $scope.showFullImage = function(imageUrl) {
      if (imageUrl == null || imageUrl === undefined) {
      } else {
        modalService.init('templates/image-modal.html',imageUrl, $scope).then(function(modal) {
          modal.show();
        });
      }
    }

    var limitStep = 2;
    $scope.limit = limitStep;
    $scope.showMore = true;
    $scope.incrementLimit = function() {
      $scope.limit = $scope.allTests.length;
      $scope.showMore = false;
    };
    $scope.decrementLimit = function() {
      $scope.limit = limitStep;
      $scope.showMore = true;
    };

    $scope.$on('$ionicView.enter', function(e) {
      var history = $ionicHistory.viewHistory();
      var historyStackLength = history.histories[$ionicHistory.currentHistoryId()].stack.length;
      if(history.histories[$ionicHistory.currentHistoryId()].stack[historyStackLength-2].stateName=="menu.welcomeLogin" || history.histories[$ionicHistory.currentHistoryId()].stack[historyStackLength-2].stateName=="menu.welcomeHome"){
        $scope.callNow();
      }
    });

// Maanik Changes

$scope.bookLabTestNow = function(){
 var user = $kinvey.getActiveUser();
 if(user.username == "TarantulaAdmin"){
             // savePharmaData.setData(data);
             saveParentState.setState('menu.labdetails');
             $state.go('menu.loginHome');

           } else{
            saveLabsDataService.setData($scope.selectedlabsDetails);
            $state.go('menu.labTestAddress');
                 // makeBooking(userDataService.getData(),$scope.selectedlabsDetails,$http,sendMail);

               }

             }
             $scope.callNow = function(){
              var pharma=  $scope.selectedlabsDetails.Lab_Ext;

              var user = $kinvey.getActiveUser();
              if(user.username == "TarantulaAdmin"){

                saveParentState.setState('menu.labdetails');
                $state.go('menu.loginHome');

              } else{

                var confirmPopup = $ionicPopup.confirm({
                 title: $scope.savedlang.callExtTxt,
                 template: $scope.savedlang.dialExtTxt+' '+pharma.toString(),
                 cssClass:'custom-popup',
                 buttons: [{
                  text: $scope.savedlang.noTxt,
                  type: 'button-no',
                  onTap: function(e) {

                    confirmPopup.close();
                  }
                }, {
                  text: $scope.savedlang.callTxt,
                  type: 'button-yes',
                  onTap: function(e) {

                    var call = "tel:" + $scope.savedlang.commonPhoneNumber;

                    document.location.href = call;
                  }
                }]
              });

              }
            }
            $scope.GoToLink = function (url) {
              window.open(url,'_system');
            }

          } catch (err) {

            var errorAlert = $ionicPopup.alert({
             title: 'Kidoro Health',
             template: $scope.savedlang.sometngwentwrng,
             cssClass:'custom-popup-alert',
             buttons: [{
              text: $scope.savedlang.okTxt,
              type: 'button-alert-ok',
              onTap: function(e) {
                errorAlert.close();


              }
            }]
          });
          }

        })


        //************************************************************Kidoro Health Patient****************************************************************//
        // Method Name:MainCtrl
        // Developed By: TarantulaLabs
        // Dependencies:$scope, $ionicPopup, $stateParams,$kinvey
        // Description:
        //************************************************************Kidoro Health Patient****************************************************************//

.controller('MainCtrl', function ($scope, $ionicPopup, $stateParams,$kinvey) {

  try {

    if(typeof analytics !== 'undefined') { analytics.trackView("MainCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }

    var promise = $kinvey.DataStore.find('Doctors');

    promise.then(function(test) {
      listData = test;
      $scope.people = listData;
    },
    function(err) {
      var errorAlert = $ionicPopup.alert({
        title: 'Kidoro Health',
        template: $scope.savedlang.sometngwentwrng,
        cssClass:'custom-popup-alert',
        buttons: [{
          text: $scope.savedlang.okTxt,
          type: 'button-alert-ok',
          onTap: function(e) {
            errorAlert.close();
          }
        }]
      });

    });
  } catch (err) {
    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }


})



//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:OnlineStatusCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $ionicPopup, onlineStatus,$kinvey,$stateParams
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('OnlineStatusCtrl', function OnlineStatusCtrl( $scope, $ionicPopup, onlineStatus,$kinvey,$stateParams) {

  try {

    $scope.onlineStatus = onlineStatus;
    if(typeof analytics !== 'undefined') { analytics.trackView("OnlineStatusCtrl"); }

    $scope.initEvent = function() {
      if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
    }

    $scope.$watch('onlineStatus.isOnline()', function(online) {
      $scope.online_status_string = online ? 'online' : 'offline';

    });

  } catch (err) {
    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:menuCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope,$window, $ionicScrollDelegate, $ionicPopup, saveParentState,$ionicSideMenuDelegate,saveGeoLocationData, $ionicLoading, $cordovaToast, $ionicPlatform, $stateParams,$kinvey, $cordovaGeolocation, $ionicHistory, $state, userDataService, locationDataService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//


.controller('menuCtrl', function ($scope,$window, $ionicScrollDelegate, $ionicPopup, saveParentState,$ionicSideMenuDelegate,saveGeoLocationData, $ionicLoading, $cordovaToast, $ionicPlatform, $stateParams,$kinvey, $cordovaGeolocation, $ionicHistory, $state, userDataService, locationDataService) {

  try {
   if(typeof analytics !== 'undefined') { analytics.trackView("menuCtrl"); }

   $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  $scope.$watch(function () {
   return $ionicSideMenuDelegate.getOpenRatio();
 },
 function (ratio) {

                     //  localization using localStorage --onMenuToggle

                     selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
                     if(selectedLang==null){
                       selectedLang="en";
                       localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
                     }
                     $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


                       //  localization using localStorage

                       if (ratio == 1){
                         if ($state.current.name == "menu.groupChat") {
                        //  console.log("MenuScrollBottom" +$state.current.name );
                        $ionicScrollDelegate.scrollBottom(true);
                      }
                      var activeUser = $kinvey.getActiveUser();
                      if(activeUser.username=="TarantulaAdmin"){
                       $scope.loginShow=true;
                       $scope.profileShow=true;
                       $scope.appointmentShow=false;


                     }
                     else{
                      $scope.user =  userDataService.getData();
                      $scope.imgURI = $scope.user.Profile_Pic_URI;
                      $scope.loginShow=false;
                      $scope.profileShow=false;
                      $scope.appointmentShow=true;
                    }

                  }
                });




  $scope.user =  userDataService.getData();


  $scope.login=function(){
   saveParentState.setState('menu.home');
   $ionicHistory.nextViewOptions({
    disableAnimate: true,
    disableBack: true
  });
   $state.go('menu.loginHome');
 }

 $scope.signOut=function(){

   $ionicLoading.show({});
   var user = $kinvey.getActiveUser();
   if(null !== user) {
     var promise = $kinvey.User.logout();
     promise.then(function() {

         // code by saras after sign out success

         var activeUser = $kinvey.getActiveUser();
         $ionicLoading.hide();

         if((activeUser === null)){
           var promise = $kinvey.User.login('TarantulaAdmin', 'admin');
           promise.then(function(user) {
            $ionicHistory.nextViewOptions({
              disableAnimate: true
            });
            $state.go('menu.home');
              $scope.user = null;
                      $scope.imgURI = null;

            $ionicLoading.hide();

          },
          function(error) {
           $ionicLoading.hide();

           var errorAlert = $ionicPopup.alert({
             title: 'Kidoro Health',
             template: $scope.savedlang.sometngwentwrng,
             cssClass:'custom-popup-alert',
             buttons: [{
              text: $scope.savedlang.okTxt,
              type: 'button-alert-ok',
              onTap: function(e) {
                errorAlert.close();


              }
            }]
          });

         });
         }

         // code by saras after sign out success



       }, function(err) {
         $ionicLoading.hide();
         var errorAlert = $ionicPopup.alert({
           title: 'Kidoro Health',
           template: $scope.savedlang.sometngwentwrng,
           cssClass:'custom-popup-alert',
           buttons: [{
            text: $scope.savedlang.okTxt,
            type: 'button-alert-ok',
            onTap: function(e) {
              errorAlert.close();


            }
          }]
        });

       });
   }
 };

 $scope.rateUs = function() {
   // Find device platform using the plugin org.apache.cordova.device
   var devicePlatform = device.platform;

       // Check which platform
       if (devicePlatform == "iOS") {
           window.open('https://itunes.apple.com/us/app/YOUR-APP-SLUG-HERE/id000000000?mt=8&uo=4','_system'); // or itms://
         } else if (devicePlatform == "Android") {
           window.open('market://details?id=com.ionicframework.cordovaplugintest730983','_system');
         } else if (devicePlatform == "BlackBerry"){
           window.open('http://appworld.blackberry.com/webstore/content/<applicationid>','_system');
         }
       };

// cordova geolocation
var geocoder;
var posOptions = {timeout: 10000, enableHighAccuracy: false};
$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
 var lat  = position.coords.latitude
 var lng = position.coords.longitude

 codeLatLng(lat, lng)
}, function(err) {
});

function initialize() {
 geocoder = new google.maps.Geocoder();

}

initialize();

function codeLatLng(lat, lng) {

 var latlng = new google.maps.LatLng(lat, lng);
 geocoder.geocode({'latLng': latlng}, function(results, status) {
   if (status == google.maps.GeocoderStatus.OK) {



     locationDataService.setData(results);

     if (results[1]) {
          //formatted address
          localStorage.setItem('address', JSON.stringify(results[0]));

         //find country name
         for (var i=0; i<results[0].address_components.length; i++) {
           for (var b=0;b<results[0].address_components[i].types.length;b++) {

             //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
             if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                     //this is the object you are looking for
                     state= results[0].address_components[i];
                     break;
                   }
                 // state
                 if (results[0].address_components[i].types[b] == "administrative_area_level_2") {
                     //this is the object you are looking for
                     city= results[0].address_components[i];
                     break;
                   }


                 }
               }

               $scope.location = city.long_name +", "+state.long_name;

               saveGeoLocationData.setData($scope.location);


             } else {
           // alert("No results found");
         }
       } else {
         // alert("Geocoder failed due to: " + status);
       }
     });
};


} catch (err) {

  //  localization using localStorage --onMenuToggle

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];


    //  localization using localStorage

    var errorAlert = $ionicPopup.alert({
     title: 'Kidoro Health',
     template: $scope.savedlang.sometngwentwrng,
     cssClass:'custom-popup-alert',
     buttons: [{
      text: $scope.savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();


      }
    }]
  });
  }

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:aptCnfCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $ionicPlatform,saveCouponData, $ionicLoading, $ionicPopup, $state, $location, $ionicHistory, $kinvey, appointmentCnfDataService, sendSMSServices, docDataService, userDataService,sendMail
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('aptCnfCtrl', function ( $scope, $ionicPlatform,saveCouponData, $ionicLoading, $ionicPopup, $state, $location, $ionicHistory, $kinvey, appointmentCnfDataService, sendSMSServices, docDataService, userDataService,sendMail) {

  try {

  //  localization using localStorage --onMenuToggle
  if(typeof analytics !== 'undefined') { analytics.trackView("aptCnfCtrl"); }

  $scope.initEvent = function() {
    if(typeof analytics !== 'undefined') { analytics.trackEvent("Category", "Action", "Label", 25); }
  }

  selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
  if(selectedLang==null){
    selectedLang="en";
    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
  }
  $scope.savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

$scope.couponData=saveCouponData.getData();
    //  localization using localStorage

    $scope.user = userDataService.getData();

    $scope.hideBackButton = true;
    $scope.goToHomePage = function() {
      $ionicHistory.nextViewOptions({
       disableAnimate: true,
       disableBack: true
     });
      $state.go('menu.home')
    }
    $scope.appointmentCnfData = appointmentCnfDataService.getData();

    $scope.selectedDoctorData = docDataService.getData();
    $scope.mapAddress = $scope.selectedDoctorData.Address.replace(/[#^]/,'');



    $scope.showConfirm = function(data) {
     var confirmPopup = $ionicPopup.confirm({
       title: $scope.savedlang.cancelApt,
       template: $scope.savedlang.cancelAptconfirmTxt,
       cssClass:'custom-popup',
       buttons: [{
        text: $scope.savedlang.noTxt,
        type: 'button-no',
        onTap: function(e) {

        }
      }, {
        text: $scope.savedlang.yesTxt,
        type: 'button-yes',
        onTap: function(e) {

          $ionicLoading.show({});
          $scope.appointmentCnfData["Appointment_Status"]="Cancelled";
          $scope.appointmentCnfData["cancelled_by"]=$scope.appointmentCnfData.Appointment_by;
          var promise = $kinvey.DataStore.save('KidoroAppointment',$scope.appointmentCnfData);
          promise.then(function(model) {

// Send mail to User after User Cancelled the Appointment

var subject = "Appointment Cancellation Details with" +" "+ $scope.selectedDoctorData.Doc_Name +" has been Cancelled";
var email = $scope.user.User_Mail;
var name = $scope.user.User_Name;
var status = $scope.appointmentCnfData.Appointment_Status;
var id = $scope.appointmentCnfData.Appointment_ID;
var date = $scope.appointmentCnfData.Appointment_Date;
var time = $scope.appointmentCnfData.Appointment_Time + " " + $scope.appointmentCnfData.Appointment_Time_Id;
var cname = $scope.appointmentCnfData.Hospital_Name;
var doctorName = $scope.selectedDoctorData.Doc_Name;
var template_name = "appointment";
var bccAddress= $scope.savedlang.bccAddress;
var discount = "Not Applicable"
sendMail.getRespose(subject, template_name, email, name, status, id, date, time, cname, doctorName,null,bccAddress,discount);


// Send mail to User after User Cancelled the Appointment

// Mail to Doctor after appointment is Cancelled //


var subject = "Appointment Cancellation Details with" +" "+ $scope.user.User_Name +" has been Cancelled";
var email = $scope.selectedDoctorData.Doc_Email;
var name = $scope.user.User_Name;
var status = $scope.appointmentCnfData.Appointment_Status;
var id = $scope.appointmentCnfData.Appointment_ID;
var date = $scope.appointmentCnfData.Appointment_Date;
var time = $scope.appointmentCnfData.Appointment_Time + " " + $scope.appointmentCnfData.Appointment_Time_Id;
var cname = $scope.appointmentCnfData.Hospital_Name;
var doctorName = $scope.selectedDoctorData.Doc_Name;
var template_name = "doctor-appointment";
sendMail.getRespose(subject, template_name, email, name, status, id, date, time, cname, doctorName,null,bccAddress,discount);


// Mail to Doctor after appointment is Cancelled //

// SMS to User after appointment is Cancelled //

var number = $scope.user.User_Phone;
var messageCnf = "APPOINTMENT CANCELLED." + "\n" + "Appointment ID" + ":   " + $scope.appointmentCnfData.Appointment_ID + " for " + $scope.appointmentCnfData.Appointment_Date + " , " + $scope.appointmentCnfData.Appointment_Time + " " +$scope.appointmentCnfData.Appointment_Time_Id + " at " + $scope.appointmentCnfData.Hospital_Name + " with " + $scope.selectedDoctorData.Doc_Name +".\n" + $scope.savedlang.aptmessage;
sendSMSServices.getRespose(number, messageCnf);

// SMS to User after appointment is Cancelled //


$ionicLoading.hide();
$ionicHistory.nextViewOptions({
 disableAnimate: true,
});

$state.go('menu.home')


}, function(err) {
 $ionicLoading.hide();

 var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();


    }
  }]
});


});
        }
      }]
    });
   };

   $scope.GoToLink = function (url) {
    window.open(url,'_system');
  }

// Mail to User after appointment is booked //

var html = "<p><b>Hi" + " " + $scope.user.User_Name + ",</b></p></br>" + "<p>Your appointment booked on <b>Kidoro Health</b> has been confirmed. Here are the appointment details:" + " " + "</p></br>" + "<p><b>Hospital Name</b>" + ":   " + $scope.appointmentCnfData.Hospital_Name + "</p>" + "<p><b>Doctor Name</b>" + ":   " + $scope.selectedDoctorData.Doc_Name + "</p>" + "<p><b>Appointment Date</b>" +":   "+ $scope.appointmentCnfData.Appointment_Date + "</p>" + "<p><b>Appointment Time</b>" +":    "+ $scope.appointmentCnfData.Appointment_Time + " " +$scope.appointmentCnfData.Appointment_Time_Id + "</p>";
var text = "Appointment Confirmation Details with ";
var subject = "Appointment Confirmation Details with" +" "+ $scope.selectedDoctorData.Doc_Name +" has been Confirmed";
var email = $scope.user.User_Mail;
var name = $scope.user.User_Name;
var status = $scope.appointmentCnfData.Appointment_Status;
var id = $scope.appointmentCnfData.Appointment_ID;
var date = $scope.appointmentCnfData.Appointment_Date;
var time = $scope.appointmentCnfData.Appointment_Time + " " + $scope.appointmentCnfData.Appointment_Time_Id;
var cname = $scope.appointmentCnfData.Hospital_Name;
var doctorName = $scope.selectedDoctorData.Doc_Name;
var template_name = "appointment";
var bccAddress=$scope.savedlang.bccAddress;
var discount=""
 if ($scope.couponData == null || $scope.couponData == undefined || $scope.couponData == "") {
                discount = "No discount available";
              }else{
                discount = $scope.couponData.discount;
              }
sendMail.getRespose(subject, template_name, email, name, status, id, date, time, cname, doctorName,null,bccAddress,discount);

// Mail to User after appointment is booked //

// Mail to Doctor after appointment is booked //

var subject = "Appointment Booking Details with" +" "+ $scope.user.User_Name +" has been Confirmed";
var email = $scope.selectedDoctorData.Doc_Email;
var name = $scope.user.User_Name;
var status = $scope.appointmentCnfData.Appointment_Status;
var id = $scope.appointmentCnfData.Appointment_ID;
var date = $scope.appointmentCnfData.Appointment_Date;
var time = $scope.appointmentCnfData.Appointment_Time + " " + $scope.appointmentCnfData.Appointment_Time_Id;
var cname = $scope.appointmentCnfData.Hospital_Name;
var doctorName = $scope.selectedDoctorData.Doc_Name;
var template_name = "doctor-appointment";
sendMail.getRespose(subject, template_name, email, name, status, id, date, time, cname, doctorName,null,bccAddress,discount);

// Mail to Doctor after appointment is booked //


// SMS to User after appointment is booked //
var number = $scope.user.User_Phone;
var messageCnf = "APPOINTMENT CONFIRMED." + "\n" + "Appointment ID" + ":   " + $scope.appointmentCnfData.Appointment_ID + " for " + $scope.appointmentCnfData.Appointment_Date + " , " + $scope.appointmentCnfData.Appointment_Time + " " +$scope.appointmentCnfData.Appointment_Time_Id + " at " + $scope.appointmentCnfData.Hospital_Name + " with " + $scope.selectedDoctorData.Doc_Name +".\n"+" Disscount :"+discount+".\n " + $scope.savedlang.aptmessage;
sendSMSServices.getRespose(number, messageCnf);
// SMS to User after appointment is booked //


} catch (err) {
  var errorAlert = $ionicPopup.alert({
   title: 'Kidoro Health',
   template: $scope.savedlang.sometngwentwrng,
   cssClass:'custom-popup-alert',
   buttons: [{
    text: $scope.savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
}

})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:notificationCtrl
// Developed By: TarantulaLabs
// Dependencies:$scope, $rootScope,$ionicPlatform,$kinvey,$cordovaLocalNotification, $ionicLoading,ionicToast, $state,userDataService
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.controller('notificationCtrl', function ( $scope, $rootScope,$ionicPlatform,$kinvey,$cordovaLocalNotification, $ionicLoading,ionicToast, $state,userDataService) {

           // console.log($rootScope.badgeNo);
           $rootScope.badgeNo=0;
            $scope.notificationList = JSON.parse(localStorage.getItem('kidoroNotification'));
            var monthNames = [
                                      "January", "February", "March",
                                      "April", "May", "June", "July",
                                      "August", "September", "October",
                                      "November", "December"
                                    ];
            $scope.user = userDataService.getData();
            // console.log($scope.user);
            $scope.showNotification=true;


  var value=null;
  $scope.VaccinationArray=[];
                     loadImmunJSON(function(response) {
                          // Do Something with the response e.g.
                          value = JSON.parse(response);

                          setArray(value[0].Birth);
                          setArray(value[1]["6weeks"]);
                          setArray(value[2]["10weeks"]);
                          setArray(value[3]["14weeks"]);
                          setArray(value[4]["6months"]);
                          setArray(value[5]["9-12months"]);
                          setArray(value[6]["12months"]);
                          setArray(value[7]["15months"]);
                          setArray(value[8]["16to18months"]);
                          setArray(value[9]["18months-2years"]);
                          setArray(value[10]["4to6years"]);
                          setArray(value[11]["10to12years"]);
                          // console.log($scope.VaccinationArray);
                          //  console.log($scope.VaccinationArray.length);

});
                     var setArray = function(array){
                        for (var j = 0; j < array.length; j++) {
                              $scope.VaccinationArray.push(array[j]);
                            }
                     }

            if($scope.notificationList == null || $scope.user==undefined){
              $scope.notificationList=[];
               $scope.showNotification=false;
            }
            else{


                for (var i = 0; i < $scope.notificationList.length; i++) {
              var tempDate = new Date($scope.notificationList[i].date);
              $scope.notificationList[i]["displayDate"] =  monthNames[tempDate.getMonth()] +' '+ tempDate.getDate() +', '+ tempDate.getFullYear();
              $scope.notificationList[i]["sortingDate"] = tempDate.getTime();
            }
            $scope.notificationList.sort(function(a,b){
  // Turn your strings into dates, and then subtract them
  // to get a value that is either negative, positive, or zero.
  return new Date(b.date) - new Date(a.date);
});

            $scope.Vaccinationjson=null;
            if($scope.user.Vaccination_Record==null){
            $scope.Vaccinationjson={

        BCG:0,
        OPV0:0,
        HepB1:0,
        IPV1:0,
        HepB2:0,
        Hib1:0,
        Rotavirus1:0,
        PCV1:0,
        DTwP2:0,
        IPV2:0,
        Hib2:0,
        Rotavirus2:0,
        PCV2:0,
        DTwP3:0,
        IPV3:0,
        Hib3:0,
        Rotavirus3:0,
        PCV3:0,
        OPV1:0,
        HepB3:0,
        OPV2:0,
        MMR1:0,
        Typhoid:0,
        ConjugateVaccine:0,
        HepA1:0,
        MMR2:0,
        Varicella1:0,
        PCVbooster:0,
        DTwPB1DTaPB1:0,
        IPVB1:0,
        HibB1:0,
        HepA2:0,
        Typhoidbooster1:0,
        DTwPB2DTaPB2:0,
        OPV3Varicella2:0,
        Typhoidbooster2:0,
        TdapTd:0,
        HPV:0

}


            }
            else{
              $scope.Vaccinationjson=$scope.user.Vaccination_Record;
            }
             }

            $scope.updateVaccination=function(item,index){

                switch (item.subName) {
                        case $scope.VaccinationArray[0]:$scope.Vaccinationjson.BCG=new Date();
                        break;
                        case $scope.VaccinationArray[1]:$scope.Vaccinationjson.OPV0=new Date();
                        break;
                        case $scope.VaccinationArray[2]:$scope.Vaccinationjson.HepB1=new Date();
                        break;
                        case $scope.VaccinationArray[3]:$scope.Vaccinationjson.IPV1=new Date();
                        break;
                        case $scope.VaccinationArray[4]:$scope.Vaccinationjson.HepB2=new Date();
                        break;
                        case $scope.VaccinationArray[5]:$scope.Vaccinationjson.Hib1=new Date();
                        break;
                        case $scope.VaccinationArray[6]:$scope.Vaccinationjson.Rotavirus1=new Date();
                        break;
                        case $scope.VaccinationArray[7]:$scope.Vaccinationjson.PCV1=new Date();
                        break;
                        case $scope.VaccinationArray[8]:$scope.Vaccinationjson.DTwP2=new Date();
                        break;
                        case $scope.VaccinationArray[9]:$scope.Vaccinationjson.IPV2=new Date();
                        break;
                        case $scope.VaccinationArray[10]:$scope.Vaccinationjson.Hib2=new Date();
                        break;
                        case $scope.VaccinationArray[11]:$scope.Vaccinationjson.Rotavirus2=new Date();
                        break;
                        case $scope.VaccinationArray[12]:$scope.Vaccinationjson.PCV2=new Date();
                        break;
                        case $scope.VaccinationArray[13]:$scope.Vaccinationjson.DTwP3=new Date();
                        break;
                        case $scope.VaccinationArray[14]:$scope.Vaccinationjson.IPV3=new Date();
                        break;
                        case $scope.VaccinationArray[15]:$scope.Vaccinationjson.Hib3=new Date();
                        break;
                        case $scope.VaccinationArray[16]:$scope.Vaccinationjson.Rotavirus3=new Date();
                        break;
                        case $scope.VaccinationArray[17]:$scope.Vaccinationjson.PCV3=new Date();
                        break;
                        case $scope.VaccinationArray[18]:$scope.Vaccinationjson.OPV1=new Date();
                        break;
                        case $scope.VaccinationArray[19]:$scope.Vaccinationjson.HepB3=new Date();
                        break;
                        case $scope.VaccinationArray[20]:$scope.Vaccinationjson.OPV2=new Date();
                        break;
                        case $scope.VaccinationArray[21]:$scope.Vaccinationjson.MMR1=new Date();
                        break;
                        case $scope.VaccinationArray[22]:$scope.Vaccinationjson.Typhoid=new Date();
                        break;
                        case $scope.VaccinationArray[23]:$scope.Vaccinationjson.ConjugateVaccine=new Date();
                        break;
                        case $scope.VaccinationArray[24]:$scope.Vaccinationjson.HepA1=new Date();
                        break;
                        case $scope.VaccinationArray[25]:$scope.Vaccinationjson.MMR2=new Date();
                        break;
                        case $scope.VaccinationArray[26]:$scope.Vaccinationjson.Varicella1=new Date();
                        break;
                        case $scope.VaccinationArray[27]:$scope.Vaccinationjson.PCVbooster=new Date();
                        break;
                        case  $scope.VaccinationArray[28]:$scope.Vaccinationjson.DTwPB1DTaPB1=new Date();
                        break;
                        case $scope.VaccinationArray[29]:$scope.Vaccinationjson.IPVB1=new Date();
                        break;
                        case $scope.VaccinationArray[30]:$scope.Vaccinationjson.HibB1=new Date();
                        break;
                        case $scope.VaccinationArray[31]:$scope.Vaccinationjson.HepA2=new Date();
                        break;
                        case $scope.VaccinationArray[32]:$scope.Vaccinationjson.Typhoidbooster1=new Date();
                        break;
                        case $scope.VaccinationArray[33]:$scope.Vaccinationjson.DTwPB2DTaPB2=new Date();
                        break;
                        case $scope.VaccinationArray[34]:$scope.Vaccinationjson.OPV3Varicella2=new Date();
                        break;
                        case $scope.VaccinationArray[35]:$scope.Vaccinationjson.Typhoidbooster2=new Date();
                        break;
                        case $scope.VaccinationArray[36]:$scope.Vaccinationjson.TdapTd=new Date();
                        break;
                        case $scope.VaccinationArray[37]:$scope.Vaccinationjson.HPV=new Date();



                    }


              $scope.user.Vaccination_Record=$scope.Vaccinationjson;
            var promise = $kinvey.DataStore.save('KidoroUser', $scope.user);
            $ionicLoading.show({});
            promise.then(function(model) {
             $scope.countDeleted = 0;
             userDataService.setData(model);
             $scope.notificationList.splice(index, 1);
             localStorage.setItem('kidoroNotification', JSON.stringify($scope.notificationList));
             $ionicLoading.hide();
           }, function(err) {
             $ionicLoading.hide();
             var errorAlert = $ionicPopup.alert({
               title: 'Kidoro Health',
               template: savedlang.sometngwentwrng,
               cssClass:'custom-popup-alert',
               buttons: [{
                text: savedlang.okTxt,
                type: 'button-alert-ok',
                onTap: function(e) {
                  errorAlert.close();

                }
              }]
            });
           });


            }

             $scope.remindLater=function(item){
             var itemArray=[];
             var someDate = new Date();
             var numberOfDaysToAdd = 3;
             someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
             itemArray.push(item.subName);
              $cordovaLocalNotification.update({
                id: item.notificationId,
                date: someDate,
                title: 'Kidoro Health',
                text: 'Vaccination Reminder',
                autoCancel: true,
                sound: true,
                data: itemArray,
                json:null
              }).then(function (result) {
               ionicToast.show("Reminder Rescheduled", 'bottom', false, 1500);
            });
            }
            function contains(a, obj) {
  var i = a.length;
  while (i--) {
   if (a[i] === obj) {
     return true;
   }
 }
 return false;
}

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//



.controller('myCurrentDetailsCtrl', function ($scope, $state,userDataService, $ionicPopup, SmsServices,ionicToast) {



$scope.pregModule = function() {

  $state.go('menu.pregModule');
}


$scope.babyModule = function() {

  $state.go('menu.babyModule');
}


$scope.tryToConceive = function() {

  $state.go('menu.tryToConceiveModule');
}

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//


.controller('tryToConceiveModuleCtrl', function($scope,$ionicModal,$state,$ionicHistory,$ionicPopup,$kinvey) {
 



$scope.KidoroUser={
  name:null,
  weight:null,
  height:null,
complications:null,
deliveryType:null,
haveKids:null,
caseType:"planning"
};

$scope.doSignUp=function(){


       $state.go('menu.welcomeHome');   
}
// .then(function(user) {
// },function(error) {
//   console.log(error);
// });






  $scope.Back=function(){
 $ionicHistory.goBack();
}  

$scope.goBack=function(){
$scope.myPopup.close();
}  

 $scope.goToKidsDetails= function() {

  $scope.myPopup = $ionicPopup.show({
    templateUrl: 'templates/NoOfKidsPopup.html',
    title: 'Details of My kids',
     scope: $scope,
    buttons: [
      {
        
        }
    ]

  });
} 
 })

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//


.controller('babyModuleCtrl', function($scope,$ionicModal,$state,$ionicHistory,$ionicPopup,$kinvey) {


$scope.KidoroUser={
  name:null,
  weight:null,
  height:null,
  caseType:"parenting"
};


$scope.doSignUp=function(){




      $state.go('menu.welcomeHome'); 
}


 $scope.Back=function(){
 $ionicHistory.goBack();
}  
  $scope.goBack=function(){
 
 $scope.myPopup.close();
}  

 $scope.goToKidsDetails= function() {

  $scope.myPopup = $ionicPopup.show({
    templateUrl: 'templates/NoOfKidsPopup.html',
    title: 'Details of My kids',
    scope: $scope,
    buttons: [
      {
       
        }
    ]

  });
}

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//


.controller('pregModuleCtrl', function($scope,$state,$ionicHistory,$ionicPopup,$kinvey) {


$scope.KidoroUser={
  name:null,
  weight:null,
  height:null,
  firstPregnancy:null,
  dueDate:null,
  periodDate:null,
  haveKids:null,
  caseType:"pregnant"
};



$scope.doSignUp=function(){



 $state.go('menu.welcomeHome'); 

}


 $scope.Back=function(){
 $ionicHistory.goBack();
}  


$scope.goBack=function(){
 // $ionicHistory.goBack();
 $scope.myPopup.close();
}

 $scope.goToKidsDetails= function() {

  $scope.myPopup = $ionicPopup.show({
    templateUrl: 'templates/NoOfKidsPopup.html',
    title: 'Details of My kids',
    scope: $scope,
    buttons: [
      {
        
        }
    ]

  });
}

});
