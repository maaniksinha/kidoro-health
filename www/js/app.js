// <!-- Created by Sarswati Kumar Pandey -->

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

var urlConstant = "new";
angular.module('starter', ['ionic', 'ion-fab-button', 'starter.controllers', 'firebase', 'starter.services','starter.directives', 'ionicLazyLoad', 'readMore', 'ionic.ion.imageCacheFactory', 'img-lazy-load', 'me-lazyload', 'ngSanitize','ionic-toast', 'ngAutocomplete', 'ngStorage', 'tabSlideBox', 'ngAnimate', 'kinvey', 'ionic.service.core','angular.filter','ngCordova'])

//Angular directive for lazy load images , useful if you need lazy load images in different ways on your websites.
.constant('imgLazyLoadConf', {    //Define constant in the main file of your app in order to overwrite the default behaviour.
  tolerance: 200,                 //tolerance - load images beforehand
  detectElement: true             //detectElement - add css to img: "min-width: 1px; min-height: 1px" in order to be detected by element.inViewport() function in case if element truly visible for user
})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//
.run(function ($ionicPlatform, $q, $animate, $http, $cordovaToast, $ionicHistory,$cordovaStatusbar,saveChatNotification, $rootScope,$ionicSideMenuDelegate, $ionicPopup, $location, $window, $timeout,$cordovaLocalNotification, $cordovaSplashscreen,$kinvey,$state,userDataService,$ionicLoading, FeedList) {
  $animate.enabled(false);
  $rootScope.badgeNo=0;
      // loadJsonObject -- Call to function with anonymous callback
      loadJSON(function(response) {
            // Do Something with the response e.g.
            jsonresponse = JSON.parse(response);
            localStorage.setItem('lang', JSON.stringify(jsonresponse));
          });
      // loadJsonObject --end
      if (window.cordova && window.cordova.plugins ) {
       // console.log("window.cordova");
       cordova.plugins.notification.local.on("click", function (notification) {
    // app.badgeCount = 0;
    // console.log("Local notification clicked, id= " + notification.id + ", state = " + notification.state + ", json = " + notification.json);
    // console.log(notification);


  });


     }

     loadScript = function(){
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'http://kidorohealth.com/maanik/Operations/BookLabs.js';
      document.body.appendChild(script);
    };



    window.loadScript();

        // back button
        //Register a hardware back button action. Only one action will execute when the back button is clicked, so this method decides which of the registered back button actions has the highest priority.
        $ionicPlatform.registerBackButtonAction(function (event) {

          //  localization using localStorage --onMenuToggle

          selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
          if(selectedLang==null){
            selectedLang="en";
            localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
          }
          savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

            //  localization using localStorage

            if($state.current.name=="menu.home"){
             var defaults = {
              title:  "",
              text:   "",
              ticker: "",
              resume: false,
              silent: true
            };
            cordova.plugins.backgroundMode.setDefaults(defaults);
            cordova.plugins.backgroundMode.enable();
            backAsHome.trigger(function(){
      }, function(){
      });

          } else if (($state.current.name == "menu.appointments") || ($state.current.name == "menu.profile") || ($state.current.name == "menu.aboutUs")  || ($state.current.name == "menu.medicalReports") || ($state.current.name == "menu.groupChat") || ($state.current.name == "menu.prescriptions")) {
            $ionicHistory.nextViewOptions({
             disableAnimate: true,
             disableBack: true
           });
            $ionicHistory.clearHistory();
            $state.go('menu.home');
          } else if ($state.current.name == "menu.cnfAppointment" || $state.current.name == "menu.orderConfirmation") {

            $ionicHistory.nextViewOptions({
             disableAnimate: true,
             disableBack: true
           });
            $ionicHistory.clearHistory();
            $state.go('menu.home');
          } else if (($state.current.name == "tempSplash")) {
            $cordovaToast.show('Exiting...', 'long', 'bottom').then(function(success) {
                  // success
                  ionic.Platform.exitApp();
                }, function (error) {
                  // error
                });
          } else if (($state.current.name == "menu.calendar")) {

            $state.go('menu.docdetails');
          } else if (($state.current.name == "menu.docdetails")) {

            $state.go('menu.doclist');
          } else if (($state.current.name == "menu.doclist") || ($state.current.name == "menu.PharmaList") || ($state.current.name == "menu.labtest")) {

            $state.go('menu.home');
          } else if (($state.current.name == "menu.address")) {
            $ionicHistory.goBack(-2);

          } else if (($state.current.name == "menu.previewPrescription")) {

            $state.go('menu.pharmadetails');
          } else if (($state.current.name == "menu.pharmadetails")) {

            $state.go('menu.PharmaList');
          } else if (($state.current.name == "menu.LabList")) {

            $state.go('menu.labtest');
          } else if (($state.current.name == "menu.labdetails")) {

            $state.go('menu.LabList');
          }
          else if($state.current.name =="menu.loginHome" ){
            $ionicHistory.clearHistory();

            $state.go('menu.home');
          }
          else {
            $ionicHistory.goBack();
          }
        }, 101);

        // back button

      // Add this inside your $ionicPlatform.ready function that is defined inside the run function:

      $ionicPlatform.ready(function($scope) {
        Branch.initSession();

      if(typeof analytics != 'undefined') {
        analytics.startTrackerWithId("UA-75507905-1");// start tracker
        //Google Analytics plugin initialization code will go in the $ionicPlatform.ready()
      } else {
      }//Google Analytics plugin is initialized, it won’t report data until we tell it to

      document.addEventListener('touchstart', function (event) {
            // workaround for Android
            if ($ionicSideMenuDelegate.isOpenLeft()) {
              event.preventDefault();
            }
          });

    $ionicPlatform.on('resume', function(){
      Branch.initSession();
      $state.go(urlConstant);
    });

    $timeout(function() {
      if (device.platform == "Android") {
        $cordovaSplashscreen.hide();
      }
      if (device.platform == "iPhone" || device.platform == "iOS") {
        navigator.splashscreen.hide();
      }
    }, 1);

    screen.lockOrientation('portrait');
    Appsee.start("5a6bda62016746eba2d34f16d6112d4f");

    ionic.Platform.fullScreen(false,true);

           function initializePush() {

            var notificationOpenedCallback = function(jsonData) {


          if(jsonData.message.indexOf("update")>-1||jsonData.message.indexOf("Update")>-1){

          //  localization using localStorage --onMenuToggle

          selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
          if(selectedLang==null){
            selectedLang="en";
            localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
          }
          savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

            //  localization using localStorage

            var updatePopup=$ionicPopup.confirm({
             title: jsonData.additionalData.title,
             template: jsonData.message,
             cssClass:'custom-popup',
             buttons: [{
              text: savedlang.noTxt,
              type: 'button-no',
              onTap: function(e) {

                updatePopup.close();
              }
            }, {
              text: savedlang.updateTxt,
              type: 'button-yes',
              onTap: function(e) {

                var devicePlatform = device.platform;

                if (devicePlatform == "iOS") {
                    window.open('https://itunes.apple.com/us/app/YOUR-APP-SLUG-HERE/id000000000?mt=8&uo=4','_system'); // or itms://
                  } else if (devicePlatform == "Android") {
                    window.open('market://details?id=com.ionicframework.cordovaplugintest730983','_system');
                  } else if (devicePlatform == "BlackBerry"){
                    window.open('http://appworld.blackberry.com/webstore/content/<applicationid>','_system');
                  }
                }
              }]
            });
          }
          else{
            var notificationArray =   JSON.parse(localStorage.getItem('kidoroNotification'));

            if(notificationArray == null){
              notificationArray=[];
            }
             var notificationObject=null;
            if(jsonData.additionalData.NotificationType=='Coupon'){
            notificationObject = {NotificationName: jsonData.additionalData.subName , message : jsonData.message, date : new Date(), status: null, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:new Date().getTime(), notificationStatus:'unread',showButton:false,coupon:jsonData.additionalData.Coupon,couponShow:true,notificationType:jsonData.additionalData.NotificationType,icon:jsonData.additionalData.icon}

            }
            else{
            notificationObject = {NotificationName: jsonData.additionalData.subName , message : jsonData.message, date : new Date(), status: null, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:new Date().getTime(), notificationStatus:'unread',showButton:false,coupon:null,couponShow:false,notificationType:jsonData.additionalData.NotificationType,icon:jsonData.additionalData.icon}

            }

            notificationArray.push(notificationObject);



         $rootScope.badgeNo++;
         $rootScope.$broadcast('update');
            localStorage.setItem('kidoroNotification', JSON.stringify(notificationArray));
          }



        };

        window.plugins.OneSignal.init("3660104e-74fa-11e5-becb-1744a64b8d3e",
         {googleProjectNumber: "162174635746"},
         notificationOpenedCallback);

        // Show an alert box if a notification comes in when the user is in your app.
        window.plugins.OneSignal.enableInAppAlertNotification(false);
        // If set to true notifications will always show in the notification area when user is using app
        window.plugins.OneSignal.enableNotificationsWhenActive(true);

      }

 $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
$rootScope.$broadcast('update');
    });

      $rootScope.$on('$cordovaLocalNotification:trigger', function(event, notification, state) {

             if(notification.id==7||notification.id==42||notification.id==70||notification.id==98||notification.id==180||notification.id==270||notification.id==365||notification.id==450||notification.id==480||notification.id==670||notification.id==1460||notification.id==3650){


          var notificationArray =   JSON.parse(localStorage.getItem('kidoroNotification'));

            if(notificationArray == null){
              notificationArray=[];
            }

            var vaccinationArray = JSON.parse(notification.data);

            for (var i = 0; i < vaccinationArray.length; i++) {

           var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i], message : "Please visit your doctor", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:notification.id, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
            var test = notificationContains(notificationArray,vaccinationObject);
            if(typeof test=="number"){
            notificationArray.splice(test,1);
            notificationArray.push(vaccinationObject);
            }
            else{
              notificationArray.push(vaccinationObject);
            }


            $rootScope.badgeNo++;
             $rootScope.$broadcast('update');
            }

            localStorage.setItem('kidoroNotification', JSON.stringify(notificationArray));

          }
      })
      $rootScope.$on('$cordovaLocalNotification:click',
        function (event, notification, state) {
            saveChatNotification.setData(notification);

            $ionicHistory.clearCache();
            $state.go('menu.groupChat');


});

      initializePush();
        // Check Network Connection cordova plugin

        if(window.Connection) {
          if(navigator.connection.type == Connection.NONE) {

              //  localization using localStorage --onMenuToggle

              selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
              if(selectedLang==null){
                selectedLang="en";
                localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
              }
              savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

                //  localization using localStorage

                $ionicPopup.alert({
                 title: savedlang.nointernet,
                 template: savedlang.connecttointernet,
                 cssClass:'custom-popup-alert',
                 buttons: [{
                  text: savedlang.okTxt,
                  type: 'button-alert-ok',
                  onTap: function(e) {
                    ionic.Platform.exitApp();

                  }
                }]
              });
              }
            }


        // Check Network Connection cordova plugin

        if (window.StatusBar) {

          StatusBar.hide();
        }
      });


        // Kinvey initialization starts

        // Testing App Key: kid_WyB3Qm2kze
        // Testing AppSecret: dff253b755e8466ab4d3be7a059b2ee5


        // Prod appKey : 'kid_bJ8t0oeIHl',
        // Prod appSecret : '275d997b7f6e4799a758e97dc2bb7869',

        var promise = $kinvey.init({
          appKey : 'kid_WyB3Qm2kze',
          appSecret : 'dff253b755e8466ab4d3be7a059b2ee5',
          sync      : { enable: true }
        });
        promise.then(function() {
                        // Kinvey initialization finished with success

                        var user = determineBehavior($kinvey, $timeout, $state, $rootScope,userDataService,$ionicLoading,FeedList,$cordovaLocalNotification,$ionicHistory,saveChatNotification);
                        userDataService.setData(user);


                      }, function(errorCallback) {
                        // Kinvey initialization finished with error
                        determineBehavior($kinvey, $timeout, $state, $rootScope,userDataService,$ionicLoading,FeedList,$cordovaLocalNotification,$ionicHistory,saveChatNotification);
                      });
      })

.config(function ($stateProvider, $httpProvider, $urlRouterProvider,$compileProvider, $ionicConfigProvider, $ionicAppProvider, $cordovaAppRateProvider) {


                // May create problems on Device Integration

                $httpProvider.defaults.useXDomain = true;
                $httpProvider.defaults.withCredentials = false;
                delete $httpProvider.defaults.headers.common["X-Requested-With"];
                $httpProvider.defaults.headers.common["Accept"] = "application/json";
                $httpProvider.defaults.headers.common["Content-Type"] = "application/json";

          // $ionicConfigProvider.views.maxCache(0);

          $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-left').previousTitleText(false);

          $ionicConfigProvider.form.checkbox('circle');

          $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile|content):|data:image\//);

          $stateProvider

          .state('tempSplash', {
            url: '/tempSplash',
            templateUrl: 'templates/tempSplash.html',
            controller: 'tempSplashCtrl'
          })



          .state('menu', {

            url: '/menu',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller:'menuCtrl'
          })


          .state('menu.registerHome', {
            url: '/registerHome',
            views:{
              'menuContent':{
                templateUrl: 'templates/registerHome.html',
                controller: 'registerHome'
              }
            }
          })

          .state('menu.verificationHome', {
            url: '/verificationHome',
            views:{
              'menuContent':{
                templateUrl: 'templates/verificationHome.html',
                controller: 'verificationHome'
              }
            }
          })

          .state('menu.welcomeHome', {
            url: '/welcomeHome',
            views:{
              'menuContent':{
                templateUrl: 'templates/welcomeHome.html',
                controller: 'welcomeHome'
              }
            }
          })

          .state('menu.welcomeLogin', {
            url: '/welcomeLogin',
            views:{
              'menuContent':{
                templateUrl: 'templates/welcomeLogin.html',
                controller: 'welcomeLogin'
              }
            }
          })

          .state('menu.forgotPassword', {
            url: '/forgotPassword',
            views:{
              'menuContent':{
                templateUrl: 'templates/forgotPassword.html',
                controller: 'forgotPassword'
              }
            }
          })

          .state('menu.loginHome', {
            url: '/loginHome',
            views:{
              'menuContent':{
                templateUrl: 'templates/loginHome.html',
                controller: 'loginHome'
              }
            }
          })
//-----------------------------------------------Signup Data States-------------------------------------------------

          .state('menu.myCurrentDetails', {
            url: '/myCurrentDetails',
            views:{
              'menuContent':{
                templateUrl: 'templates/myCurrentDetails.html',
                controller: 'myCurrentDetailsCtrl'
              }
            }
          })



          .state('menu.tryToConceiveModule', {
            url: '/tryToConceiveModule',
            views:{
              'menuContent':{
                templateUrl: 'templates/tryToConceiveModule.html',
                controller: 'tryToConceiveModuleCtrl'
              }
            }
          })




          .state('menu.babyModule', {
            url: '/babyModule',
            views:{
              'menuContent':{
                templateUrl: 'templates/babyModule.html',
                controller: 'babyModuleCtrl'
              }
            }
          })

           .state('menu.pregModule', {
            url: '/pregModule',
            views:{
              'menuContent':{
                templateUrl: 'templates/pregModule.html',
                controller: 'pregModuleCtrl'
              }
            }
          })
//-----------------------------------------------Signup Data States-------------------------------------------------

          .state('menu.home', {
            cache: false,
            url: '/home',
            views:{
              'menuContent':{
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
              }
            }

          })

          .state('menu.profile', {
            url: '/profile',
            views:{
              'menuContent':{
                templateUrl: 'templates/profile.html',
                controller: 'profileCtrl'
              }
            }

          })

          .state('menu.appointments', {
            url: '/appointments',
            views: {
              'menuContent': {
                templateUrl: 'templates/appointments.html',
                controller: 'appointmentCtrl'
              }
            }
          })

          .state('menu.appointmentDetails', {
            url: '/appointmentDetails',
            views: {
              'menuContent': {
                templateUrl: 'templates/appointmentDetails.html',
                controller: 'appointmentDetailsCtrl'
              }
            }
          })

          .state('menu.prescriptions', {
            url: '/prescriptions',
            views: {
              'menuContent': {
                templateUrl: 'templates/prescriptions.html',
                controller: 'prescriptionsCtrl'
              }
            }
          })

          .state('menu.previewPrescription', {
            url: '/previewPrescription',
            views: {
              'menuContent': {
                templateUrl: 'templates/previewPrescription.html',
                controller: 'previewPrescriptionCtrl'
              }
            }
          })

          .state('menu.address', {
            url: '/address',
            views: {
              'menuContent': {
                templateUrl: 'templates/address.html',
                controller: 'addressCtrl'
              }
            }
          })
          .state('menu.labTestAddress', {
            url: '/labTestAddress',
            views: {
              'menuContent': {
                templateUrl: 'templates/labTestAddress.html',
                controller: 'labTestAddressCtrl'
              }
            }
          })
          .state('menu.orderConfirmation', {
            url: '/orderConfirmation',
            views: {
              'menuContent': {
                templateUrl: 'templates/orderConfirmation.html',
                controller: 'orderConfirmationCtrl'
              }
            }
          })
          .state('menu.labTestConfirmation', {
            url: '/labTestConfirmation',
            views: {
              'menuContent': {
                templateUrl: 'templates/labTestConfirmation.html',
                controller: 'labTestConfirmationCtrl'
              }
            }
          })

          .state('menu.medicalReports', {
            url: '/medicalReports',
            views: {
              'menuContent': {
                templateUrl: 'templates/medicalReports.html',
                controller: 'medicalReportsCtrl'
              }
            }
          })

          .state('menu.medicalReportDetail', {
            url: '/medicalReportDetail',
            views: {
              'menuContent': {
                templateUrl: 'templates/medicalReportDetail.html',
                controller: 'medicalReportDetailCtrl'
              }
            }
          })

          .state('menu.aboutUs', {
            url: '/aboutUs',
            views: {
              'menuContent': {
                templateUrl: 'templates/aboutUs.html',
                controller: 'aboutUsCtrl'
              }
            }
          })

          .state('menu.rss', {
            url: "/rss",
            views:{
              'menuContent':{
                templateUrl: "templates/rss.html",
                controller: "FeedCtrl"
              }
            }
          })
.state('menu.notification', {
              url: "/notification",
              views:{
                'menuContent':{
                  templateUrl: "templates/notification.html",
                  controller:'notificationCtrl'
              }
            }
          })
          .state('menu.fav', {
            url: "/favList",
            views:{
              'menuContent':{
                templateUrl: "templates/favList.html",
                controller: "favListCtrl"
              }
            }
          })

          .state('menu.rsscontent', {
            url: "/rsscontent",
            views:{
              'menuContent':{
                templateUrl: "templates/rssContent.html",
                controller: "FeedContentCtrl"
              }
            }
          })

          .state('menu.doclist', {
            url: "/doclist",
            views:{
              'menuContent':{
                templateUrl: "templates/doctorList.html",
                controller: "DoctorListCtrl"
              }
            }
          })

          .state('menu.docdetails', {
            cache: false,
            url: "/docdetails",
            views:{
              'menuContent':{
                templateUrl: "templates/doctorDetails.html",
                controller:'DoctorDetailsCtrl'
              }
            }

          })

          .state('menu.labdetails', {
            url: "/labdetails",
            views:{
              'menuContent':{
                templateUrl: "templates/labDetails.html",
                controller:'labDetailsCtrl'
              }
            }

          })

          .state('menu.pharmadetails', {
            url: "/pharmadetails",
            views:{
              'menuContent':{
                templateUrl: "templates/pharmadetails.html",
                controller:'pharmadetailsCtrl'
              }
            }

          })


          .state('menu.calendar', {
            cache: false,
            url: "/calendar",
            views:{
              'menuContent':{
                templateUrl: "templates/Calendar.html",
                controller:'CalendarController'
              }
            }
          })

          .state('menu.labtest', {
            url: "/labtest",
            views:{
              'menuContent':{
                templateUrl: "templates/labtest.html",
                controller: "labTestCtrl"
              }
            }
          })
          .state('menu.LabList', {
            url: '/LabList',
            views:{
              'menuContent':{
                templateUrl: 'templates/LabList.html',
                controller: 'LabListCtrl'
              }
            }
          })
          .state('menu.PharmaList', {
            url: '/PharmaList',
            views:{
              'menuContent':{
                templateUrl: 'templates/PharmaList.html',
                controller: 'pharmaListCtrl'
              }
            }
          })

          .state('menu.cnfAppointment', {
            url: '/cnfAppointment',
            views:{
              'menuContent':{
                templateUrl: 'templates/appointmentConfirmation.html',
                controller: 'aptCnfCtrl'
              }
            }
          })

          .state('menu.groupChat', {
            url: '/groupChat',
            views:{
              'menuContent':{
                templateUrl: 'templates/groupChat.html',
                controller: 'groupChatCtrl'
              }
            }
          })
          .state('menu.privateChat', {
            url: '/privateChat',
            views:{
              'menuContent':{
                templateUrl: 'templates/privateChat.html',
                controller: 'privateChatCtrl'
              }
            }
          })
          .state('menu.presDetails', {
            cache: false,
            url: "/presDetails",
            views:{
              'menuContent':{
                templateUrl: "templates/presDetails.html",
                controller:'presDetailsController'
              }
            }
          })




          $urlRouterProvider.otherwise('/tempSplash');

        })

function determineBehavior($kinvey, $timeout, $state, $rootScope,userDataService,$ionicLoading,FeedList,$cordovaLocalNotification,$ionicHistory,saveChatNotification) {

  try {


    $state.go('tempSplash');
    function navigateToHome() {
     $state.go('menu.home');
   }
    function navigateToLogin() {
     $state.go('menu.loginHome');
   }
   var activeUser = $kinvey.getActiveUser();
  //  if((activeUser === null)){
  //   var promise = $kinvey.User.login('TarantulaAdmin', 'admin');
  //   promise.then(function(user) {

  //    var feeds = FeedList.get();
  //      $timeout(navigateToHome, 3000);
  //    },
  //    function(error) {

  //    });
  // }
  // else
   if(activeUser==null) {
    // var feeds = FeedList.get();
       // loadLabsData.loadData();
       $timeout(navigateToLogin, 3000);
     }
     else{
      var query = new $kinvey.Query();
      query.equalTo('Kidoro_Code', activeUser.kidoroCode);
      var promise = $kinvey.DataStore.find('KidoroUser', query);

      promise.then(function(models) {

       userDataService.setData(models[0]);
       registerNotificationForUserChat($cordovaLocalNotification,$rootScope,$ionicHistory,userDataService,saveChatNotification);
       registerNotificationForGroupChat($cordovaLocalNotification,$rootScope,$ionicHistory,userDataService);
       var feeds = FeedList.get();

          $timeout(navigateToHome, 3000);
          return models;
        }, function(err) {

        });

    }

  } catch (err) {
  }

};

var registerNotificationForUserChat = function($cordovaLocalNotification,$rootScope,$ionicHistory,userDataService,saveChatNotification){
  var userKidoroCode=userDataService.getData().Kidoro_Code;
  var ref = new Firebase('https://kidorotestchat.firebaseio.com/userChat');

  ref.once('value', function(snapshot) {
    $rootScope.chatKeys=[];
    snapshot.forEach(function(messageSnapshot) {
    // Will be called with a messageSnapshot for each child under the /messages/ node
    var key = messageSnapshot.key();  // e.g. "-JqpIO567aKezufthrn8"
    if(!contains($rootScope.chatKeys,key)){
      $rootScope.chatKeys.push(key);
    }
  });
    for (var i = 0; i < $rootScope.chatKeys.length; i++) {
      if($rootScope.chatKeys[i].indexOf(userKidoroCode)>-1){


        var refChat = new Firebase('https://kidorotestchat.firebaseio.com/userChat/'+$rootScope.chatKeys[i]);
        refChat.orderByChild('time').startAt(Date.now()).on('child_added', function(snapshot) {

          if($ionicHistory.currentStateName().length==0){
          }
          else{
            if(snapshot.val().kidoroCode!=userKidoroCode){

              var notificationData={
                id: "1234",
                date: new Date(),
                message:   snapshot.val().text,
                title:  snapshot.val().user,
                autoCancel: true,
                sound: true,
                data:snapshot.val(),
                json:snapshot.val()
              };
              $cordovaLocalNotification.add(notificationData).then(function () {
               saveChatNotification.setData(notification);
               console.log("$cordovaLocalNotification.add");
            });
            }
          }

      // }
    });
      }
    }






  });
}
var registerNotificationForGroupChat = function($cordovaLocalNotification,$rootScope,$ionicHistory,userDataService){
  var userKidoroCode=userDataService.getData().Kidoro_Code;

  var refChat = new Firebase('https://kidorotestchat.firebaseio.com/groupChat/messages');
  refChat.orderByChild('time').startAt(Date.now()).on('child_added', function(snapshot) {
    console.log(snapshot.val());

          if($ionicHistory.currentStateName().length==0){
          }
          else{
            if(snapshot.val().kidoroCode!=userKidoroCode){
              $cordovaLocalNotification.add({
                id: "1234",
                date: new Date(),
                message:   snapshot.val().text,
                title:  snapshot.val().user,
                autoCancel: true,
                sound: true,
                data:snapshot.val(),
                json:snapshot.val()
              }).then(function () {
            });
            }

          }

    });






}



function contains(a, obj) {
  var i = a.length;
  while (i--) {
   if (a[i] === obj) {
     return true;
   }
 }
 return false;
}
function DeepLinkHandler(data) {
    urlConstant=data.data;
  }

  function notificationContains(a, obj) {
  if(a!=null){
   for (var i = 0; i < a.length; i++) {
    if (a[i].subName === obj.subName) {

      return Number(i);
    }
  }
}

return false;
}
  // <!-- Created by Sarswati Kumar Pandey -->
