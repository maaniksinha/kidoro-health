angular.module('starter.directives', [])
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:map
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.directive('map', function() {
    return {
        restrict: 'A',
        scope: {
        control: '='
        },
        link:function(scope, element, attrs){

          var zValue = scope.$eval(attrs.zoom);
          var lat = scope.$eval(attrs.lat);
          var lng = scope.$eval(attrs.lng);

          var myLatlng = new google.maps.LatLng(lat,lng),
          mapOptions = {
              zoom: zValue,
              center: myLatlng
          },
          map = new google.maps.Map(element[0],mapOptions),
          marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable:false
          });

        }
    };
})
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.directive('cta', function ($document, $rootScope, $timeout, $ionicScrollDelegate) {
  return {
    restrict: 'A',
    link: function ($scope, $element, $attr) {
      var header = $document[0].body.querySelector('ion-header-bar, .bar-header');
      var headerHeight = header.offsetHeight;
      var cta = $document[0].body.querySelector('#cta');
      var updateCta = function(y) {
       ionic.requestAnimationFrame(function () {
          cta.style[ionic.CSS.TRANSFORM] = 'translate3d(0, ' + y + '%, 0)';
        });
      }
      var shrinkAmt;
      var amt;
      var y = 0;
      var prevY = 0;
      var scrollDelay = 0.2;
      function onScroll(e) {
        var scrollTop = e.detail.scrollTop;


        if (scrollTop >= 0) {
          y = Math.min(headerHeight / scrollDelay, Math.max(0, y + scrollTop - prevY));
        } else {
          y = scrollTop;
        }
        updateCta(y);
        prevY = scrollTop;
      }
      $element.bind('scroll', onScroll);
    }
  };
})

// Parallax by Sarswati Kumar Pandey

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.directive('headShrink', function($document) {

  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {
      var resizeFactor, scrollFactor, blurFactor;
      var header = $document[0].body.querySelector('.parallax');

      $element.bind('scroll', function(e) {
        if (e.detail.scrollTop >= 0) {
          scrollFactor = e.detail.scrollTop/2;
          header.style[ionic.CSS.TRANSFORM] = 'translate3d(0, +' + scrollFactor + 'px, 0)';
        } else {
          resizeFactor = -e.detail.scrollTop/100 + 0.99;
          blurFactor = -e.detail.scrollTop/10;
          header.style[ionic.CSS.TRANSFORM] = 'scale('+resizeFactor+','+resizeFactor+')';
          header.style.webkitFilter = 'blur('+blurFactor+'px)';
        }
      });
    }
  }
})

// RSS Content Header by Sarswati Kumar Pandey
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//


.directive('headerShrink', function($document) {
  var fadeAmt;

  var shrink = function(header, content, amt, max) {
    amt = Math.min(44, amt);
    fadeAmt = 1 - amt / 44;
    ionic.requestAnimationFrame(function() {
      header.style.opacity = 1- fadeAmt;

    });
  };

  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {
      var starty = $scope.$eval($attr.headerShrink) || 0;
      var shrinkAmt;

      var header = $document[0].body.querySelector('.cust-bar-header');
      var headerHeight = header.offsetHeight;

      $element.bind('scroll', function(e) {
        var scrollTop = null;
        if(e.detail){
          scrollTop = e.detail.scrollTop;
        }else if(e.target){
          scrollTop = e.target.scrollTop;
        }
        if(scrollTop > starty){
          // Start shrinking
          shrinkAmt = headerHeight - Math.max(0, (starty + headerHeight) - scrollTop);
          shrink(header, $element[0], shrinkAmt, headerHeight);
        } else {
          shrink(header, $element[0], 0, headerHeight);
        }
      });
    }
  }
})


.filter('hrefToJS', function ($sce, $sanitize) {
    return function (text) {
        var regex = /href="([\S]+)"/g;
        var newString = $sanitize(text).replace(regex, "onClick=\"window.open('$1', '_blank', 'location=yes')\"");
        return $sce.trustAsHtml(newString);
    }
})
