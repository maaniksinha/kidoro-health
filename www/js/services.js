

angular.module('starter.services', [])
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:Message
// Developed By: TarantulaLabs
// Dependencies:$firebase
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.factory('Message', ['$firebase', function($firebase) {

		var ref = new Firebase('https://kidorochatclient.firebaseio.com/groupChat');
		var messages = $firebase(ref.child('messages')).$asArray();
		var Message = {
			all: messages,
			create: function (message) {
				return messages.$add(message);
			},
			get: function (messageId) {
				return $firebase(ref.child('messages').child(messageId)).$asObject();
			},
			delete: function (message) {
				return messages.$remove(message);
			}
		};

		return Message;

	}])
	//************************************************************Kidoro Health Patient****************************************************************//
	// Method Name:saveChatNotification
	// Developed By: TarantulaLabs
	// Dependencies:$stateParams
	// Description:
	//************************************************************Kidoro Health Patient****************************************************************//

.factory('saveChatNotification',function($stateParams){
        var savedData = null;
            return {
                getData : function(){
                return savedData;
                },
                setData: function(item){
                  savedData = item;

                }
            }
        })
				//************************************************************Kidoro Health Patient****************************************************************//
				// Method Name:
				// Developed By: TarantulaLabs
				// Dependencies:
				// Description:
				//************************************************************Kidoro Health Patient****************************************************************//

.factory('saveImages',function($stateParams){
        var savedData = null;
            return {
                getData : function(){
                return savedData;
                },
                setData: function(item){
                  savedData = item;

                }
            }
        })
				//************************************************************Kidoro Health Patient****************************************************************//
				// Method Name:saveDays
				// Developed By: TarantulaLabs
				// Dependencies:$stateParams
				// Description:
				//************************************************************Kidoro Health Patient****************************************************************//

.factory('saveDays',function($stateParams){
        var savedData = null;
            return {
                getData : function(){
                return savedData;
                },
                setData: function(item){
                  savedData = item;

                }
            }
        })
				//************************************************************Kidoro Health Patient****************************************************************//
				// Method Name:saveCouponData
				// Developed By: TarantulaLabs
				// Dependencies:$stateParams
				// Description:
				//************************************************************Kidoro Health Patient****************************************************************//

.factory('saveCouponData',function($stateParams){
        var savedData = null;
            return {
                getData : function(){
                return savedData;
                },
                setData: function(item){
                  savedData = item;

                }
            }
        })
				//************************************************************Kidoro Health Patient****************************************************************//
				// Method Name:privateMessage
				// Developed By: TarantulaLabs
				// Dependencies:$firebase
				// Description:
				//************************************************************Kidoro Health Patient****************************************************************//

	.factory('privateMessage', ['$firebase', function($firebase) {

			var ref = new Firebase('https://kidorochatclient.firebaseio.com/userChat');
			var messages = $firebase(ref.child('messages')).$asArray();
			var privateMessage = {
				all: function(id) {
					var messages = $firebase(ref.child(id)).$asArray();
					return messages;
				},
				create: function (message, id) {

					var id = $firebase(ref.child(id)).$asArray();
					return id.$add(message);
				},
				get: function (messageId) {
					return $firebase(ref.child('messages').child(messageId)).$asObject();
				},
				delete: function (message) {
					return messages.$remove(message);
				}
			};

			return privateMessage;

		}])



.service('modalService', function($ionicModal, $rootScope) {

  var init = function(templateUrl, imageUrl, $scope) {

    var promise;
     $scope.imageSrc = imageUrl;
    $scope = $scope || $rootScope.$new();

    promise = $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      return modal;
    });

    $scope.openModal = function() {
       $scope.modal.show();
     };
     $scope.closeModal = function() {
       $scope.modal.hide();
     };
     $scope.$on('$destroy', function() {
       $scope.modal.remove();
     });

    return promise;
  }

  return {
    init: init
  }

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:FeedLoader
// Developed By: TarantulaLabs
// Dependencies:$http,$resource,$ionicPopup
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.factory('FeedLoader', function ($http,$resource,$ionicPopup) {

  try {



    return $resource('http://ajax.googleapis.com/ajax/services/feed/load', {}, {
        fetch: { method: 'JSONP', params: {v: '1.0', callback: 'JSON_CALLBACK'} }
    });
  return{

  }

  } catch (err) {

    //  localization using localStorage

    selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
    if(selectedLang==null){
      selectedLang="en";
      localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
    }
    savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

      //  localization using localStorage

  var errorAlert = $ionicPopup.alert({
         title: 'Kidoro Health',
         template: savedlang.sometngwentwrng,
         cssClass:'custom-popup-alert',
          buttons: [{
      text: savedlang.okTxt,
      type: 'button-alert-ok',
      onTap: function(e) {
        errorAlert.close();

      }
    }]
  });
  }

})

//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:saveGeoLocationData
// Developed By: TarantulaLabs
// Dependencies:$stateParams, $ionicPopup
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

.factory('saveGeoLocationData',function($stateParams, $ionicPopup){
	try {

		var geoLocation =null;
				return {
						getData : function(){

						return geoLocation;
						},
						setData : function(location){
							geoLocation=location;

						}
				}

	} catch (err) {

 var errorAlert = $ionicPopup.alert({
			title: 'Kidoro Health',
			template: savedlang.sometngwentwrng,
			cssClass:'custom-popup-alert',
			 buttons: [{
	 text: savedlang.okTxt,
	 type: 'button-alert-ok',
	 onTap: function(e) {
		 errorAlert.close();

	 }
 }]
});
	}

	 })

	 //************************************************************Kidoro Health Patient****************************************************************//
	 // Method Name:saveKidoroCode
	 // Developed By: TarantulaLabs
	 // Dependencies:$stateParams, $ionicPopup
	 // Description:
	 //************************************************************Kidoro Health Patient****************************************************************//

.factory('saveKidoroCode',function($stateParams, $ionicPopup){
  try {

    var kidoroCode =null;
        return {
            getData : function(){

            return kidoroCode;
            },
            setData : function(code){
              kidoroCode=code;

            }
        }

  } catch (err) {

 var errorAlert = $ionicPopup.alert({
      title: 'Kidoro Health',
      template: savedlang.sometngwentwrng,
      cssClass:'custom-popup-alert',
       buttons: [{
   text: savedlang.okTxt,
   type: 'button-alert-ok',
   onTap: function(e) {
     errorAlert.close();

   }
 }]
});
  }

   })



	 //************************************************************Kidoro Health Patient****************************************************************//
	 // Method Name:savePharmaData
	 // Developed By: TarantulaLabs
	 // Dependencies:$stateParams, $ionicPopup
	 // Description:
	 //************************************************************Kidoro Health Patient****************************************************************//

 .factory('savePharmaData',function($stateParams, $ionicPopup){
   try {

     var savePharmaData =null;
         return {
             getData : function(){

             return savePharmaData;
             },
             setData : function(pharmaData){
               savePharmaData=pharmaData;

             }
         }

   } catch (err) {

  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
   }

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:saveTestsService
		// Developed By: TarantulaLabs
		// Dependencies:$stateParams, $ionicPopup
		// Description:
		//************************************************************Kidoro Health Patient****************************************************************//

		.factory('saveTestsService',function($stateParams, $ionicPopup){



	      try {



	        var savedData = {};

	            return {

	                getData : function(){

	                return savedData;

	                },

	                setData: function(data){

	                  savedData = data;



	                }

	            }



	      } catch (err) {

	   var errorAlert = $ionicPopup.alert({

	        title: 'Kidoro Health',

	        template: savedlang.sometngwentwrng,

	        cssClass:'custom-popup-alert',

	         buttons: [{

	     text: savedlang.okTxt,

	     type: 'button-alert-ok',

	     onTap: function(e) {

	       errorAlert.close();



	     }

	   }]

	 });

	      }



	     })
			 //************************************************************Kidoro Health Patient****************************************************************//
			 // Method Name:saveLabsDataService
			 // Developed By: TarantulaLabs
			 // Dependencies:$stateParams, $ionicPopup
			 // Description:
			 //************************************************************Kidoro Health Patient****************************************************************//

  .factory('saveLabsDataService',function($stateParams, $ionicPopup){
   try {

     var saveLabsData =null;
         return {
             getData : function(){

             return saveLabsData;
             },
             setData : function(LabsData){
               saveLabsData=LabsData;

             }
         }

   } catch (err) {

  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
   }

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:saveParentState
		// Developed By: TarantulaLabs
		// Dependencies:$stateParams, $ionicPopup
		// Description:
		//************************************************************Kidoro Health Patient****************************************************************//

 .factory('saveParentState',function($stateParams, $ionicPopup){
   try {

     var parentStateName ="";
         return {
             getState : function(){

             return parentStateName;
             },
             setState: function(parentState){
               parentStateName=parentState;

             }
         }

   } catch (err) {

  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
   }

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:saveRssDataService
		// Developed By: TarantulaLabs
		// Dependencies:$stateParams, $ionicPopup
		// Description:
		//************************************************************Kidoro Health Patient****************************************************************//

 .factory('saveRssDataService',function($stateParams, $ionicPopup){

   try {

     var savedData = {};
         return {
             getData : function(){
             return savedData;
             },
             setData: function(model){
               savedData = model;



             }
         }

   } catch (err) {
       var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
   }

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:factory-loadRssDataService
		// Developed By: TarantulaLabs
		// Dependencies:$stateParams,$ionicPopup
		// Description:
		//************************************************************Kidoro Health Patient****************************************************************//

    .factory('loadRssDataService',function($stateParams, $ionicPopup){
try {
  return {
      loadData : function(loadedDataLength,feed){

        var savedData=[];
        var rssData = feed;
        var rssDataLength=rssData.length;

        if(loadedDataLength<rssDataLength){
          if(rssDataLength-loadedDataLength>10){
            for (var i = 1; i<10; i++) {
              savedData.push(rssData[loadedDataLength+i]);
            };
          }else{
                for (var i = 1; i<(rssDataLength-loadedDataLength); i++) {
              savedData.push(rssData[loadedDataLength+i]);
            };
          }
        }
      return savedData;
      }
  }
} catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
}

      })

			//************************************************************Kidoro Health Patient****************************************************************//
			// Method Name:service-FeedList
			// Developed By: TarantulaLabs
			// Dependencies:$rootScope,$ionicPopup,$kinvey,FeedLoader,$http,$ionicLoading,$state,saveRssDataService,$timeout
			// Description:
			//************************************************************Kidoro Health Patient****************************************************************//

.service('FeedList', function ($rootScope,$ionicPopup,$kinvey,FeedLoader,$http,$ionicLoading,$state,saveRssDataService,$timeout) {

try {

  function navigateToHome() {

  }

          this.get = function() {

                $timeout(navigateToHome, 3000);

                var promise = $kinvey.DataStore.find('KidoroRss');
                var feeds=[];
                var listData;
                promise.then(function(test) {

                 listData = test;

              if (feeds.length === 0) {
                  for (var i=0; i<listData.length; i++) {

                    var currentData = listData[i];

                                var url = listData[i].Rss_Link;

                      FeedLoader.fetch({q: listData[i].Rss_Link, num:10}, {}, function (data) {

                          var feed = data.responseData.feed.entries;

                          for (var j = 0; j < feed.length; j++) {


                              var tempDate=new Date(feed[j].publishedDate);

                              var monthNames = [
                                      "January", "February", "March",
                                      "April", "May", "June", "July",
                                      "August", "September", "October",
                                      "November", "December"
                                    ];


                                    feed[j].date = tempDate.getDate();
                                    feed[j].monthIndex = tempDate.getMonth();
                                    feed[j].year = tempDate.getFullYear();
                                    feed[j].publishedDate = monthNames[feed[j].monthIndex] +' '+ feed[j].date +', '+ feed[j].year;

                              feed[j]["time"] = new Date(tempDate).getTime();


                              var str = feed[j].content;

                          var regex = /<img.*?src="(.*?)"/;
                          var imgSrc = regex.exec(str);
                          var src ;
                          if(imgSrc!=null){
                            src = imgSrc[1];
                          }
                          if (src!=null) {
                             feed[j]["image"] = src;
                             feed[j]["image"] = feed[j]["image"].replace(/http[:][/]*rc[.]feedsportal[.]com[/][a-z]*[/][0-9]*[/][a-z]*[/][0-9]*[/][a-z]*[/][0-9]*[/][a-z]*[/][0-9]*[/][a-z]*[/][0-9 | a-z]*[/][a-z]*[/][0-9]*[/][a-z]*[/][0-9][/]rc[.]img/,'./img/cover3.png');
                           } else {
                             feed[j]["image"] = "./img/cover3.png"
                           }

                             for (var l = 0; l <listData.length; l++) {

                               if(data.responseData.feed.link.indexOf(listData[l].Rss_Link.split('/')[2])>-1){
                                var tempArray = listData[l].Category.split(",");

                              feed[j]["thumb"] = listData[l].Logo_Image;
                              feed[j]["categories"]=tempArray;
                              feed[j].liked = false;



                               }
                             };

                           feeds.push(feed[j]);


                          };

                          saveRssDataService.setData(feeds);
                          })

                  }




              }
              },
             function(err) {

               var errorAlert = $ionicPopup.alert({
                    title: 'Kidoro Health',
                    template: savedlang.sometngwentwrng,
                    cssClass:'custom-popup-alert',
                     buttons: [{
                 text: savedlang.okTxt,
                 type: 'button-alert-ok',
                 onTap: function(e) {
                   errorAlert.close();

                 }
               }]
             });
            })



              return feeds;
          };

} catch (err) {
    var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
}

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:onlineStatus
		// Developed By: TarantulaLabs
		// Dependencies:$window, $ionicPopup, $rootScope,$stateParams
		// Description:
		//************************************************************Kidoro Health Patient****************************************************************//

.factory('onlineStatus', ["$window", "$rootScope", function ($window, $ionicPopup, $rootScope,$stateParams) {

  try {
    var onlineStatus = {};



    onlineStatus.isOnline = function() {
        return onlineStatus.onLine;
    }

    $window.addEventListener("online", function () {
        onlineStatus.onLine = true;
        $rootScope.$digest();
    }, true);

    $window.addEventListener("offline", function () {
        onlineStatus.onLine = false;
        $rootScope.$digest();
    }, true);

    return onlineStatus;

  } catch (err) {
      var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
  }

}])
//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:rssPassDataService
// Developed By: TarantulaLabs
// Dependencies:
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

  .factory('rssPassDataService',function($stateParams, $ionicPopup){
    try {

      var savedData = {};
          return {
              getData : function(){
              return savedData;
              },
              setData: function(content,date,title,image,link,categories){
                savedData = {
                  'content': content, 'date':date, 'title':title,'image':image,'link':link,'categories':categories

                }


              }
          }

    } catch (err) {
          var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
    }

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:factory-docDataService
		// Developed By: TarantulaLabs
		// Dependencies:$stateParams, $ionicPopup
		// Description:save doc details
		//************************************************************Kidoro Health Patient****************************************************************//

  .factory('docDataService',function($stateParams, $ionicPopup){

    try {

      var savedData = {};
          return {
              getData : function(){
              return savedData;
              },
              setData: function(data){
                savedData = data;

              }
          }

    } catch (err) {
        var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
    }

    })

		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:labDataService
		// Developed By: TarantulaLabs
		// Dependencies:$stateParams, $ionicPopup
		// Description:contains pharma details
		//************************************************************Kidoro Health Patient****************************************************************//

    .factory('labDataService',function($stateParams, $ionicPopup){

      try {

        var savedData = {};
            return {
                getData : function(){
                return savedData;
                },
                setData: function(data){
                  savedData = data;

                }
            }

      } catch (err) {
          var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
      }

      })
			//************************************************************Kidoro Health Patient****************************************************************//
			// Method Name:appointmentCnfDataService
			// Developed By: TarantulaLabs
			// Dependencies:$stateParams, $ionicPopup
			// Description:
			//************************************************************Kidoro Health Patient****************************************************************//

    .factory('appointmentCnfDataService',function($stateParams, $ionicPopup){

      try {

        var savedData = {};
            return {
                getData : function(){
                return savedData;
                },
                setData: function(model){
                  savedData = model;

                }
            }

      } catch (err) {
          var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
      }

      })

      .factory('appointmentDataService',function($stateParams, $ionicPopup){

        try {

          var savedData = {};
              return {
                  getData : function(){
                  return savedData;
                  },
                  setData: function(models){
                    savedData = models;

                  }
              }

        } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
        }

        })
				//************************************************************Kidoro Health Patient****************************************************************//
				// Method Name:appointmentDetailsService
				// Developed By: TarantulaLabs
				// Dependencies:$stateParams, $ionicPopup
				// Description:
				//************************************************************Kidoro Health Patient****************************************************************//

        .factory('appointmentDetailsService',function($stateParams, $ionicPopup){
          try {

            var savedData = {};
                return {
                    getData : function(){
                    return savedData;
                    },
                    setData: function(data){
                      savedData = data;

                    }
                }

          } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
          }

          })

					//************************************************************Kidoro Health Patient****************************************************************//
					// Method Name:prescriptionService
					// Developed By: TarantulaLabs
					// Dependencies:$ionicPopup
					// Description:
					//************************************************************Kidoro Health Patient****************************************************************//

    .factory('prescriptionService',function($stateParams, $ionicPopup){

      try {

        var savedData = {};
            return {
                getData : function(){
                return savedData;
                },
                setData: function(user){
                  savedData = user;

                }
            }

      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
      }

      })
			//************************************************************Kidoro Health Patient****************************************************************//
			// Method Name:locDataService
			// Developed By: TarantulaLabs
			// Dependencies:$stateParams, $ionicPopup
			// Description:
			//************************************************************Kidoro Health Patient****************************************************************//

  .factory('locDataService',function($stateParams, $ionicPopup){

    try {

      var savedData = {};
          return {
              getData : function(){
              return savedData;
              },
              setData: function(data){
                savedData = data;

              }
          }

    } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
    }

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:sendDataService
		// Developed By: TarantulaLabs
		// Dependencies:$stateParams, $ionicPopup
		// Description:
		//************************************************************Kidoro Health Patient****************************************************************//

   .factory('sendDataService',function($stateParams, $ionicPopup){

     try {

       var savedData = {};
           return {
               getData : function(){
               return savedData;
               },
               setData: function(data){
                 savedData = data;

               }
           }

     } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
     }

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:userDataService
		// Developed By: TarantulaLabs
		// Dependencies:$stateParams, $ionicPopup
		// Description:
		//************************************************************Kidoro Health Patient****************************************************************//

    .factory('userDataService',function($stateParams, $ionicPopup){
      try {

        var savedData = {};
            return {
                getData : function(){
                return savedData;
                },
                setData: function(data){
                  savedData = data;

                }
            }

      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
      }

    })


//************************************************************Kidoro Health Patient****************************************************************//
// Method Name:imageService
// Developed By: TarantulaLabs
// Dependencies:$stateParams, $ionicPopup
// Description:
//************************************************************Kidoro Health Patient****************************************************************//

    .factory('imageService',function($stateParams, $ionicPopup){
      try {

        var savedData = {};
            return {
                getData : function(){
                return savedData;
                },
                setData: function(data){
                  savedData = data;

                }
            }

      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
      }

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:locationDataService
		// Developed By: TarantulaLabs
		// Dependencies:$stateParams, $ionicPopup
		// Description:
		//************************************************************Kidoro Health Patient****************************************************************//

    .factory('locationDataService',function($stateParams, $ionicPopup){

      try {

        var savedData = {};
            return {
                getData : function(){
                return savedData;
                },
                setData: function(results){
                  savedData = results;

                }
            }

      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
      }

    })
		//************************************************************Kidoro Health Patient****************************************************************//
		// Method Name:sendSMSServices
		// Developed By: TarantulaLabs
		// Dependencies:$http, $scope, $stateParams, $ionicPopup
		// Description:
		//************************************************************Kidoro Health Patient****************************************************************//

    .factory('sendSMSServices',['$http',function($http, $scope, $stateParams, $ionicPopup){

      try {

        return {
            getRespose : function(number,message) {
              var authKey="91802AFF83DNnxK55fc18a1";
              // var number="7046546954";
            $http.get("https://control.msg91.com/api/sendhttp.php?authkey="+authKey+"&mobiles="+number+"&message="+message+"&sender=Kidoro&route=4&country=91")
             .success(function(result) {

             })
             .error(function(data) {

             });
           }
        }
      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();
    }
  }]
});
      }

  }])

	//************************************************************Kidoro Health Patient****************************************************************//
	// Method Name:SmsServices
	// Developed By: TarantulaLabs
	// Dependencies:$http,$scope, $stateParams, $ionicPopup
	// Description:
	//************************************************************Kidoro Health Patient****************************************************************//

    .factory('SmsServices',['$http',function($http,$scope, $stateParams, $ionicPopup){

      try {

        return {
            getRespose : function(number){

              TOTP = function() {

    var dec2hex = function(s) {
        return (s < 15.5 ? "0" : "") + Math.round(s).toString(16);
    };

    var hex2dec = function(s) {
        return parseInt(s, 16);
    };

    var leftpad = function(s, l, p) {
        if(l + 1 >= s.length) {
            s = Array(l + 1 - s.length).join(p) + s;
        }
        return s;
    };

    var base32tohex = function(base32) {
        var base32chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
        var bits = "";
        var hex = "";
        for(var i = 0; i < base32.length; i++) {
            var val = base32chars.indexOf(base32.charAt(i).toUpperCase());
            bits += leftpad(val.toString(2), 5, '0');
        }
        for(var i = 0; i + 4 <= bits.length; i+=4) {
            var chunk = bits.substr(i, 4);
            hex = hex + parseInt(chunk, 2).toString(16) ;
        }
        return hex;
    };

    this.getOTP = function(secret) {
        try {
            var epoch = Math.round(new Date().getTime() / 1000.0);
            var time = leftpad(dec2hex(Math.floor(epoch / 30)), 16, "0");
            var hmacObj = new jsSHA(time, "HEX");
            var hmac = hmacObj.getHMAC(base32tohex(secret), "HEX", "SHA-1", "HEX");
            var offset = hex2dec(hmac.substring(hmac.length - 1));
            var otp = (hex2dec(hmac.substr(offset * 2, 8)) & hex2dec("7fffffff")) + "";
            otp = (otp).substr(otp.length - 6, 6);
        } catch (error) {
            throw error;
        }
        return otp;
    };

}

var totpObj = new TOTP();
var otp = totpObj.getOTP("JBSWY3DPK5XXE3DEJ5TE6QKUJA");
var otpString = "Kidoro health mobile verification code is" +" "+ otp;

             var authKey="91802AFF83DNnxK55fc18a1";

              $http.get("https://control.msg91.com/api/sendhttp.php?authkey="+authKey+"&mobiles="+number+"&message="+otpString+"&sender=Kidoro&route=4&country=91")
            .success(function(result) {

                return otp;
            })
            .error(function(data) {
                return data;
            });

               return otp;

            }
        }

      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
      }

        }])

				//************************************************************Kidoro Health Patient****************************************************************//
				// Method Name:makeCall
				// Developed By: TarantulaLabs
				// Dependencies:$http,$scope, $stateParams, $ionicPopup
				// Description:
				//************************************************************Kidoro Health Patient****************************************************************//
    .factory('makeCall',['$http',function($http,$scope, $stateParams, $ionicPopup){

      try {
        return {
            getRespose : function(fromNumber,toNumber){
              var toPhone=toNumber;


              var trans="trans";
             var from = "0"+fromNumber;
             var callerId ="07930256873";
             var appid = "54924";
             var dataString = "CallType="+trans+"&From="+from+"&CallerId="+callerId+"&Url=http%3A%2F%2Fmy.exotel.in%2Fexoml%2Fstart%"+appid+"";
               var exotel_sid = "tarantulalabs"; // Your Exotel SID - Get it here: http://my.exotel.in/Exotel/settings/site#exotel-settings
               var exotel_token = "45c4d64296397478518c8205d8fa42b37695e111"; // Your exotel token - Get it here: http://my.exotel.in/Exotel/settings/site#exotel-settings
               var exotel_url = "https://"+exotel_sid+":"+exotel_token+"@twilix.exotel.in/v1/Accounts/"+exotel_sid+"/Calls/connect";
             var req = {
              method: 'POST',
              url: exotel_url,
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
              },
              data:dataString
             }
             $http(req).then(function(response){

             }, function(response){

               var errorAlert = $ionicPopup.alert({
                    title: 'Kidoro Health',
                    template: savedlang.sometngwentwrng,
                    cssClass:'custom-popup-alert',
                     buttons: [{
                 text: savedlang.okTxt,
                 type: 'button-alert-ok',
                 onTap: function(e) {
                   errorAlert.close();

                 }
               }]
             });

             });

            }
        }
      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
      }

        }])

				//************************************************************Kidoro Health Patient****************************************************************//
				// Method Name:sendMail
				// Developed By: TarantulaLabs
				// Dependencies:$http, $scope, $stateParams, $ionicPopup
				// Description:
				//************************************************************Kidoro Health Patient****************************************************************//

    .factory('sendMail',['$http',function($http, $scope, $stateParams, $ionicPopup){

      try {


        return {


            getRespose : function(subject, template_name, email, name, status, id, date, time, cname, doctorName, imageArray,bccAddress,discount){

                                //  localization using localStorage --onMenuToggle

                                  var selectedLang = JSON.parse(localStorage.getItem('selectedLang'));
                                  if(selectedLang==null){
                                    selectedLang="en";
                                    localStorage.setItem('selectedLang', JSON.stringify(selectedLang));
                                  }
                                  var savedlang = JSON.parse(localStorage.getItem('lang'))[selectedLang];

                                  //  localization using localStorage
              // var email="maaniksinha@gmail.com";
              $http.post('https://mandrillapp.com/api/1.0/messages/send-template.json',
              {
    "key": "3LgvpWOf9LxZCUHf36IF8w",
    "template_name": template_name,
    "template_content": [
        {
            "name": "userName",
            "content": name,
        }
    ],
    "message": {
      "global_merge_vars": [
                {
                    "name": "name",
                    "content": name
                },
                {
                    "name": "status",
                    "content": status
                },
                {
                    "name": "id",
                    "content": id
                },
                {
                    "name": "date",
                    "content": date
                },
                {
                    "name": "time",
                    "content": time
                },
                {
                    "name": "cname",
                    "content": cname
                },
                {
                    "name": "doctorName",
                    "content": doctorName
                },
                {
                  "name": "discount",
                    "content": discount
                }
            ],
        "subject": subject,
        "from_email": savedlang.fromEmail,
        "from_name": "Kidoro Health",
        "to": [
            {
                "email": email,
                "name": name,
                "type": "to"
            }
        ],
        "headers": {
            "Reply-To": savedlang.ReplyToEmail
        },
        "bcc_address": bccAddress,

        "images": imageArray


    }

}

            ).
      then(function(response) {
      }, function(response) {

      });
      }
        }
      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
    text: savedlang.okTxt,
    type: 'button-alert-ok',
    onTap: function(e) {
      errorAlert.close();

    }
  }]
});
      }

  }])

	// -------------------------------------------------------Aman-Aakritichanges----------------------------------------------------//

.factory('VaccinationUpdateService',function($cordovaLocalNotification){
	        return {
	          reminder: function(Child_Date,Child_Due_Date){

	                    var oneDay = 24*60*60*1000;

	                    var selectedDate  = 0;
	                    var diffdays=0;
	                    var numberOfDueDaysToAdd = 0;
	                        // console.log(selectedDate);

	                    var currentDate = new Date();
	                         // console.log(currentDate);

	                    if (Child_Date == null){
	                        selectedDate = new Date(Child_Due_Date);
	                        diffdays=0;
	                        numberOfDueDaysToAdd=Math.round(Math.abs((selectedDate.getTime() - currentDate.getTime())/(oneDay)));

	                      }
	                    else{
	                         selectedDate = new Date(Child_Date);
	                         diffdays=Math.round(Math.abs((selectedDate.getTime() - currentDate.getTime())/(oneDay)));
	                         numberOfDueDaysToAdd=0;
	                      }

	                      console.log(diffdays);

	                     var value=null;
	                     loadImmunJSON(function(response) {
	                          // Do Something with the response e.g.
	                          value = JSON.parse(response);
	                          var caseValue;
	                          if (diffdays >= 0 && diffdays < 7) {
	                            caseValue=0;
	                          }
	                          else if (diffdays>7&&diffdays<=42){
	                            caseValue=42;
	                          }
	                          else if (diffdays>42&& diffdays<=70) {
	                            caseValue = 70;
	                          }
	                          else if (diffdays>70 && diffdays <=98) {
	                            caseValue = 98;
	                          }
	                          else if (diffdays>98 && diffdays <=180) {
	                            caseValue = 180;
	                          }
	                          else if (diffdays>180 && diffdays <=270) {
	                            caseValue = 270;
	                          }
	                          else if (diffdays>270 && diffdays <=365) {
	                            caseValue = 365;
	                          }
	                          else if (diffdays>365 && diffdays <=450) {
	                            caseValue = 450;
	                          }
	                          else if (diffdays>450 && diffdays <=480) {
	                            caseValue = 480;
	                          }
	                          else if (diffdays>480 && diffdays <=670) {
	                            caseValue = 670;
	                          }
	                          else if (diffdays>670 && diffdays <=1460) {
	                            caseValue = 1460;
	                          }
	                          else if (diffdays>1460 && diffdays <= 3650) {
	                            caseValue = 3650;
	                          }
	 //--------------------------------
	 var notificationArray =   JSON.parse(localStorage.getItem('kidoroNotification'));

	            if(notificationArray == null){
	              notificationArray=[];
	            }


	//-----------------------
	                      switch (caseValue) {
	                        case 0:
	                              var someDate = new Date();
	                                someDate.setDate(someDate.getDate() - diffdays+numberOfDueDaysToAdd);
	                                //***********************dudatechanges********************
	                              var notificationData={
	                                id: 7,
	                                date: someDate,
	                                message: "Vaccination Reminder" ,
	                                title: "Kidoro Health",
	                                autoCancel: true,
	                                sound: true,
	                                data: value[0].Birth,
	                                json:null
	                              };
	                              $cordovaLocalNotification.add(notificationData).then(function () {
	                               // console.log("notification aadded");
	                            });


	                        case 42:
	                              var someDate = new Date();
	                                var numberOfDaysToAdd = 42 - diffdays+numberOfDueDaysToAdd;
	                                someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
	                                //***********************dudatechanges********************

	                              var notificationData={
	                                id: 42,
	                                date: someDate,
	                                message: "Vaccination Reminder" ,
	                                title: "Kidoro Health",
	                                autoCancel: true,
	                                sound: true,
	                                data: value[1]["6weeks"],
	                                json:null
	                              };
	                              $cordovaLocalNotification.add(notificationData).then(function () {
	                               // console.log("notification aadded");
	                            });


	                        case 70:
	                              var someDate = new Date();
	                                var numberOfDaysToAdd = 70 - diffdays+numberOfDueDaysToAdd;
	                                someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
	                                //***********************dudatechanges********************

	                              var notificationData={
	                                id: 70,
	                                date: someDate,
	                                message: "Vaccination Reminder" ,
	                                title: "Kidoro Health",
	                                autoCancel: true,
	                                sound: true,
	                                data: value[2]["10weeks"],
	                                json:null
	                              };
	                              $cordovaLocalNotification.add(notificationData).then(function () {
	                               // console.log("notification aadded");
	                            });


	                        case 98:
	                            var someDate = new Date();
	                              var numberOfDaysToAdd = 98 - diffdays+numberOfDueDaysToAdd;
	                              someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
	                              //***********************dudatechanges********************

	                            var notificationData={
	                              id: 98,
	                              date: someDate,
	                              message: "Vaccination Reminder" ,
	                              title: "Kidoro Health",
	                              autoCancel: true,
	                              sound: true,
	                              data: value[3]["14weeks"],
	                              json:null
	                            };
	                            $cordovaLocalNotification.add(notificationData).then(function () {
	                             // console.log("notification aadded");
	                          });

	                        case 180:
	                            var someDate = new Date();
	                              var numberOfDaysToAdd =180 - diffdays+numberOfDueDaysToAdd;
	                              someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
	                              //***********************dudatechanges********************

	                            var notificationData={
	                              id: 180,
	                              date: someDate,
	                              message: "Vaccination Reminder" ,
	                              title: "Kidoro Health",
	                              autoCancel: true,
	                              sound: true,
	                              data: value[4]["6months"],
	                              json:null
	                            };
	                            $cordovaLocalNotification.add(notificationData).then(function () {
	                             // console.log("notification aadded");
	                          });


	                        case 270:
	                            var someDate = new Date();
	                              var numberOfDaysToAdd = 270 - diffdays+numberOfDueDaysToAdd;
	                              someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
	                              //***********************dudatechanges********************

	                            var notificationData={
	                              id: 270,
	                              date: someDate,
	                              message: "Vaccination Reminder" ,
	                              title: "Kidoro Health",
	                              autoCancel: true,
	                              sound: true,
	                              data: value[5]["9-12months"],
	                              json:null
	                            };
	                            $cordovaLocalNotification.add(notificationData).then(function () {
	                             // console.log("notification aadded");
	                          });

	                        case 365:
	                            var someDate = new Date();
	                              var numberOfDaysToAdd = 365 - diffdays+numberOfDueDaysToAdd;
	                              someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
	                              //***********************dudatechanges********************

	                            var notificationData={
	                              id: 365,
	                              date: someDate,
	                              message: "Vaccination Reminder" ,
	                              title: "Kidoro Health",
	                              autoCancel: true,
	                              sound: true,
	                              data: value[6]["12months"],
	                              json:null
	                            };
	                            $cordovaLocalNotification.add(notificationData).then(function () {
	                             // console.log("notification aadded");
	                          });

	                      case 450:
	                          var someDate = new Date();
	                            var numberOfDaysToAdd = 450 - diffdays+numberOfDueDaysToAdd;
	                            someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
	                            //***********************dudatechanges********************

	                          var notificationData={
	                            id: 450,
	                            date: someDate,
	                            message: "Vaccination Reminder" ,
	                            title: "Kidoro Health",
	                            autoCancel: true,
	                            sound: true,
	                            data: value[7]["15months"],
	                            json:null
	                          };
	                          $cordovaLocalNotification.add(notificationData).then(function () {
	                           // console.log("notification aadded");
	                        });

	                       case 480:
	                          var someDate = new Date();
	                            var numberOfDaysToAdd = 480 - diffdays+numberOfDueDaysToAdd;
	                            someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
	                            //***********************dudatechanges********************

	                          var notificationData={
	                            id: 480,
	                            date: someDate,
	                            message: "Vaccination Reminder" ,
	                            title: "Kidoro Health",
	                            autoCancel: true,
	                            sound: true,
	                            data: value[8]["16to18months"],
	                            json:null
	                          };
	                          $cordovaLocalNotification.add(notificationData).then(function () {
	                           // console.log("notification aadded");
	                        });

	                      case 670:

	                          var someDate = new Date();
	                            var numberOfDaysToAdd = 670 - diffdays+numberOfDueDaysToAdd;
	                            someDate.setDate(someDate.getDate() + numberOfDaysToAdd);

															var notificationData={
															 id: 670,
															 date: someDate,
															 message: "Vaccination Reminder" ,
															 title: "Kidoro Health",
															 autoCancel: true,
															 sound: true,
															 data: value[9]["18months-2years"],
															 json:null
														 };
														 $cordovaLocalNotification.add(notificationData).then(function () {
															// console.log("notification aadded");
													 });

													 case 1460:
			                           var someDate = new Date();
			                             var numberOfDaysToAdd = 1460 - diffdays;
			                             someDate.setDate(someDate.getDate() + numberOfDaysToAdd);

			                           var notificationData={
			                             id: 1460,
			                             date: someDate,
			                             message: "Vaccination Reminder" ,
			                             title: "Kidoro Health",
			                             autoCancel: true,
			                             sound: true,
			                             data: value[10]["4to6years"],
			                             json:null
			                           };
			                           $cordovaLocalNotification.add(notificationData).then(function () {
			                            // console.log("notification aadded");
			                         });

			                         case 3650:

			                           var someDate = new Date();
			                             var numberOfDaysToAdd = 3650 -  diffdays;
			                             someDate.setDate(someDate.getDate() + numberOfDaysToAdd);

			                           var notificationData={
			                             id: 3650,
			                             date: someDate,
			                             message: "Vaccination Reminder" ,
			                             title: "Kidoro Health",
			                             autoCancel: true,
			                             sound: true,
			                             data: value[11]["10to12years"],
			                             json:null
			                           };
			                           $cordovaLocalNotification.add(notificationData).then(function () {
			                            // console.log("notification aadded");
			                         });

			                     }

			              switch(caseValue){
			                 case 3605:
			                           var vaccinationArray = value[10]["4to6years"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:3650, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}

			                           notificationArray.push(vaccinationObject);
			                             }
			                 case 1460: var vaccinationArray = value[11]["10to12years"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:1460, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                             }
			                 case 670: var vaccinationArray =  value[9]["18months-2years"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:670, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                         }

			                 case 480: var vaccinationArray =  value[8]["16to18months"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                             var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:480, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                             notificationArray.push(vaccinationObject);
			                         }

			                 case 450: var vaccinationArray =  value[7]["15months"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:450, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                         }
			                 case 365: var vaccinationArray =  value[6]["12months"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:365, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                         }

			                 case 270: var vaccinationArray =  value[5]["9-12months"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:270, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                         }

			                 case 180: var vaccinationArray =  value[4]["6months"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:180, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                         }
			                 case 98: var vaccinationArray =  value[3]["14weeks"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:98, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                         }
			                 case 70: var vaccinationArray =  value[2]["10weeks"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:70, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                         }
			                 case 42: var vaccinationArray =  value[1]["6weeks"];

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:42, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                         }
			                 case 0:  var vaccinationArray =  value[0].Birth;

			                           for (var i = 0; i < vaccinationArray.length; i++) {
			                            var vaccinationObject = {NotificationName:"VACCINATION REMINDER",subName:vaccinationArray[i] , message : "Please visit your doctor.", date : new Date(), status: 0, kidoroCode :  userDataService.getData().Kidoro_Code, notificationId:7, notificationStatus:'unread',showButton:true,coupon:null,couponShow:false,notificationType:'vaccination',icon:'img/vaccination.png'}
			                            notificationArray.push(vaccinationObject);
			                         }
			               }
			                localStorage.setItem('kidoroNotification', JSON.stringify(notificationArray));

					});
			}
	}
})

.factory('MilestoneService',function($cordovaLocalNotification){

									 		return {
											milestone: function(Child_Due_Date){
																	var oneDay = 24*60*60*1000;
																	var selectedDate = new Date(Child_Due_Date); //Assuming Child_Due_Date as Preg_date only for now (Preg_date which will be 30 days later from first day of last period)
									 								var currentDate = new Date();
																	// console.log(currentDate);
														 	    var diffdays=Math.round(Math.abs((selectedDate.getTime() - currentDate.getTime())/(oneDay)));


									 								var value=null;
									 								loadmilestoneJSON(function(response) {
									 								value = JSON.parse(response);
																	// value=JSON.stringify(value);
																	console.log(value);

//---------------------------
var notificationArray =  JSON.parse(localStorage.getItem('kidoroNotification'));

if(notificationArray == null)
{
notificationArray=[];
}
//--------------------------------
														if(diffdays>=0 && diffdays<280)
														{
															for(var i=280-diffdays+1;i<280;i++){
																milestonestring="Day"+ i;
																milestonemessage = value[milestonestring];
																console.log(milestonemessage);
																var someDate = new Date();

															someDate.setDate(someDate.getDate()+280-i);
															if(milestonemessage!=null){
										          var notificationData=
																		{
																			id: i,
																			date: someDate,
																			message: "Vaccination Reminder" ,
																			title: "Kidoro Health",
																			autoCancel: true,
																			sound: true,
																			data: milestonemessage,
																			json:null
																		};

																		  $cordovaLocalNotification.add(notificationData).then(function () {
																		   // console.log("notification added");
																		});
																	}
															}
														}
													});
												}}
											});
