function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    // var url = "http://kidorohealth.com/LanguageData/languageJson.json";       //  ServerJsonFile
    var url ='json/languageJson.json';                        // localJsonFile
    xobj.overrideMimeType("application/json");
    xobj.open('GET', url, true);
    xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {

    // .open will NOT return a value but simply returns undefined in async mode so use a callback
    callback(xobj.responseText);

    }
    }
    xobj.send(null);
    }
function loadImmunJSON(callback) {
    var xobj = new XMLHttpRequest();
    // var url = "http://kidorohealth.com/LanguageData/languageJson.json";       //  ServerJsonFile
    var url ='json/immun.json';                        // localJsonFile
    xobj.overrideMimeType("application/json");
    xobj.open('GET', url, true);
    xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {

    // .open will NOT return a value but simply returns undefined in async mode so use a callback
    callback(xobj.responseText);

    }
    }
    xobj.send(null);
    }
function loadmilestoneJSON(callback) {
        var xobj = new XMLHttpRequest();
        // var url = "http://kidorohealth.com/LanguageData/languageJson.json";       //  ServerJsonFile
        var url ='json/milestone.json';                        // localJsonFile
        xobj.overrideMimeType("application/json");
        xobj.open('GET', url, true);
        xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {

        // .open will NOT return a value but simply returns undefined in async mode so use a callback
        callback(xobj.responseText);

        }
        }
        xobj.send(null);
        }
